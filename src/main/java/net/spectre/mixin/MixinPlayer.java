package net.spectre.mixin;

import net.minecraft.entity.LivingEntity;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.monsters.IFlyingMonster;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(LivingEntity.class)
public class MixinPlayer {

    @Inject(at = @At("HEAD"), method = "Lnet/minecraft/entity/LivingEntity;isElytraFlying()Z", cancellable = true)
    public void isElytraFlying(CallbackInfoReturnable<Boolean> callback) {

        ((LivingEntity)(Object)this).getCapability(Capabilities.MONSTER).ifPresent(cap -> {
            if(cap.getTypeObject() instanceof IFlyingMonster){
                IFlyingMonster fly = (IFlyingMonster) cap.getTypeObject();

                if(fly.getFlightHandler().isFlying())
                    callback.setReturnValue(true);

            }
        });

    }

}
