package net.spectre.vamp.recipes.serializers;

import com.google.gson.JsonObject;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.monsters.MonsterType;
import net.spectre.vamp.recipes.MonsterSpecificRecipe;

public class MonsterSpecificRecipeSerializer extends ShapedRecipe.Serializer {

    @Override
    public ShapedRecipe read(ResourceLocation recipeId, JsonObject json) {
        MonsterSpecificRecipe recipe = new MonsterSpecificRecipe(super.read(recipeId, json));
        recipe.addMonster(MonsterRegistry.getFromString(json.get("monster").getAsString()));
        return recipe;
    }

    @Override
    public ShapedRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
        MonsterSpecificRecipe recipe = new MonsterSpecificRecipe(super.read(recipeId, buffer));
        int monsters = buffer.readInt();
        for(int i = 0; i < monsters; ++i){
            recipe.addMonster(buffer.readRegistryIdUnsafe(MonsterRegistry.REGISTRY.get()));
        }
        return recipe;
    }

    @Override
    public void write(PacketBuffer buffer, ShapedRecipe recipe) {
        super.write(buffer, recipe);
        MonsterSpecificRecipe mRecipe = (MonsterSpecificRecipe) recipe;
        buffer.writeInt(mRecipe.getMonstersThatCanCraft().size());
        for(MonsterType type : mRecipe.getMonstersThatCanCraft()){
            buffer.writeRegistryId(type);
        }
    }
}
