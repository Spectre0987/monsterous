package net.spectre.vamp.recipes;

import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.recipes.serializers.MonsterSpecificRecipeSerializer;

public class Recipes {

    public static final DeferredRegister<IRecipeSerializer<?>> RECIPES = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, Monsterous.MODID);

    public static final RegistryObject<IRecipeSerializer<?>> CONTRACTS = RECIPES.register("contract", ContractRecipeSerializer::new);
    public static final RegistryObject<IRecipeSerializer<?>> MONSTER_SPECIFIC = RECIPES.register("monster_specific", MonsterSpecificRecipeSerializer::new);


    public static final IRecipeType<ContractRecipe> CONTRACT_TYPE = IRecipeType.register(Helper.createRL("contract").toString());

}
