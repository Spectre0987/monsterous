package net.spectre.vamp.recipes;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;
import java.util.List;

public class ContractRecipeSerializer extends net.minecraftforge.registries.ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<ContractRecipe> {

    @Override
    public ContractRecipe read(ResourceLocation recipeId, JsonObject json) {

        String type = null;
        NonNullList<Ingredient> ingredients = NonNullList.create();

        //Result
        JsonObject result = json.get("result").getAsJsonObject();
        type = result.get("contract").getAsString();

        //Ingredients
        JsonArray ingredObj = json.get("ingredients").getAsJsonArray();
        for(JsonElement element : ingredObj){
            ingredients.add(Ingredient.deserialize(element));
        }

        return new ContractRecipe(recipeId, type, ingredients);
    }

    @Nullable
    @Override
    public ContractRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
        return ContractRecipe.read(recipeId, buffer);
    }

    @Override
    public void write(PacketBuffer buffer, ContractRecipe recipe) {
        ContractRecipe.write(buffer, recipe);
    }

}
