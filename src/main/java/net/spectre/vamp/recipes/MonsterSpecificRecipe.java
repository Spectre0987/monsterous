package net.spectre.vamp.recipes;


import com.google.common.collect.Lists;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.container.WorkbenchContainer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.misc.ObjectWrapper;
import net.spectre.vamp.monsters.MonsterType;

import java.util.List;

public class MonsterSpecificRecipe extends ShapedRecipe {

    private List<MonsterType> monstersThatCanCraft = Lists.newArrayList();

    public MonsterSpecificRecipe(ResourceLocation idIn, String groupIn, int recipeWidthIn, int recipeHeightIn, NonNullList<Ingredient> recipeItemsIn, ItemStack recipeOutputIn) {
        super(idIn, groupIn, recipeWidthIn, recipeHeightIn, recipeItemsIn, recipeOutputIn);
    }

    public MonsterSpecificRecipe(ShapedRecipe recipe){
        super(recipe.getId(), recipe.getGroup(), recipe.getRecipeWidth(), recipe.getRecipeHeight(), recipe.getIngredients(), recipe.getRecipeOutput());
    }

    public void addMonster(MonsterType type){
        this.monstersThatCanCraft.add(type);
    }

    @Override
    public boolean matches(CraftingInventory inv, World worldIn) {
        ObjectWrapper<Boolean> result = new ObjectWrapper<>(false);

        PlayerEntity player = findPlayer(inv);
        if(player != null){
            player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
                if(canMonsterCraft(cap.getMonsterType()))
                    result.set(true);
            });
        }

        return result.get() && super.matches(inv, worldIn);

    }

    public PlayerEntity findPlayer(CraftingInventory inv){
        if(inv.eventHandler instanceof WorkbenchContainer){
            return ((WorkbenchContainer)inv.eventHandler).player;
        }
        return null;
    }

    public boolean canMonsterCraft(MonsterType type){
        return this.monstersThatCanCraft.contains(type);
    }

    public List<MonsterType> getMonstersThatCanCraft(){
        return this.monstersThatCanCraft;
    }
}
