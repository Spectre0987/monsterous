package net.spectre.vamp.recipes;

import com.google.common.collect.Lists;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.SpecialRecipe;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.spectre.vamp.items.DemonicContract;
import net.spectre.vamp.items.VItems;
import net.spectre.vamp.items.data.ContractData;

import java.util.List;

public class ContractRecipe extends SpecialRecipe {

    private String contractType;
    private NonNullList<Ingredient> ingredients;

    public ContractRecipe(ResourceLocation idIn, String contractType, NonNullList<Ingredient> ingredients) {
        super(idIn);
        this.contractType = contractType;
        this.ingredients = ingredients;
    }

    @Override
    public boolean matches(CraftingInventory inv, World worldIn) {

        List<ItemStack> grid = Lists.newArrayList();

        //fill grid
        for(int i = 0; i < inv.getSizeInventory(); ++i){
            if(!inv.getStackInSlot(i).isEmpty())
                grid.add(inv.getStackInSlot(i));
        }

        //For every item in the table
        for(ItemStack stack : grid){
            //check against all ingredients required
            boolean fits = false;
            for(Ingredient ing : this.ingredients){
                if(ing.test(stack))
                    fits = true;
            }
            //If iterates through all ingredients and none match the stack, recipe doesn't match
            if(!fits)
                return false;
        }


        return grid.size() == this.ingredients.size();
    }

    @Override
    public ItemStack getCraftingResult(CraftingInventory inv) {

        ItemStack stack = new ItemStack(VItems.CONTRACT.get());
        DemonicContract.setContract(stack, new ContractData(this.contractType));
        return stack;
    }

    @Override
    public boolean canFit(int width, int height) {
        return width >=2 && height >= 2;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return Recipes.CONTRACTS.get();
    }

    @Override
    public IRecipeType<?> getType() {
        return Recipes.CONTRACT_TYPE;
    }

    public static ContractRecipe read(ResourceLocation id, PacketBuffer buf){
         String type = buf.readString(100);
        int size = buf.readInt();

        NonNullList<Ingredient> ingredients = NonNullList.create();

        for(int i = 0; i < size; ++i){
            ingredients.add(Ingredient.read(buf));
        }

        return new ContractRecipe(id, type, ingredients);
    }
    public static void write(PacketBuffer buf, ContractRecipe recipe) {
        buf.writeString(recipe.contractType, 100);
        buf.writeInt(recipe.ingredients.size());

        for(Ingredient ing : recipe.ingredients){
            ing.write(buf);
        }

    }

    @Override
    public NonNullList<Ingredient> getIngredients() {
        return this.ingredients;
    }
}
