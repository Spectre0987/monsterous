package net.spectre.vamp.entities.ai;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.FollowOwnerGoal;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.passive.DolphinEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.pathfinding.Path;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.cap.IMinion;

import javax.xml.stream.events.Attribute;
import java.util.EnumSet;

public class VFollowMasterGoal extends Goal {

    public static final int TP_DIST = 30;
    public static final int FOLLOW_MIN = 10;

    CreatureEntity entity;
    IMinion minion;
    private Path pathToOwner;

    public VFollowMasterGoal(CreatureEntity entity){
        this.setMutexFlags(EnumSet.of(Flag.MOVE, Flag.JUMP));
        this.entity = entity;
        this.minion = entity.getCapability(Capabilities.MINION).orElse(null);
    }

    @Override
    public boolean shouldExecute() {

        if(this.minion == null)
            return false;

        if(this.minion.getOwnerEntity() != null && !this.minion.isSitting()){
            return true;
        }


        return false;
    }

    @Override
    public boolean shouldContinueExecuting() {
        if(!super.shouldContinueExecuting())
            return false;
        if(this.pathToOwner == null || this.entity.getNavigator().noPath());
            return false;
    }

    @Override
    public void tick() {
        super.tick();

        PlayerEntity owner = minion.getOwnerEntity();
        if(owner != null){

            //If more than the tp distance, teleport to owner
            if(!owner.getPosition().withinDistance(this.entity.getPosition(), TP_DIST)){
                this.entity.setPosition(owner.getPosX(), owner.getPosY(), owner.getPosZ());
            }
            else if(!owner.getPosition().withinDistance(entity.getPosition(), FOLLOW_MIN)){
                //If in that distance, follow normally
                if(entity.ticksExisted % 10 == 0){

                    this.entity.getNavigator().tryMoveToEntityLiving(owner, this.entity.getAttributeValue(Attributes.MOVEMENT_SPEED));
                    this.pathToOwner = this.entity.getNavigator().getPath();

                }
            }

        }


    }
}
