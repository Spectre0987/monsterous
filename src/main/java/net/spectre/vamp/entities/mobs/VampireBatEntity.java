package net.spectre.vamp.entities.mobs;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.passive.BatEntity;
import net.minecraft.world.World;

public class VampireBatEntity extends BatEntity {

    public VampireBatEntity(EntityType<? extends BatEntity> type, World worldIn) {
        super(type, worldIn);
        
    }
}
