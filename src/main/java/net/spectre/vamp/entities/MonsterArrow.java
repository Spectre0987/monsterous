package net.spectre.vamp.entities;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.world.World;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.monsters.MonsterType;

import java.util.List;

public class MonsterArrow extends ArrowEntity{

	private List<MonsterType> types;

	public MonsterArrow(World world, LivingEntity shooter, List<MonsterType> types) {
		this(world, shooter);
		this.types = types;
	}

	public MonsterArrow(EntityType<? extends ArrowEntity> type, World world) {
		super(type, world);
	}
	
	public MonsterArrow(World worldIn, LivingEntity shooter) {
		super(worldIn, shooter);
	}

	public void setTypes(List<MonsterType> types){
		this.types = types;
	}

	@Override
	protected void arrowHit(LivingEntity living) {
		super.arrowHit(living);
		
		//If hit a monster
		living.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
			
			//If the monster is one this arrow can kill
			for(MonsterType type : types) {
				//Do normal arrow damage
				if(cap.getMonsterType() == type) {
					living.attackEntityFrom(DamageSources.MONSTER_ARROW, (float)this.getDamage());
					return;
				}
			}
			
		});

		System.out.println(living + " was hit by an arrow, had " + this.types.toString());

	}

}
