package net.spectre.vamp.entities;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.spectre.vamp.Monsterous;

public class VEntities {

    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, Monsterous.MODID);

    public static final RegistryObject<EntityType<MonsterArrow>> MONSTER_ARROW = register((type, world) -> new MonsterArrow(type, world), "monster_arrow", 0.1F, 0.1F);


    public static <T extends Entity> RegistryObject<EntityType<T>> register(EntityType.IFactory<T> sup, String name, float width, float height){
        EntityType<T> type = EntityType.Builder.create(sup, EntityClassification.MISC)
                .setShouldReceiveVelocityUpdates(true)
                .setTrackingRange(64)
                .setUpdateInterval(1)
                .size(width, height)
                .build(name);

        return ENTITIES.register(name, () -> type);
    }
}
