package net.spectre.vamp.network;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.network.packets.*;

@Mod.EventBusSubscriber(modid = Monsterous.MODID, bus = Bus.MOD)
public class Network {

	public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(new ResourceLocation(Monsterous.MODID, "main"), () -> "1", str -> str.contentEquals("1"), str -> str.contentEquals("1"));
	private static int ID = 0;
	
	public static void registerPackets() {
		INSTANCE.registerMessage(id(), CapUpdateMessage.class, CapUpdateMessage::encode, CapUpdateMessage::decode, CapUpdateMessage::handle);
		INSTANCE.registerMessage(id(), PowerMessage.class, PowerMessage::encode, PowerMessage::decode, PowerMessage::handle);
		INSTANCE.registerMessage(id(), CooldownUpdateMessage.class, CooldownUpdateMessage::encode, CooldownUpdateMessage::decode, CooldownUpdateMessage::handle);
		INSTANCE.registerMessage(id(), TransformMessage.class, TransformMessage::encode, TransformMessage::decode, TransformMessage::handle);
		INSTANCE.registerMessage(id(), SmiteMessage.class, SmiteMessage::encode, SmiteMessage::decode, SmiteMessage::handle);
		INSTANCE.registerMessage(id(), RequestMonsterUpdateMessage.class, RequestMonsterUpdateMessage::encode, RequestMonsterUpdateMessage::decode, RequestMonsterUpdateMessage::handle);

		INSTANCE.registerMessage(id(), FlightMessage.class, FlightMessage::encode, FlightMessage::decode, FlightMessage::handle);
		INSTANCE.registerMessage(id(), FlightStateMessage.class, FlightStateMessage::encode, FlightStateMessage::decode, FlightStateMessage::handle);
	}

	public static int id(){
		return ++ID;
	}

	public static void sendToClient(ServerPlayerEntity player, Object message) {
		INSTANCE.sendTo(message, player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
	}

	public static void sendToAll(Object message){
		INSTANCE.send(PacketDistributor.ALL.noArg(), message);
	}

	public static void sendToServer(Object message) {
		INSTANCE.send(PacketDistributor.SERVER.noArg(), message);
	}

	public static void sendToAllTrackingEntity(Entity entity, Object message){
		INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> entity), message);
	}
}
