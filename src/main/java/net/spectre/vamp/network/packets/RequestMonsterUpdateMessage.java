package net.spectre.vamp.network.packets;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.network.Network;

import java.util.function.Supplier;

public class RequestMonsterUpdateMessage {

    private int playerID;

    public RequestMonsterUpdateMessage(PlayerEntity player) {
        this(player.getEntityId());
    }

    public RequestMonsterUpdateMessage(int playerId) {
        this.playerID = playerId;
    }

    public static void encode(RequestMonsterUpdateMessage mes, PacketBuffer buffer){
        buffer.writeInt(mes.playerID);
    }

    public static RequestMonsterUpdateMessage decode(PacketBuffer buffer){
        return new RequestMonsterUpdateMessage(buffer.readInt());
    }

    public static void handle(RequestMonsterUpdateMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            Entity e = context.get().getSender().world.getEntityByID(mes.playerID);
            if(e != null){
                e.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
                    Network.sendToClient(context.get().getSender(), new CapUpdateMessage(e.getUniqueID(), cap.serializeNBT()));
                });
            }
        });
        context.get().setPacketHandled(true);
    }

}
