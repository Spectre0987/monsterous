package net.spectre.vamp.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.monsters.IFlyingMonster;

import java.util.function.Supplier;

public class FlightMessage {

    public FlightMessage() {}

    public static void encode(FlightMessage mes, PacketBuffer buf){}

    public static FlightMessage decode(PacketBuffer buf){
        return new FlightMessage();
    }

    public static void handle(FlightMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            context.get().getSender().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
                if(cap.getTypeObject() instanceof IFlyingMonster){
                    IFlyingMonster fly = (IFlyingMonster) cap.getTypeObject();
                    if(fly.getFlightHandler().isFlying())
                        fly.getFlightHandler().flap();

                }
            });
        });
        context.get().setPacketHandled(true);
    }



}
