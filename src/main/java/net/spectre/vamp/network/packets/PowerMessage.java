package net.spectre.vamp.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.powers.Power;

public class PowerMessage {

	private PowerAction action;
	
	public PowerMessage(PowerAction action) {
		this.action = action;
	}
	
	public static void encode(PowerMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.action.ordinal());
	}
	
	public static PowerMessage decode(PacketBuffer buf) {
		return new PowerMessage(PowerAction.values()[buf.readInt()]);
	}
	
	public static void handle(PowerMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			context.get().getSender().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
				if(cap.getTypeObject() != null) {
					if(mes.action == PowerAction.ACT) {
						Power power = cap.getTypeObject().getSelectedPower();
						if(power != null && cap.getTypeObject().canUsePower(power)) {

							if(power.run(context.get().getSender(), cap.getTypeObject()))
								cap.getTypeObject().getOrCreateCooldown(power).startCooldown();
						}
					}
					else {
						cap.getTypeObject().cyclePower(mes.action == PowerAction.NEXT ? 1 : -1);
						MonsterCapability.update(context.get().getSender());
					}
				}
			});
		});
		context.get().setPacketHandled(true);
	}
	
	
	public enum PowerAction{
		ACT,
		NEXT,
		PREV
	}
}
