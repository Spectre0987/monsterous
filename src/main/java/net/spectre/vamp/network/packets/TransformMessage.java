package net.spectre.vamp.network.packets;

import net.minecraft.entity.Entity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.misc.TransformationType;

import java.util.function.Supplier;

public class TransformMessage {

    int entityID;
    TransformationType type;

    public TransformMessage(int entityID, TransformationType type){
        this.entityID = entityID;
        this.type = type;
    }

    public static void encode(TransformMessage mes, PacketBuffer buf){
        buf.writeInt(mes.entityID);
        buf.writeInt(mes.type.ordinal());
    }

    public static TransformMessage decode(PacketBuffer buf){
        return new TransformMessage(buf.readInt(), TransformationType.values()[buf.readInt()]);
    }

    public static void handle(TransformMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {

            Entity e = context.get().getSender().world.getEntityByID(mes.entityID);
            if(e != null){
                e.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
                    if(cap.getTypeObject() != null)
                        cap.getTypeObject().setTransformationType(mes.type);
                });
            }

        });
        context.get().setPacketHandled(true);
    }

}
