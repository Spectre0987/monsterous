package net.spectre.vamp.network.packets;

import net.minecraft.entity.Entity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.misc.ObjectWrapper;
import net.spectre.vamp.monsters.FlyingHandler;
import net.spectre.vamp.monsters.IFlyingMonster;

import java.util.function.Supplier;

public class FlightStateMessage {


    public boolean flying;
    public int entityID;

    public FlightStateMessage(int entityID, boolean flying){
        this.entityID = entityID;
        this.flying = flying;
    }

    public static void encode(FlightStateMessage mes, PacketBuffer buf){
        buf.writeInt(mes.entityID);
        buf.writeBoolean(mes.flying);
    }

    public static FlightStateMessage decode(PacketBuffer buf){
        return new FlightStateMessage(buf.readInt(), buf.readBoolean());
    }

    public static void handle(FlightStateMessage mes, Supplier<NetworkEvent.Context> context){

        ObjectWrapper<World> world = new ObjectWrapper<>(null);
        if(context.get().getDirection() == NetworkDirection.PLAY_TO_SERVER)
            world.set(context.get().getSender().world);
        else world.set(Monsterous.PROXY.getClientWorld());

        context.get().enqueueWork(() -> {
            Entity e = world.get().getEntityByID(mes.entityID);
            if(e != null){
                e.getCapability(Capabilities.MONSTER).ifPresent(cap -> {

                    if(cap.getTypeObject() instanceof IFlyingMonster){
                        FlyingHandler flight = ((IFlyingMonster)cap.getTypeObject()).getFlightHandler();
                        flight.setInFlight(mes.flying);
                    }

                });
            }
        });
        context.get().setPacketHandled(true);
    }
}
