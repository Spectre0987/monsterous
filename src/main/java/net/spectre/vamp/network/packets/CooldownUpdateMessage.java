package net.spectre.vamp.network.packets;

import net.minecraft.entity.Entity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.powers.Power;

import java.util.function.Supplier;

public class CooldownUpdateMessage {

    private int entityID;
    private ResourceLocation powerID;
    private int cooldown;

    public CooldownUpdateMessage(int entityID, ResourceLocation powerID, int cooldownTicks) {
        this.entityID = entityID;
        this.powerID = powerID;
        this.cooldown = cooldownTicks;
    }

    public static void encode(CooldownUpdateMessage mes, PacketBuffer buf){
        buf.writeInt(mes.entityID);
        buf.writeResourceLocation(mes.powerID);
        buf.writeInt(mes.cooldown);
    }

    public static CooldownUpdateMessage decode(PacketBuffer buf){
        return new CooldownUpdateMessage(buf.readInt(), buf.readResourceLocation(), buf.readInt());
    }

    public static void handle(CooldownUpdateMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {

            Entity e = context.get().getSender().getServerWorld().getEntityByID(mes.entityID);
            if(e != null){
                e.getCapability(Capabilities.MONSTER).ifPresent(cap -> {

                    if(cap.getTypeObject() != null){
                        for(Power p : cap.getTypeObject().getPowers()){
                            if(p.getRegistryName().equals(mes.powerID)){
                                cap.getTypeObject().getOrCreateCooldown(p).setCooldown(mes.cooldown);
                                break;
                            }
                        }
                    }

                });
            }

        });
        context.get().setPacketHandled(true);
    }
}
