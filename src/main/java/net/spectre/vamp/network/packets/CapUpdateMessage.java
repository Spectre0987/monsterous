package net.spectre.vamp.network.packets;

import java.util.UUID;
import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.cap.MonsterCapability;

public class CapUpdateMessage {

	UUID id;
	CompoundNBT tag;
	
	public CapUpdateMessage(UUID id, CompoundNBT tag) {
		this.tag = tag;
		this.id = id;
	}
	
	public static void encode(CapUpdateMessage mes, PacketBuffer buf) {
		buf.writeUniqueId(mes.id);
		buf.writeCompoundTag(mes.tag);
	}
	
	public static CapUpdateMessage decode(PacketBuffer buf){
		return new CapUpdateMessage(buf.readUniqueId(), buf.readCompoundTag());
	}
	
	public static void handle(CapUpdateMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			PlayerEntity player = Minecraft.getInstance().world.getPlayerByUuid(mes.id);

			if(player != null){
				player.getCapability(Capabilities.MONSTER).ifPresent(cap -> cap.deserializeNBT(mes.tag));
			}

		});
		cont.get().setPacketHandled(true);
	}
}
