package net.spectre.vamp.network.packets;

import net.minecraft.entity.Entity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.cap.Capabilities;

import java.util.function.Supplier;

public class SmiteMessage {

    private int smiteTicks;
    private int smiteColor;
    private int entityID;

    public SmiteMessage(int entityID, int smiteTicks, int smiteColor){
        this.entityID = entityID;
        this.smiteTicks = smiteTicks;
        this.smiteColor = smiteColor;
    }

    public static void encode(SmiteMessage mes, PacketBuffer buf){
        buf.writeInt(mes.entityID);
        buf.writeInt(mes.smiteTicks);
        buf.writeInt(mes.smiteColor);
    }

    public static SmiteMessage decode(PacketBuffer buf){
        return new SmiteMessage(buf.readInt(), buf.readInt(), buf.readInt());
    }

    public static void handle(SmiteMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            Entity e = Monsterous.PROXY.getClientWorld().getEntityByID(mes.entityID);
            e.getCapability(Capabilities.SMITE).ifPresent(cap -> {
                cap.setSmiteTicks(mes.smiteTicks);
                cap.setSmiteColor(mes.smiteColor);
            });
        });
        context.get().setPacketHandled(true);
    }


}
