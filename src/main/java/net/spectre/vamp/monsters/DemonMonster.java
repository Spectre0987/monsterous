package net.spectre.vamp.monsters;

import com.google.common.collect.Lists;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FlowerBlock;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.merchant.villager.VillagerEntity;
import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.powers.Powers;
import net.spectre.vamp.tags.VBlockTags;
import net.spectre.vamp.tags.VEntityTags;
import net.spectre.vamp.tags.VItemTags;

import java.util.List;
import java.util.UUID;

public class DemonMonster extends BaseMonster implements IFlyingMonster, ICantBeAttackedByAll, IDontTakeNormalDamage{

	public static final AttributeModifier DEMONIC_HEALTH = new AttributeModifier(UUID.fromString("82beb50a-c0f8-4a99-a71b-fcf722612826"), "demonic health", 10, Operation.ADDITION);
	public static final BlockPos CORRUPT_RADIUS = new BlockPos(4, 4, 4);

	private float corruption = 100.0F;
	private List<UUID> playerContracts = Lists.newArrayList();
	private FlyingHandler flyingHandler;
	
	public DemonMonster(PlayerEntity player) {
		super(player);

		this.flyingHandler = new FlyingHandler(player);

		if(!player.getAttribute(Attributes.MAX_HEALTH).hasModifier(DEMONIC_HEALTH))
			player.getAttribute(Attributes.MAX_HEALTH).applyPersistentModifier(DEMONIC_HEALTH);
	}

	@Override
	public void tick() {

		this.flyingHandler.tick();
		this.collideWhenFlying();

		if(!this.player.isCreative() && !this.player.isSpectator()) {
			
			//Food
			this.player.getFoodStats().setFoodLevel(19);
			
			//Corruption

			//Out of nether timer
			if(player.ticksExisted % 20 == 0){
				if(!this.isInHeat() && !this.isInHell() && !hasActiveContract()) {
					this.addCorruption(-0.0666F);
					if(corruption <= 0.0F) {
						//Die
						this.player.attackEntityFrom(DamageSources.OUT_OF_HELL, 2);
					}
				}
				else {
					//Slowly gain corruption in nether
					this.addCorruption(0.6F);
				}
			}
			
			this.repelFromHolyEntity();
			
			//Water damage loop
			if(player.ticksExisted % 80 == 0) {
				if(player.isInWaterRainOrBubbleColumn())
					player.attackEntityFrom(DamageSource.DROWN, 2);
			}
		}
		
		this.player.extinguish();
		if(player.ticksExisted % 60 == 0)
			this.corruptPlants();
		
	}

	public void corruptPlants(){
		BlockPos.getAllInBox(this.player.getPosition().subtract(CORRUPT_RADIUS), this.player.getPosition().add(CORRUPT_RADIUS))
				.filter(pos -> player.getRNG().nextDouble() < 0.25).forEach(pos -> {
			BlockState state = player.world.getBlockState(pos);
			//Flowers
			if(state.getBlock() instanceof FlowerBlock && state.getBlock() != VBlocks.DEMON_ROSE.get()) {
				player.world.setBlockState(pos, VBlocks.DEMON_ROSE.get().getDefaultState());

				for(int i = 0; i < 3; ++i)
					player.world.addParticle(ParticleTypes.SMOKE, pos.getX() + 0.5, pos.getY() + 0.5, pos.getZ() + 0.5, 0, 0, 0);

			}
			if(state.getBlock().isIn(BlockTags.SAPLINGS))
				player.world.setBlockState(pos, Blocks.DEAD_BUSH.getDefaultState());

			if(state.getBlock() == Blocks.GRASS_BLOCK)
				player.world.setBlockState(pos, Blocks.DIRT.getDefaultState());

			if(state.getBlock() == Blocks.GRASS)
				player.world.setBlockState(pos, Blocks.AIR.getDefaultState());
		});
	}

	public void collideWhenFlying(){
		if(this.getFlightHandler().isFlying()){
			for(LivingEntity e : player.world.getEntitiesWithinAABB(LivingEntity.class, player.getBoundingBox())){
				if(e != player){
					float damage = 10.0F * (float)this.getFlightHandler().getSpeed();

					if(Helper.isShielding(e)){
						this.getFlightHandler().setInFlight(false);
					}
					else e.attackEntityFrom(DamageSources.DEMONIC_RAM, damage);

				}
			}
		}
	}

	private boolean hasActiveContract(){
		for(UUID id : this.playerContracts){
			PlayerEntity p = player.world.getPlayerByUuid(id);
			if(p != null && p.isAlive())
				return true;
		}
		return false;
	}
	
	private void repelFromHolyEntity() {
		for(LivingEntity live : player.world.getEntitiesWithinAABB(LivingEntity.class, new AxisAlignedBB(player.getPosition()).grow(16))) {

			//Repel from clerics
			if(live instanceof VillagerEntity){
				if(((VillagerEntity)live).getVillagerData().getProfession() == VillagerProfession.CLERIC) {
					this.repel(live.getPositionVec());
					return;
				}
			}

			//Repel if holding a holy item
			if(Helper.inEitherHand(stack -> VItemTags.HOLY_ITEMS.contains(stack.getItem()), live)){
				this.repel(live.getPositionVec());
				return;
			}
			
		}
	}

	public boolean isInHell(){
		return player.world.getDimensionKey().equals(World.THE_NETHER);
	}

	/**
	 * Not the sexy kind
	 * @return
	 */
	public boolean isInHeat(){
		BlockState state = player.world.getBlockState(player.getPosition());

		if(state.getBlock().isIn(VBlockTags.HEAT_SOURCE))
			return true;

		return state.getBlock() == Blocks.FIRE || state.getFluidState().isTagged(FluidTags.LAVA) || state.getBlock() == Blocks.CAMPFIRE;
	}

	/**
	 *
	 * @param pos - the position to repel from
	 */
	private void repel(Vector3d pos) {
		Vector3d motion = player.getPositionVec().subtract(pos).normalize().scale(0.1F);
		
		player.setMotion(player.getMotion().add(motion));
	}
	
	public void addCorruption(float add) {
		this.corruption = MathHelper.clamp(this.corruption + add, 0.0F, 100.0F);
	}
	
	public float getCorruptionBar() {
		return this.corruption / 100.0F;
	}

	@Override
	public void respawn() {
		this.corruption = 100.0F;
	}
	
	public float getCorruption() {
		return this.corruption;
	}
	
	public void setCorruption(float corruption) {
		this.corruption = corruption;
	}

	public List<UUID> getContractPlayers(){
		return this.playerContracts;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = super.serializeNBT();
		tag.putFloat("corruption", this.corruption);

		ListNBT contracts = new ListNBT();
		for(UUID id : this.playerContracts){
			contracts.add(StringNBT.valueOf(id.toString()));
		}
		tag.put("contracts", contracts);

		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		super.deserializeNBT(nbt);
		this.corruption = nbt.getFloat("corruption");

		this.playerContracts.clear();
		ListNBT contracts = nbt.getList("contracts", Constants.NBT.TAG_STRING);
		for(INBT base : contracts){
			this.playerContracts.add(UUID.fromString(((StringNBT)base).getString()));
		}

	}

	@Override
	public void initPowers() {
		this.registerPower(Powers.RETURN_TO_HELL.get());
		this.registerPower(Powers.HUNT_EYE.get());
		this.registerPower(Powers.FORCE_PUSH.get());
	}

	@Override
	public void onRemove() {
		if(player.getAttribute(Attributes.MAX_HEALTH).hasModifier(DEMONIC_HEALTH))
			player.getAttribute(Attributes.MAX_HEALTH).removeModifier(DEMONIC_HEALTH);
	}

	@Override
	public FlyingPose getPose() {
		return this.getFlightHandler().isFlying() ? FlyingPose.FLYING : FlyingPose.LANDED;
	}

	@Override
	public FlyingHandler getFlightHandler() {
		return this.flyingHandler;
	}


	@Override
	public boolean canAttackMe(MobEntity entity) {
		return !VEntityTags.HELL_FRIEND.contains(entity.getType());
	}

	@Override
	public boolean blockCompletely(DamageSource source) {
		return source.isFireDamage();
	}

	@Override
	public float changeDamage(DamageSource source, float damage) {
		return damage * 0.25F;
	}
}
