package net.spectre.vamp.monsters;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.registries.ForgeRegistryEntry;

import java.util.function.Function;

public class MonsterType extends ForgeRegistryEntry<MonsterType> {

    private final Function<PlayerEntity, IMonsterType> factory;

    public MonsterType(Function<PlayerEntity, IMonsterType> factory){
        this.factory = factory;
    }

    public IMonsterType create(PlayerEntity player){
        return this.factory.apply(player);
    }

}
