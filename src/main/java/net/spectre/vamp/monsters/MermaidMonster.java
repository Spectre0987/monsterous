package net.spectre.vamp.monsters;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.DamageSource;
import net.spectre.vamp.powers.Powers;

public class MermaidMonster extends BaseMonster implements ImFoodRestricted, ICantWearAllArmor{

    public static final int MAX_TIME_OUT_OF_WATER = 200;
    private int timeOutOfWater;

    public MermaidMonster(PlayerEntity player) {
        super(player);
    }

    @Override
    public void initPowers() {
        this.registerPower(Powers.MERMAID_FEED.get());
    }

    @Override
    public void tick() {

        //Handle dryness
        if(!this.isWet()){
            ++this.timeOutOfWater;
            if(this.timeOutOfWater >= MAX_TIME_OUT_OF_WATER){
                if(player.ticksExisted % 20 == 0)
                    player.attackEntityFrom(DamageSource.DRYOUT, 2);

            }
        }
        else this.timeOutOfWater = 0;

        //Air
        if(this.isWet()){
            player.addPotionEffect(new EffectInstance(Effects.WATER_BREATHING, 10, 0, false, false, false));
        }

        if(!player.world.getFluidState(player.getPosition()).isTagged(FluidTags.WATER))
            player.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 22, 1, true, false));
        else player.addPotionEffect(new EffectInstance(Effects.SPEED, 22, 1, true, false));
    }

    @Override
    public void respawn() {
        this.timeOutOfWater = 0;
    }

    public boolean isWet(){
        return player.isInWaterRainOrBubbleColumn();
    }

    @Override
    public boolean onFoodEaten(ItemStack itemSnack) {
        return itemSnack.getItem().getFood().isMeat();
    }

    public float getDrynessPercent(){
        return 1.0F - (timeOutOfWater / (float)MAX_TIME_OUT_OF_WATER);
    }

    @Override
    public boolean canWear(ItemStack stack, EquipmentSlotType slot) {
        return slot != EquipmentSlotType.FEET && slot != EquipmentSlotType.LEGS;
    }
}
