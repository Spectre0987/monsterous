package net.spectre.vamp.monsters;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.common.util.INBTSerializable;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.config.Config;
import net.spectre.vamp.network.Network;
import net.spectre.vamp.network.packets.FlightStateMessage;

public class FlyingHandler implements INBTSerializable<CompoundNBT> {

    private int flapCooldownTicks;
    private int flapticks = 0;
    private int landedTicks = 0;
    private Entity player;
    private boolean inFlight = false;
    private int netTicks = 0;

    //Client
    public int jumpedTicks = 0;

    public FlyingHandler(PlayerEntity player){
        this.player = player;
    }

    public void flap(){
        if(this.flapCooldownTicks <= 0){
            this.flapCooldownTicks = 60;
            this.flapticks = 20;

            if(this.isFlying()){
                Vector3d move = player.getLookVec().scale(0.5);
                player.setMotion(player.getMotion().add(move));
            }
        }

    }

    public double getSpeed(){
       return player.getMotion().length();
    }

    public void tick(){
        if(this.flapCooldownTicks > 0){
            --this.flapCooldownTicks;
        }

        if(this.flapticks > 0){
            --this.flapticks;

            Vector3d move = player.getLookVec().scale(0.05);
            player.setMotion(player.getMotion().add(move));
        }

        if(!player.world.isRemote && player.isOnGround()){
            ++this.landedTicks;
            if(landedTicks > 20){
                this.setInFlight(false);
            }
        }
        else this.landedTicks = 0;

        if(this.jumpedTicks > 0)
            --this.jumpedTicks;

        if(this.netTicks > 0) {
            --this.netTicks;
        }

    }

    public boolean isFlapping(){
        return this.flapCooldownTicks <= 0;
    }

    public int getFlapCooldownTicks(){
        return this.flapCooldownTicks;
    }

    public void setFlapCooldownTicks(int ticks){
        this.flapCooldownTicks = ticks;
    }

    /**
     *
     * @return - True if we should be flying
     */
    public boolean jumped(){
        if(this.jumpedTicks > 0)
            return true;
        this.jumpedTicks = Config.Client.JUMP_TIME.get();
        return false;
    }

    public boolean isFlying(){
        return this.inFlight && !this.isCaptured();
    }

    public void setInFlight(boolean flight){
        this.inFlight = flight;
        if(!player.world.isRemote)
            sendToClients();
    }

    public void netAndUpdate() {
        this.netTicks = 140; //7 seconds
        this.sendToClients();
    }

    public void setNetTicks(int netTicks) {
        this.netTicks = netTicks;
    }

    public boolean isCaptured(){
        return this.netTicks > 0;
    }

    public void sendToClients(){
        if(!player.world.isRemote){
            Network.sendToAllTrackingEntity(player, new FlightStateMessage(player.getEntityId(), this.isFlying()));
        }
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putInt("flap_ticks", this.flapticks);
        tag.putInt("flap_cooldown", this.flapCooldownTicks);
        tag.putBoolean("flying", this.inFlight);
        tag.putInt("net_ticks", this.netTicks);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        this.flapticks = tag.getInt("flap_ticks");
        this.flapCooldownTicks = tag.getInt("flap_cooldown");
        this.inFlight = tag.getBoolean("flying");
        this.netTicks = tag.getInt("net_ticks");
    }
}
