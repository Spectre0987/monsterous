package net.spectre.vamp.monsters;

import net.minecraft.entity.CreatureAttribute;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.MathHelper;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.misc.TransformationType;
import net.spectre.vamp.powers.Powers;
import net.spectre.vamp.tags.VItemTags;

public class VampireMonster extends BaseMonster implements ICantBeAttackedByAll{

	private int inSunTicks = 0;
	private int blood = 20;
	private int batTicks;
	
	public VampireMonster(PlayerEntity player) {
		super(player);
	}

	@Override
	public void tick() {
		player.setAir(player.getMaxAir());
		this.player.getFoodStats().setFoodLevel(20);
		
		if(!player.isCreative()) {
			
			//Burn in day
			//Give the player a bit of spawn immunity
			if(Helper.isDay(player.world) && player.ticksExisted > 200) {
				if(player.world.canBlockSeeSky(player.getPosition()) && !player.isInWater() && !this.player.world.isRainingAt(this.player.getPosition())) {
					++this.inSunTicks;
					for(int i = 0; i < 36; ++i) {
						double rad = Math.toRadians(i * 10);
						player.world.addParticle(ParticleTypes.SMOKE, player.getPosX() + Math.sin(rad) * 0.5, player.getPosY() + 0.1, player.getPosZ() + Math.cos(rad) * 0.5, 0, 0.1, 0);
					}
				}
				else this.inSunTicks = 0;

				//Burn
				if(!player.world.isRemote && this.inSunTicks > 40)
					player.attackEntityFrom(DamageSources.SUN, Integer.MAX_VALUE);

			}
			else {
				this.inSunTicks = 0;
			}
			
			//Burn when holding a holy item
			//Every 2 seconds, set the player on fire for two seconds
			//if(Helper.inEitherHand(stack -> stack.getItem().isIn(MonsterTags.Items.HOLY), this.player) && this.player.ticksExisted % 40 == 0) {
			//	this.player.setFire(2);
			//}
			
			//Decrease blood bar
			if(this.player.ticksExisted % 1200 == 0) {
				this.addBlood(-1);
				MonsterCapability.update(this.player);
			}

			//Blood Starve
			if(this.blood <= 0){
				if(player.ticksExisted % 80 == 0)
					player.attackEntityFrom(DamageSource.STARVE, 2);

				this.player.getFoodStats().setFoodLevel(0);
			}

			//Fly when bat
			if(this.getTransformationType() == TransformationType.BAT){
				this.player.abilities.allowFlying = true;
			}
			else player.abilities.allowFlying = false;

		}

		//If in low-light, give night vision
		if(player.world.getLight(player.getPosition().down()) < 7 || !Helper.isDay(player.world))
			player.addPotionEffect(new EffectInstance(Effects.NIGHT_VISION, 305, 0, true, false));

		//Burn when holding holy items
		if(Helper.inEitherHand(stack -> VItemTags.HOLY_ITEMS.contains(stack.getItem()), player))
			player.setFire(5);
	}

	public void setBlood(int blood) {
		this.blood = MathHelper.clamp(blood, 0, 20);
	}
	
	public void addBlood(int blood) {
		this.setBlood(this.blood + blood);
	}
	
	public int getBlood() {
		return this.blood;
	}
	
	@Override
	public void respawn() {
		this.blood = 20;
		MonsterCapability.update(player);
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = super.serializeNBT();
		tag.putInt("sun_ticks", this.inSunTicks);
		tag.putInt("blood", this.blood);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		super.deserializeNBT(tag);
		this.inSunTicks = tag.getInt("sun_ticks");
		this.blood = tag.getInt("blood");
	}

	@Override
	public void initPowers() {
		this.registerPower(Powers.HYPNOTIZE.get());
		this.registerPower(Powers.TURN_INTO_BAT.get());
		this.registerPower(Powers.FEED.get());
	}

	@Override
	public boolean canAttackMe(MobEntity entity) {
		return entity.getCreatureAttribute() != CreatureAttribute.UNDEAD;
	}
}
