package net.spectre.vamp.monsters;

import com.google.common.collect.Lists;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.spectre.vamp.misc.PowerCooldown;
import net.spectre.vamp.misc.TransformationType;
import net.spectre.vamp.powers.Power;
import net.spectre.vamp.powers.TickablePower;

import java.util.List;

public abstract class BaseMonster implements IMonsterType{

	private List<Power> powers = Lists.newArrayList();
	private int powerIndex;
	private TransformationType transformationType = TransformationType.NONE;
	private int transformedTime = 0;
	protected PlayerEntity player;
	private List<PowerCooldown> cooldowns = Lists.newArrayList();
	private TickablePower tickingPower;
	private boolean hasSentData = false;

	private BaseMonster(){}

	public BaseMonster(PlayerEntity player){
		this.player = player;
		this.initPowers();
	}

	@Override
	public Power getSelectedPower() {
		
		if(this.powerIndex >= 0 && this.powerIndex < this.powers.size()) {
			return powers.get(powerIndex);
		}
		return null;
	}

	@Override
	public void cyclePower(int mod) {

		if(this.tickingPower != null){
			this.tickingPower.onStop(false);
			this.tickingPower = null;
		}

		this.powerIndex += mod;
		
		if(this.powerIndex >= powers.size())
			this.powerIndex = 0;
		else if(this.powerIndex < 0)
			this.powerIndex = this.powers.size() - 1;
	}

	@Override
	public void update() {
		//update cooldowns
		for (PowerCooldown cool : cooldowns) {
			cool.tick();
		}
		//tick down transformation
		if (this.transformedTime > 0) {
			--this.transformedTime;
			if (this.transformedTime <= 0)
				this.transformationType = TransformationType.NONE;
		}
		//Tickable powers
		if (this.tickingPower != null) {
			if (this.tickingPower.tick()) {
				this.tickingPower = null;
			}
		}
		//Get rid of armor
		if(this instanceof ICantWearAllArmor){
			for(ItemStack stack : player.getArmorInventoryList()){
				EquipmentSlotType slot = MobEntity.getSlotForItemStack(stack);
				if(!((ICantWearAllArmor)this).canWear(stack, slot)){
					player.setItemStackToSlot(slot, ItemStack.EMPTY);
					player.addItemStackToInventory(stack);
				}
			}
		}
		this.tick();
	}

	@Override
	public List<Power> getPowers() {
		return this.powers;
	}

	public void registerPower(Power power) {
		this.powers.add(power);
	}

	@Override
	public PlayerEntity getPlayer() {
		return this.player;
	}

	public TransformationType getTransformationType(){
		return this.transformationType;
	}

	public void setTransformationType(TransformationType type){
		this.transformationType = type;
		this.transformedTime = type.getTransformationTime();
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("power", this.powerIndex);
		tag.putInt("transformation", this.transformationType.ordinal());

		//Cooldowns
		ListNBT cooldownList = new ListNBT();
		for(PowerCooldown cool : this.cooldowns){
			cooldownList.add(cool.serializeNBT());
		}
		tag.put("cooldowns", cooldownList);

		//Do Flying
		if(this instanceof IFlyingMonster){
			tag.put("flying_handler", ((IFlyingMonster)this).getFlightHandler().serializeNBT());
		}

		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.powerIndex = nbt.getInt("power");
		this.transformationType = TransformationType.values()[nbt.getInt("transformation")];

		//Cooldowns
		this.cooldowns.clear();
		ListNBT cooldownList = nbt.getList("cooldowns", Constants.NBT.TAG_COMPOUND);
		for(INBT base : cooldownList){
			this.cooldowns.add(new PowerCooldown((CompoundNBT)base));
		}

		//If this is a flying creature
		if(this instanceof IFlyingMonster){
			((IFlyingMonster)this).getFlightHandler().deserializeNBT(nbt.getCompound("flying_handler"));
		}
	}

	@Override
	public PowerCooldown getOrCreateCooldown(Power power){
		for(PowerCooldown cool : cooldowns){
			if(cool.matches(power))
				return cool;
		}

		PowerCooldown cooldown = new PowerCooldown(power);
		cooldown.reset();
		this.cooldowns.add(cooldown);
		return cooldown;

	}

	@Override
	public boolean canUsePower(Power power) {
		//True if cooldown is done
		return this.getOrCreateCooldown(power).canUsePower();
	}

	@Override
	public boolean hasSentData() {
		return this.hasSentData;
	}

	@Override
	public void setSentData(boolean data) {
		this.hasSentData = data;
	}

	@Override
	public void onAttackLiving(LivingEntity target) {}

	//Abstract methods
	public abstract void tick();
	public abstract void initPowers();
}
