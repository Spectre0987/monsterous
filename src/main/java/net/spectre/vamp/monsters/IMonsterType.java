package net.spectre.vamp.monsters;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.spectre.vamp.misc.PowerCooldown;
import net.spectre.vamp.misc.TransformationType;
import net.spectre.vamp.powers.Power;

import java.util.List;
import java.util.Optional;

public interface IMonsterType extends INBTSerializable<CompoundNBT>{

	void update();
	void respawn();
	Power getSelectedPower();
	void cyclePower(int mod);
	PlayerEntity getPlayer();
	List<Power> getPowers();
	PowerCooldown getOrCreateCooldown(Power power);
	boolean canUsePower(Power power);
	TransformationType getTransformationType();
	void setTransformationType(TransformationType type);
	boolean hasSentData();
	void setSentData(boolean data);
	void onAttackLiving(LivingEntity target);
	
	default void onRemove() {}

    default <T extends IMonsterType> Optional<T> getAs(Class<T> clazz){
		if(clazz.isInstance(this))
			return Optional.of((T)this);
    	return Optional.empty();
	}
}
