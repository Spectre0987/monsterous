package net.spectre.vamp.monsters;

import net.minecraft.entity.Entity;
import net.minecraft.entity.MobEntity;

public interface ICantBeAttackedByAll {

    boolean canAttackMe(MobEntity entity);
}
