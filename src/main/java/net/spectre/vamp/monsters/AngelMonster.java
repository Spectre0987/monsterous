package net.spectre.vamp.monsters;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.MathHelper;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.powers.Powers;

public class AngelMonster extends BaseMonster implements IFlyingMonster{

	public static final int HEAVEN_Y = 175;

    //From 0-10
    public float purity = 10;
    private final FlyingHandler flyingHandler;

    public AngelMonster(PlayerEntity player){
        super(player);
        this.flyingHandler = new FlyingHandler(player);
    }

    @Override
    public void initPowers() {
        this.registerPower(Powers.SUMMON_CLOUD.get());
        this.registerPower(Powers.CALM.get());
        this.registerPower(Powers.SMITE.get());
    }

    @Override
    public void tick() {

        this.flyingHandler.tick();

    	//Handle Purity
    	if(this.player.getPosY() < HEAVEN_Y) {
    		//Ten seconds of spawn immunity
    		if(this.player.ticksExisted % 40 == 0 && player.ticksExisted > 200) {
    			this.addPurity(-0.009F);
    		}
    	}
    	else {
    		this.addPurity(0.5F);
    	}
    	//Handle purity damage
    	if(this.purity <= 0 && this.player.ticksExisted % 20 == 0)
    		this.player.attackEntityFrom(DamageSources.OUT_OF_CLOUDS, 2F);
    	
    }

    @Override
    public void respawn() {
    	this.setPurity(10, false);
    }
    
    public void addPurity(float purity) {
    	this.setPurity(this.purity + purity, true);
    }
    
    public void setPurity(float purity, boolean sendUpdate) {
    	this.purity = MathHelper.clamp(purity, 0, 10.0F);
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = super.serializeNBT();
        tag.putFloat("purity", this.purity);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
    	super.deserializeNBT(tag);
    	this.purity = tag.getFloat("purity");
    }

	@Override
	public FlyingPose getPose() {
		return this.getFlightHandler().isFlying() ? FlyingPose.FLYING : FlyingPose.LANDED;
	}

	@Override
	public FlyingHandler getFlightHandler() {
		return this.flyingHandler;
	}

	public float getPurityPercent(){
        return this.purity / 10.0F;
    }
}
