package net.spectre.vamp.monsters;

import net.minecraft.util.DamageSource;

public interface IDontTakeNormalDamage {

    boolean blockCompletely(DamageSource source);
    float changeDamage(DamageSource source, float damage);


}
