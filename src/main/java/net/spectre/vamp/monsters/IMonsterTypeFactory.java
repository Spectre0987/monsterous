package net.spectre.vamp.monsters;

import net.minecraft.entity.player.PlayerEntity;

public interface IMonsterTypeFactory<T extends IMonsterType> {

	T create(PlayerEntity player);
}
