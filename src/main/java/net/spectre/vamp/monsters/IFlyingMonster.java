package net.spectre.vamp.monsters;

public interface IFlyingMonster {

	FlyingPose getPose();
    FlyingHandler getFlightHandler();

    enum FlyingPose{
		LANDED,
		FLYING
	}
}
