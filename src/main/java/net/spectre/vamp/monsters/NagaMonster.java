package net.spectre.vamp.monsters;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.spectre.vamp.powers.Powers;

public class NagaMonster extends BaseMonster{

    public NagaMonster(PlayerEntity player) {
        super(player);
    }

    @Override
    public void onAttackLiving(LivingEntity target) {
        target.addPotionEffect(new EffectInstance(Effects.POISON, 120));
    }

    @Override
    public void tick() {

    }

    @Override
    public void initPowers() {
        this.registerPower(Powers.STRIKE.get());
    }

    @Override
    public void respawn() {}
}
