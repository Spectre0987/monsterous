package net.spectre.vamp.monsters;

import net.minecraft.item.ItemStack;

public interface ImFoodRestricted {

    /**
     *
     * @param itemSnack
     * @return - If this can be eaten by the monster
     */
    boolean onFoodEaten(ItemStack itemSnack);

}
