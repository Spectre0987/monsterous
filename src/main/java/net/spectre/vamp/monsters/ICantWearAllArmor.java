package net.spectre.vamp.monsters;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;

public interface ICantWearAllArmor {

    boolean canWear(ItemStack stack, EquipmentSlotType slot);
}
