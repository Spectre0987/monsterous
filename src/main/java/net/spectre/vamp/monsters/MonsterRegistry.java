package net.spectre.vamp.monsters;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.spectre.vamp.Monsterous;

import java.util.function.Supplier;

public class MonsterRegistry {

    public static final DeferredRegister<MonsterType> MONSTER_TYPES = DeferredRegister.create(MonsterType.class, Monsterous.MODID);
    public static final Supplier<IForgeRegistry<MonsterType>> REGISTRY = MONSTER_TYPES.makeRegistry("monsters", () -> new RegistryBuilder<MonsterType>().setMaxID(Integer.MAX_VALUE - 1));


    public static final RegistryObject<MonsterType> NONE = MONSTER_TYPES.register("none", () -> new MonsterType(player -> null));
    public static final RegistryObject<MonsterType> VAMPIRE = MONSTER_TYPES.register("vampire", () -> new MonsterType(VampireMonster::new));
    public static final RegistryObject<MonsterType> DEMON = MONSTER_TYPES.register("demon", () -> new MonsterType(DemonMonster::new));
    public static final RegistryObject<MonsterType> ANGEL = MONSTER_TYPES.register("angel", () -> new MonsterType(AngelMonster::new));
    public static final RegistryObject<MonsterType> MERMAID = MONSTER_TYPES.register("mermaid", () -> new MonsterType(MermaidMonster::new));
    public static final RegistryObject<MonsterType> NAGA = MONSTER_TYPES.register("naga", () -> new MonsterType(NagaMonster::new));


    public static MonsterType getFromString(String type) {

        ResourceLocation loc = new ResourceLocation(type.contains(":") ? type : Monsterous.MODID + ":" + type);

        MonsterType monster = REGISTRY.get().getValue(loc);
        return monster == null ? NONE.get() : monster;

    }
}
