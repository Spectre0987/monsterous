package net.spectre.vamp.powers;

import net.minecraft.entity.player.PlayerEntity;

public abstract class TickablePower {

    private PlayerEntity player;
    private int time;

    public TickablePower(PlayerEntity player, int time){
        this.player = player;
        this.time = time;
    }

    public PlayerEntity getPlayer(){
        return this.player;
    }

    public abstract void onStart();
    public abstract void onStop(boolean complete);
    public abstract void doPower();

    /**
     *
     * @return true if power is done
     */
    public boolean tick(){
        if(this.time >= 0){
            --this.time;
            if(time <= 0){
                this.onStop(true);
                return true;
            }
        }
        this.doPower();
        return false;
    }

}
