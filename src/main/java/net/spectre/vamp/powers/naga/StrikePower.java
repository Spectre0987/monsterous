package net.spectre.vamp.powers.naga;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.vector.Vector3d;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.powers.Power;

public class StrikePower extends Power {

    public StrikePower() {
        super(200, "naga/strike");
    }

    @Override
    public boolean run(PlayerEntity player, IMonsterType type) {

        double power = 1.5;
        Vector3d motion = new Vector3d(player.getLookVec().x * power, 0.5, player.getLookVec().z * power);
        player.setMotion(player.getMotion().add(motion));

        return true;
    }

    @Override
    public boolean canUsePower(PlayerEntity player, IMonsterType type) {
        return true;
    }
}
