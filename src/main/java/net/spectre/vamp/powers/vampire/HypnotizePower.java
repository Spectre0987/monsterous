package net.spectre.vamp.powers.vampire;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.VampireMonster;
import net.spectre.vamp.powers.Power;

public class HypnotizePower extends Power{

	public HypnotizePower() {
		super(1200, "eye");
	}

	@Override
	public boolean run(PlayerEntity player, IMonsterType type) {
		
		Entity target = Helper.rayTraceEntity(player, 6);
		
		if(target instanceof LivingEntity) {
			((LivingEntity)target).addPotionEffect(new EffectInstance(Effects.SLOWNESS, 200, 100, false, true));
			return true;
		}
		return false;
	}

	@Override
	public boolean canUsePower(PlayerEntity player, IMonsterType type) {
		return false;
	}

}
