package net.spectre.vamp.powers.vampire;

import net.minecraft.entity.CreatureAttribute;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.VampireMonster;
import net.spectre.vamp.powers.Power;

public class VampireFeedPower extends Power {

    public VampireFeedPower() {
        super(0, "fangs");
    }

    @Override
    public boolean run(PlayerEntity player, IMonsterType type) {

        if(type instanceof VampireMonster){

            VampireMonster vampireData = (VampireMonster) type;

            Entity e = Helper.rayTraceEntity(player, 5);

            if(e instanceof LivingEntity){
                if(((LivingEntity) e).getCreatureAttribute() != CreatureAttribute.UNDEAD){

                    vampireData.addBlood(4);
                    e.attackEntityFrom(DamageSources.VAMPIRE, 2);

                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean canUsePower(PlayerEntity player, IMonsterType type) {
        return true;
    }
}
