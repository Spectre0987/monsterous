package net.spectre.vamp.powers;

import net.minecraft.entity.player.PlayerEntity;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.MermaidMonster;

public class LurePower extends Power {

    public LurePower() {
        super(2400, "mermaid/lure");
    }

    @Override
    public boolean run(PlayerEntity player, IMonsterType type) {
        return false;
    }

    @Override
    public boolean canUsePower(PlayerEntity player, IMonsterType type) {
        return false;
    }
}
