package net.spectre.vamp.powers.angel;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.powers.Power;

public class SmitePower extends Power {

    public static final float DAMAGE = 15.0F;
    public SmitePower() {
        super(200, "angel/smite");
    }

    @Override
    public boolean run(PlayerEntity player, IMonsterType type) {
        Entity e = Helper.rayTraceEntity(player, 3);
        if(e instanceof LivingEntity){
            LivingEntity live = (LivingEntity)e;

            e.getCapability(Capabilities.SMITE).ifPresent(cap -> {

                if(live.getHealth() > DAMAGE)
                    e.attackEntityFrom(DamageSources.SMITE, DAMAGE);
                else cap.smite();


            });
            return true;
        }
        return false;
    }

    @Override
    public boolean canUsePower(PlayerEntity player, IMonsterType type) {
        return true;
    }
}
