package net.spectre.vamp.powers.angel;

import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.monsters.AngelMonster;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.powers.Power;

public class SummonCloudPower extends Power {

    public SummonCloudPower() {
        super(200, "angel/cloud");
    }

    @Override
    public boolean run(PlayerEntity player, IMonsterType type) {

       int dist = 4;
       BlockPos radius = new BlockPos(dist, 3, dist);
       BlockPos.getAllInBox(player.getPosition().subtract(radius), player.getPosition().add(new BlockPos(dist, -1, dist))).forEach(pos -> {
           if(pos.withinDistance(player.getPosition(), dist)) {
            	if(player.world.getBlockState(pos).isAir())
                    player.world.setBlockState(pos, VBlocks.CLOUD.get().getDefaultState());
           }
       });

        return true;
    }

    @Override
    public boolean canUsePower(PlayerEntity player, IMonsterType type) {
        return true;
    }
}
