package net.spectre.vamp.powers.angel;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.spectre.vamp.monsters.AngelMonster;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.powers.Power;

public class CalmPower extends Power{

	public CalmPower() {
		super(200, "angel/calm");
	}

	@Override
	public boolean run(PlayerEntity player, IMonsterType type) {
		for(LivingEntity entity : player.world.getEntitiesWithinAABB(LivingEntity.class, player.getBoundingBox().grow(16))) {
			entity.setRevengeTarget(null);
			if(entity instanceof MobEntity)
				((MobEntity)entity).setAttackTarget(null);
		}
		return true;
	}

	@Override
	public boolean canUsePower(PlayerEntity player, IMonsterType type) {
		return true;
	}

	
	
}
