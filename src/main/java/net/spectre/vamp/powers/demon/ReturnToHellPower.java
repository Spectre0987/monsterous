package net.spectre.vamp.powers.demon;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.misc.BlankTeleporter;
import net.spectre.vamp.monsters.DemonMonster;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.powers.Power;

import javax.annotation.Nullable;

public class ReturnToHellPower extends Power{

	public static final TranslationTextComponent TRANS_NOT_SAFE = new TranslationTextComponent(Monsterous.MODID + ".power.demon.hell.unsafe");
	public ReturnToHellPower() {
		super(2400, "demon/hell");
	}

	@Override
	public boolean run(PlayerEntity player, IMonsterType type) {

		DemonMonster demonData = type.getAs(DemonMonster.class).orElse(null);
		if(demonData == null)
			return false;

		if(!player.world.isRemote) {
			
			ServerWorld world = player.world.getServer().getWorld(demonData.isInHell() ? World.OVERWORLD : World.THE_NETHER);
			return this.teleport(player, world);
		}
		return false;
	}

	private boolean teleport(PlayerEntity player, ServerWorld world){
		BlockPos safe = this.findSafePlace(world, player.getPosition());

		if(safe == null) {
			player.sendStatusMessage(TRANS_NOT_SAFE, true);
			return false;
		}

		//teleport touching as well
		for(ServerPlayerEntity e : player.getEntityWorld().getEntitiesWithinAABB(ServerPlayerEntity.class, player.getBoundingBox())){
			e.teleport(world, safe.getX() + 0.5, safe.getY() + 1, safe.getZ() + 0.5, e.cameraYaw, e.rotationPitch);
		}

		((ServerPlayerEntity)player).teleport(world, safe.getX() + 0.5, safe.getY() + 1, safe.getZ() + 0.5, player.cameraYaw, player.rotationPitch);
		return true;
	}

	@Nullable
	private BlockPos findSafePlace(ServerWorld world, BlockPos pos) {
		
		BlockPos current = pos;
		//30 tries to find a safe place
		for(int i = 0; i < 30; ++i) {
			
			//look from y down to 0
			for(int y = pos.getY(); y > 0; --y){
				if(this.isPosSafe(world, current))
					return current;
				else current = new BlockPos(current.getX(), y, current.getZ());
			}
			
			//Randomize if failed
			current = new BlockPos(current.getX() + (world.rand.nextDouble() * 16) - 8, pos.getY(), current.getZ() + (world.rand.nextDouble() * 16) - 8);
			
		}
		
		return null;
	}
	
	private boolean isPosSafe(ServerWorld world, BlockPos pos) {
		
		//If we cannot collide with this block
		if(!world.getBlockState(pos).isAir(world, pos))
			return false;
		
		//And we cannot collide with the one above
		if(!world.getBlockState(pos.up()).isAir(world, pos))
			return false;
		
		//And we CAN collide with the one below
		if(world.getBlockState(pos.down()).getCollisionShape(world, pos.down()).isEmpty())
			return false;
		
		return true;
		
	}

	@Override
	public boolean canUsePower(PlayerEntity player, IMonsterType type) {
		return true;
	}

}
