package net.spectre.vamp.powers.demon;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.math.AxisAlignedBB;
import net.spectre.vamp.monsters.DemonMonster;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.powers.Power;

public class HuntPower extends Power{

	public static final int COST = 20;
	
	public HuntPower() {
		super(1200, "eye");
	}

	@Override
	public boolean run(PlayerEntity player, IMonsterType type) {
		
		if(player.world.isRemote) {
			for(LivingEntity ent : player.world.getEntitiesWithinAABB(LivingEntity.class, new AxisAlignedBB(player.getPosition()).grow(64.0))) {
				ent.addPotionEffect(new EffectInstance(Effects.GLOWING, 100));
			}
		}
		
		return false;
	}

	@Override
	public boolean canUsePower(PlayerEntity player, IMonsterType type) {
		return true;
	}

}
