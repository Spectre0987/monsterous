package net.spectre.vamp.powers;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.monsters.IMonsterType;

public class ForcePower extends Power{

    public ForcePower() {
        super(500, "textures/force_push");
    }

    @Override
    public boolean run(PlayerEntity player, IMonsterType type) {

        Entity target = Helper.rayTraceEntity(player, 16);

        if(target != null && player != null){
            Helper.forceEntity(target, player.getPositionVec(), 10.0F, true);
            return true;
        }

        return false;
    }

    @Override
    public boolean canUsePower(PlayerEntity player, IMonsterType type) {
        return true;
    }
}
