package net.spectre.vamp.powers.mermaid;

import net.minecraft.entity.player.PlayerEntity;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.MermaidMonster;
import net.spectre.vamp.powers.Power;

public class MermaidTransformPower extends Power {

    public MermaidTransformPower() {
        super(13000, "mermaid/transform");
    }

    @Override
    public boolean run(PlayerEntity player, IMonsterType type) {
        return false;
    }

    @Override
    public boolean canUsePower(PlayerEntity player, IMonsterType type) {
        return false;
    }
}
