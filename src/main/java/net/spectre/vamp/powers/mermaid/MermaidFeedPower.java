package net.spectre.vamp.powers.mermaid;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.powers.Power;
import net.spectre.vamp.tags.VEntityTags;

public class MermaidFeedPower extends Power {

    public static final ItemStack MEAT = new ItemStack(Items.COOKED_BEEF);

    public MermaidFeedPower() {
        super(200, "fangs");
    }

    @Override
    public boolean run(PlayerEntity player, IMonsterType type) {

        Entity e = Helper.rayTraceEntity(player, 2);

        if(e != null && e.getType() != null && VEntityTags.MERMAID_EAT.contains(e.getType())){

            e.attackEntityFrom(DamageSources.MERMAID_CONSUME, 4);
            player.getFoodStats().consume(MEAT.getItem(), MEAT);

            return true;
        }

        return false;
    }

    @Override
    public boolean canUsePower(PlayerEntity player, IMonsterType type) {
        return true;
    }
}
