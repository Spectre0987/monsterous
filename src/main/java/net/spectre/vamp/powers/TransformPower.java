package net.spectre.vamp.powers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.misc.TransformationType;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.network.Network;
import net.spectre.vamp.network.packets.TransformMessage;

public class TransformPower extends Power{

    private TransformationType type;

    public TransformPower(int maxCooldown, String tex, TransformationType type){
        super(maxCooldown, tex);
        this.type = type;
    }

    @Override
    public boolean run(PlayerEntity player, IMonsterType type) {

        if(type != null){

            type.setTransformationType(type.getTransformationType() == TransformationType.NONE ? this.type : TransformationType.NONE);
            if(!player.world.isRemote){
                Network.sendToAllTrackingEntity(player, new TransformMessage(player.getEntityId(), type.getTransformationType()));
            }
            player.recalculateSize();
            return true;

        }

        return false;
    }

    @Override
    public boolean canUsePower(PlayerEntity player, IMonsterType type) {
        return true;
    }
}
