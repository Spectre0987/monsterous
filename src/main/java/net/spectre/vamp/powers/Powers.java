package net.spectre.vamp.powers;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.misc.TransformationType;
import net.spectre.vamp.powers.angel.CalmPower;
import net.spectre.vamp.powers.angel.SmitePower;
import net.spectre.vamp.powers.angel.SummonCloudPower;
import net.spectre.vamp.powers.demon.HuntPower;
import net.spectre.vamp.powers.demon.ReturnToHellPower;
import net.spectre.vamp.powers.mermaid.MermaidFeedPower;
import net.spectre.vamp.powers.naga.StrikePower;
import net.spectre.vamp.powers.vampire.HypnotizePower;
import net.spectre.vamp.powers.vampire.VampireFeedPower;

import java.util.function.Supplier;

public class Powers {

	public static final DeferredRegister<Power> POWERS = DeferredRegister.create(Power.class, Monsterous.MODID);
	public static final Supplier<IForgeRegistry<Power>> POWER_REGISTRY = POWERS.makeRegistry("powers", () -> new RegistryBuilder<Power>().setMaxID(Integer.MAX_VALUE - 1));
	
	//Demon Powers
	public static final RegistryObject<Power> RETURN_TO_HELL = POWERS.register("return_to_hell", ReturnToHellPower::new);
	public static final RegistryObject<Power> HUNT_EYE = POWERS.register("hunt_eye", HuntPower::new);
	
	//Vampire Powers
	public static final RegistryObject<Power> HYPNOTIZE = POWERS.register("hypnotize", HypnotizePower::new);
	public static final RegistryObject<Power> TURN_INTO_BAT = POWERS.register("bat_transform", () -> new TransformPower(200, "vampire/bat", TransformationType.BAT));
	public static final RegistryObject<Power> FEED = POWERS.register("feed", VampireFeedPower::new);
	
	//Angel RegistryObject<Power>
	public static final RegistryObject<Power> SUMMON_CLOUD = POWERS.register("summon_cloud", SummonCloudPower::new);
	public static final RegistryObject<Power> CALM = POWERS.register("calm", CalmPower::new);
	public static final RegistryObject<Power> SMITE = POWERS.register("smite", SmitePower::new);

	//Mermaid Powers
	public static final RegistryObject<Power> LURE = POWERS.register("lure", LurePower::new);
	public static final RegistryObject<Power> MERMAID_FEED = POWERS.register("mermaid_feed", MermaidFeedPower::new);

	//Naga Powers
	public static final RegistryObject<Power> STRIKE = POWERS.register("strike", StrikePower::new);

	//Generic Powers
	public static final RegistryObject<Power> FORCE_PUSH = POWERS.register("force_push", ForcePower::new);
}
