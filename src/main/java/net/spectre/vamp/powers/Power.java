package net.spectre.vamp.powers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.monsters.IMonsterType;

public abstract class Power extends ForgeRegistryEntry<Power> {

	ResourceLocation iconPath;
	int maxCooldown;
	
	public Power(int maxCooldown) {
		this.maxCooldown = maxCooldown;
	}
	
	public Power(int maxCooldown, String power) {
		this(maxCooldown);
		this.setIconPath(Helper.createRL(power));
	}
	
	public void setIconPath(ResourceLocation loc) {
		this.iconPath = new ResourceLocation(loc.getNamespace(), "textures/powers/" + loc.getPath() + ".png");
	}
	
	public int getMaxCoolDown() {
		return this.maxCooldown;
	}
	
	public ResourceLocation getIconPath() {
		return this.iconPath;
	}

	public abstract boolean run(PlayerEntity player, IMonsterType type);
	public abstract boolean canUsePower(PlayerEntity player, IMonsterType type);
}
