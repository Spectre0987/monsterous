package net.spectre.vamp.compat;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.recipes.Recipes;

@JeiPlugin
public class JEICompat implements IModPlugin {

    public static final ResourceLocation ID = Helper.createRL("main");

    public JEICompat(){}

    @Override
    public void registerCategories(IRecipeCategoryRegistration registration) {
        registration.addRecipeCategories(new ContractCrafting(registration.getJeiHelpers().getGuiHelper()));
    }

    @Override
    public void registerRecipes(IRecipeRegistration reg) {
        Minecraft mc = Minecraft.getInstance();

       reg.addRecipes(mc.world.getRecipeManager().getRecipesForType(Recipes.CONTRACT_TYPE), ContractCrafting.ID);

    }

    @Override
    public ResourceLocation getPluginUid() {
        return ID;
    }
}
