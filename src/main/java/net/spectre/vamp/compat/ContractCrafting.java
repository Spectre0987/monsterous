package net.spectre.vamp.compat;

import com.google.common.collect.Lists;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.items.VItems;
import net.spectre.vamp.recipes.ContractRecipe;

public class ContractCrafting implements IRecipeCategory<ContractRecipe> {

    public static final ResourceLocation ID = Helper.createRL("monster_crafting");

    public final IDrawable background;
    public final IDrawable icon;

    public  ContractCrafting(IGuiHelper helper){
        this.background = helper.createDrawable(Helper.createRL("textures/jei/contract.png"), 0, 0, 116, 54);
        this.icon = helper.createDrawableIngredient(new ItemStack(VItems.CONTRACT.get()));
    }

    @Override
    public ResourceLocation getUid() {
        return ID;
    }

    @Override
    public Class<? extends ContractRecipe> getRecipeClass() {
        return ContractRecipe.class;
    }

    @Override
    public String getTitle() {
        return "Contract Crafting";
    }

    @Override
    public IDrawable getBackground() {
        return this.background;
    }

    @Override
    public IDrawable getIcon() {
        return this.icon;
    }

    @Override
    public void setIngredients(ContractRecipe recipe, IIngredients ingredients) {
        ingredients.setInputIngredients(recipe.getIngredients());
        ingredients.setOutput(VanillaTypes.ITEM, recipe.getCraftingResult(null));
    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, ContractRecipe recipe, IIngredients ingredients) {

        int index = 0;
        for(Ingredient ing : recipe.getIngredients()){

            recipeLayout.getItemStacks().init(index, true, ((index) / 3) * 18, ((index % 3) * 18));
            recipeLayout.getItemStacks().set(index, Lists.newArrayList(ing.getMatchingStacks()));

            ++index;
        }

        recipeLayout.getItemStacks().init(index, false, 94, 18);
        recipeLayout.getItemStacks().set(index, recipe.getCraftingResult(null));
    }
}
