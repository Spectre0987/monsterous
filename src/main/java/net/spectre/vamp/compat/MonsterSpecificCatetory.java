package net.spectre.vamp.compat;

import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.Constants;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.recipes.MonsterSpecificRecipe;

public class MonsterSpecificCatetory implements IRecipeCategory<MonsterSpecificRecipe> {

    public static final ResourceLocation ID = Helper.createRL("monster_specific");

    @Override
    public ResourceLocation getUid() {
        return null;
    }

    @Override
    public Class<? extends MonsterSpecificRecipe> getRecipeClass() {
        return null;
    }

    @Override
    public String getTitle() {
        return Constants.Translations.JEI_MONSTER_SPECIFIC.getKey();
    }

    @Override
    public IDrawable getBackground() {
        return null;
    }

    @Override
    public IDrawable getIcon() {
        return null;
    }

    @Override
    public void setIngredients(MonsterSpecificRecipe recipe, IIngredients ingredients) {

    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, MonsterSpecificRecipe recipe, IIngredients ingredients) {

    }
}
