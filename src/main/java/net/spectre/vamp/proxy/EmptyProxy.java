package net.spectre.vamp.proxy;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

public class EmptyProxy implements IProxy {

    @Override
    public World getClientWorld() {
        return null;
    }

    @Override
    public PlayerEntity getClientPlayer() {
        return null;
    }
}
