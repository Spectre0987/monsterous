package net.spectre.vamp;

import net.minecraft.util.text.TranslationTextComponent;

public class Constants {

    public static final int MAX_LIGHT = 15728880;


    public static class Translations{
        public static final TranslationTextComponent JEI_MONSTER_SPECIFIC = new TranslationTextComponent("jei.monsterous.recipe.monster_specific");
    }
}
