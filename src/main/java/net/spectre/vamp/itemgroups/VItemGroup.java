package net.spectre.vamp.itemgroups;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.items.VItems;

public class VItemGroup {

    public static final ItemGroup MAIN = new ItemGroup(Monsterous.MODID) {

        @Override
        public ItemStack createIcon() {
            return new ItemStack(VItems.CONTRACT_FILLED.get());
        }
    };
}
