package net.spectre.vamp.constants;

import com.google.common.collect.Lists;
import net.minecraft.entity.ai.attributes.AttributeModifier;

import java.util.List;
import java.util.UUID;

public class AttributeConstants {

    public static final List<AttributeModifier> DEMON_BOONS = Lists.newArrayList();

    public static final AttributeModifier DEMONIC_HASTE = new AttributeModifier(UUID.fromString("9fb887ff-66a4-4891-ab94-cd0f248c300c"), "Demonic Haste", 1.75, AttributeModifier.Operation.MULTIPLY_BASE);


    static{
        DEMON_BOONS.add(DEMONIC_HASTE);
    }
}
