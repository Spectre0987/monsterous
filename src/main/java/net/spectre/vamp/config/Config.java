package net.spectre.vamp.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.spectre.vamp.Monsterous;

public class Config {
	
	public static final ForgeConfigSpec SERVER = buildServerConfig();
	public static final ForgeConfigSpec CLIENT = buildClientConfig();
	
	public static ForgeConfigSpec buildServerConfig() {
		ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();
		
		builder.comment("Monsterous").push(Monsterous.MODID);
		
		builder.comment("Damage modifier for damage vampires are not impervious to")
		.define("damage", 1.25F);
		
		builder.pop();
		
		return builder.build();
	}

	public static ForgeConfigSpec buildClientConfig(){

		ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();

		builder.comment("Monstrous").push(Monsterous.MODID);

		Client.JUMP_TIME = builder.comment("The Time you have to hit the jump button the second time to fly")
				.define("jump_time", 10);

		builder.pop();
		return builder.build();
	}

	public static class Client{
		public static ForgeConfigSpec.ConfigValue<Integer> JUMP_TIME;
	}
}
