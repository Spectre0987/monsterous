package net.spectre.vamp.trades;

import com.google.common.collect.Lists;
import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.items.VItems;
import net.spectre.vamp.misc.NonRegistry;

import java.util.ArrayList;

@Mod.EventBusSubscriber(modid = Monsterous.MODID)
public class MonsterousVillagerTrades {

    public static final ArrayList<NonRegistry<LeveledTrade>> TRADES = Lists.newArrayList();

    public static final NonRegistry<LeveledTrade> CHALLICE = register(NonRegistry.of(() -> new LeveledTrade(VillagerProfession.CLERIC, 4, new ItemTrade(new ItemStack(VItems.CHALICE_OF_PURITY.get()), 32, 1, 1, 25))));

    public static NonRegistry<LeveledTrade> register(NonRegistry<LeveledTrade> reg){
        TRADES.add(reg);
        return reg;
    }

    @SubscribeEvent
    public static void onTradeEvent(VillagerTradesEvent event){
        for(NonRegistry<LeveledTrade> trade : TRADES){
            //If correct villager job
            if(trade.get().getType() == event.getType()){
                event.getTrades().get(trade.get().getLevel()).add(trade.get().getTrade());
            }
        }
    }
}
