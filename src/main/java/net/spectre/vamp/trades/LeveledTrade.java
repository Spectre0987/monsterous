package net.spectre.vamp.trades;

import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.entity.merchant.villager.VillagerTrades;

public class LeveledTrade {

    private VillagerProfession type;
    private VillagerTrades.ITrade trade;
    private int level;


    public LeveledTrade(VillagerProfession type, int level, VillagerTrades.ITrade trade){
        this.type = type;
        this.level = level;
        this.trade = trade;
    }
    public int getLevel(){
        return this.level;
    }

    public VillagerTrades.ITrade getTrade(){
        return this.trade;
    }

    public VillagerProfession getType(){
        return this.type;
    }

}
