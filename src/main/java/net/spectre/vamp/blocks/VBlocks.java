package net.spectre.vamp.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.FlowerBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.potion.Effects;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.properties.Prop;

public class VBlocks {

	public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Monsterous.MODID);
	
	public static final RegistryObject<Block> DEMON_TRAP = BLOCKS.register("demon_trap", () -> new ChalkBlock(Prop.Blocks.CHALK));
	public static final RegistryObject<Block> DEMON_ROSE = BLOCKS.register("demon_rose", () -> new FlowerBlock(Effects.BLINDNESS, 0, Block.Properties.create(Material.PLANTS).doesNotBlockMovement().sound(SoundType.PLANT)));
	public static final RegistryObject<Block> COFFIN = BLOCKS.register("coffin", CoffinBlock::new);
	public static final RegistryObject<SummonCircleBlock> SUMMON_CIRCLE = BLOCKS.register("summon_circle", SummonCircleBlock::new);
	public static final RegistryObject<CloudBlock> CLOUD = BLOCKS.register("cloud", CloudBlock::new);
}
