package net.spectre.vamp.blocks;

import com.google.common.collect.Lists;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.cap.IMonster;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.items.DemonicContract;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.properties.Prop;

import javax.annotation.Nullable;
import java.util.List;

public class SummonCircleBlock extends ChalkBlock{

    public static final TranslationTextComponent NEEDS_DIRT = new TranslationTextComponent("");
    public static final TranslationTextComponent INVALID_CONTRACT = new TranslationTextComponent("");

    public SummonCircleBlock() {
        super(Prop.Blocks.CHALK);
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {

        if(!worldIn.isRemote && this.isCorrect(worldIn, pos, player)){
           List<ServerPlayerEntity> demons = this.getAllDemons(worldIn.getServer());

           ServerPlayerEntity demon = demons.get(worldIn.rand.nextInt(demons.size()));

           double x = pos.getX() + 0.5, y = pos.getY() + 1, z = pos.getZ() + 0.5;
            //If in other dimension
            if(!demon.world.getDimensionKey().getLocation().equals(worldIn.getDimensionKey().getLocation())){
                demon.teleport((ServerWorld)worldIn, x, y, z, Helper.getRotationFromDir(player.getHorizontalFacing().getOpposite()), 0);
            }
            else{
                //If in same dimension
                demon.attemptTeleport(x, y, z, false);
            }

            //Give Contract
            InventoryHelper.spawnItemStack(worldIn, x, y, z, this.getContract(worldIn, pos, true));

        }

        return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
    }

    private boolean isCorrect(World world, BlockPos pos, @Nullable PlayerEntity player){
        //If there's a solid block under this
        if(world.getBlockState(pos.down(1)).isSolid()){
            ItemStack stack = this.getContract(world, pos, false);
            if(!stack.isEmpty())
                return true;
            this.warn(player, INVALID_CONTRACT);
        }
        else this.warn(player, NEEDS_DIRT);
        return false;
    }

    private void warn(@Nullable PlayerEntity player, TranslationTextComponent trans){
        if(player != null){
            player.sendStatusMessage(trans, true);
        }
    }

    private ItemStack getContract(World world, BlockPos pos, boolean remove){
        TileEntity te = world.getTileEntity(pos.down(2));
        if(te instanceof ChestTileEntity){
            ItemStack stack = this.getContract((ChestTileEntity)te, remove);
            if(!stack.isEmpty())
                return stack;
        }
        return ItemStack.EMPTY;
    }

    private ItemStack getContract(IInventory inv, boolean remove){
        for(int i = 0; i < inv.getSizeInventory(); ++i){
            ItemStack stack = inv.getStackInSlot(i);

            if(stack.getItem() instanceof DemonicContract){
                if(DemonicContract.getContract(stack) != null){
                    ItemStack newStack = stack.copy();
                    if(remove)
                        inv.setInventorySlotContents(i, ItemStack.EMPTY);
                    return newStack;
                }
            }
        }
        return ItemStack.EMPTY;
    }

    private List<ServerPlayerEntity> getAllDemons(MinecraftServer server){
        List<ServerPlayerEntity> players = Lists.newArrayList(server.getPlayerList().getPlayers());
        players.removeIf(player -> {
            IMonster monster = player.getCapability(Capabilities.MONSTER).orElse(null);

            return monster == null || monster.getMonsterType() != MonsterRegistry.DEMON.get();
        });
        return players;
    }

}
