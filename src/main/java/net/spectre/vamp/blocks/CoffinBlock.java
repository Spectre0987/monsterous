package net.spectre.vamp.blocks;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BedPart;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;
import net.spectre.vamp.blocks.voxelShapes.DirectionalVoxelShape;

import java.util.stream.Stream;

public class CoffinBlock extends TileBlock{
	
	public static final BooleanProperty OPEN = BooleanProperty.create("open");

	public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(createShape());
	public static final DirectionalVoxelShape SHAPE_FOOT = new DirectionalVoxelShape(createFootShape());
	
	public CoffinBlock() {
		super(Block.Properties.create(Material.WOOD)
				.hardnessAndResistance(3)
				.harvestTool(ToolType.AXE)
				.sound(SoundType.WOOD));
		this.setDefaultState(this.stateContainer.getBaseState().with(BedBlock.PART, BedPart.FOOT));
	}

	@Override
	public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote){
			if(player.isSneaking() && handIn == Hand.MAIN_HAND) {
				worldIn.setBlockState(pos, state.with(OPEN, !state.get(OPEN)));
			}
			else if(state.get(OPEN) && !player.isSneaking()) {
				player.trySleep(pos).ifLeft((result) -> {
					if (result != null) {
						player.sendStatusMessage(result.getMessage(), true);
					}

				});
				worldIn.setBlockState(pos, state.with(OPEN, false));
				player.setBedPosition(pos);
			}
		}
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}

	@Override
	public boolean isBed(BlockState state, IBlockReader world, BlockPos pos, Entity player) {
		return true;
	}
	
	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		super.fillStateContainer(builder);
		builder.add(BlockStateProperties.HORIZONTAL_FACING, BedBlock.OCCUPIED, BedBlock.PART, OPEN);
	}

	
	
	@Override
	public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
		super.onReplaced(state, worldIn, pos, newState, isMoving);
		
		if(state.getBlock() != newState.getBlock()) {
			
			BedPart part = state.get(BedBlock.PART);
			Direction dir = state.get(BlockStateProperties.HORIZONTAL_FACING);
			
			if(part == BedPart.HEAD)
				dir = dir.getOpposite();
			
			worldIn.setBlockState(pos.offset(dir), Blocks.AIR.getDefaultState());
		}
		
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing());
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		DirectionalVoxelShape shape = SHAPE;
		
		if(state.get(BedBlock.PART) == BedPart.FOOT)
			shape = SHAPE_FOOT;
		
		return shape.getBy(state.get(BlockStateProperties.HORIZONTAL_FACING));
		
	}
	
	public static VoxelShape createShape() {
		return Stream.of(
				Block.makeCuboidShape(15, 0, 1, 17, 11, 16),
				Block.makeCuboidShape(1, 0, 1, 15, 2, 16),
				Block.makeCuboidShape(-1, 0, 1, 1, 11, 16),
				Block.makeCuboidShape(-1, 0, -1, 17, 11, 1)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}
	
	public static VoxelShape createFootShape() {
		return Stream.of(
				Block.makeCuboidShape(15, 0, 0, 17, 11, 15),
				Block.makeCuboidShape(1, 0, 0, 15, 2, 15),
				Block.makeCuboidShape(-1, 0, 0, 1, 11, 15),
				Block.makeCuboidShape(-1, 0, 15, 17, 11, 17)
				).reduce((v1, v2) -> {return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);}).get();
	}

}
