package net.spectre.vamp.blocks.voxelShapes;

import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.math.vector.Vector3d;

public class DirectionalVoxelShape {

	private VoxelShape NORTH;
	private VoxelShape EAST;
	private VoxelShape SOUTH;
	private VoxelShape WEST;
	
	public DirectionalVoxelShape(VoxelShape normal) {
		this.NORTH = normal;
		this.EAST = this.rotateVoxelShape(Direction.EAST, normal);
		this.SOUTH = this.rotateVoxelShape(Direction.SOUTH, normal);
		this.WEST = this.rotateVoxelShape(Direction.WEST, normal);
	}
	
	public VoxelShape getBy(Direction dir) {
		switch(dir) {
			default: return NORTH;
			case EAST: return EAST;
			case SOUTH: return SOUTH;
			case WEST: return WEST;
		}
	}
	
	private Vector3d rotateCoord(Direction dir, Vector3d pos) {
		
		switch(dir) {
			default: return pos;
			case EAST: return new Vector3d(pos.z, pos.y, -pos.x);
			case SOUTH: return new Vector3d(-pos.x, pos.y, -pos.z);
			case WEST: return new Vector3d(-pos.z, pos.y, pos.x);
		}
	}
	
	private AxisAlignedBB rotateBB(Direction dir, AxisAlignedBB bb) {
		return new AxisAlignedBB(rotateCoord(dir, new Vector3d(bb.minX - 0.5, bb.minY, bb.minZ - 0.5)), rotateCoord(dir, new Vector3d(bb.maxX - 0.5, bb.maxY, bb.maxZ - 0.5))).offset(0.5, 0, 0.5);
	}
	
	private VoxelShape rotateVoxelShape(Direction dir, VoxelShape shape) {
		
		VoxelShape newShape = VoxelShapes.empty();
		
		for(AxisAlignedBB bb : shape.toBoundingBoxList()) {
			newShape = VoxelShapes.combineAndSimplify(newShape, VoxelShapes.create(this.rotateBB(dir, bb)), IBooleanFunction.OR);
		}
		return newShape;
		
	}
}
