package net.spectre.vamp.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.world.IBlockReader;

public class TileBlock extends Block{

	private TileEntityType<?> type;
	
	public TileBlock(Block.Properties prop) {
		super(prop);
	}
	
	public void setTile(TileEntityType<?> type) {
		this.type = type;
	}

	@Override
	public boolean hasTileEntity(BlockState state) {
		return type != null;
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) {
		return type.create();
	}

	@Override
	public BlockRenderType getRenderType(BlockState state) {
		return BlockRenderType.ENTITYBLOCK_ANIMATED;
	}
	
}
