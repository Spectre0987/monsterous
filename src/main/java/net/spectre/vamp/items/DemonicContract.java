package net.spectre.vamp.items;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.helpers.ClientHelper;
import net.spectre.vamp.items.data.ContractData;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.properties.Prop;

import java.util.List;
import java.util.UUID;

public class DemonicContract extends Item{

	public DemonicContract() {
		super(Prop.Items.ONE.get());
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {

		ItemStack stack = playerIn.getHeldItem(handIn);

		playerIn.getCapability(Capabilities.MONSTER).ifPresent(cap -> {

			ContractData data = getContract(stack);
			//If demon
			if(cap.getMonsterType() == MonsterRegistry.DEMON.get()) {
				//if valid contract
				if(data != null){
					UUID ownerID = data.getOwner();
					//if contract was writen by player
					if(ownerID != null){
						//If player is online and present
						PlayerEntity owner = worldIn.getPlayerByUuid(ownerID);
						if(owner != null) {
							//Consume contract and apply it to player
							data.applyContract(owner);

							ItemStack filled = new ItemStack(VItems.CONTRACT_FILLED.get());
							SignedDemonicContractItem.setSigner(filled, ownerID);
							playerIn.setHeldItem(handIn, filled);
						}
					}
				}
			}
			//If not demon && doesn't have an owner
			else if(data != null && data.getOwner() == null){
				data.setOwner(playerIn.getUniqueID());
				setContract(stack, data);
			}

		});
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		ContractData data = getContract(stack);
		if(data != null && data.getOwner() != null)
			tooltip.add(new StringTextComponent("Bound to: " + ClientHelper.getUsernameOrUUID(data.getOwner())));

		super.addInformation(stack, worldIn, tooltip, flagIn);
	}
	
	public static ContractData getContract(ItemStack stack){
		if(!stack.hasTag() || !stack.getTag().contains("contract"))
			return null;

		return ContractData.read(stack.getTag().getCompound("contract"));

	}

	@Override
	public ITextComponent getDisplayName(ItemStack stack) {
		ContractData data = getContract(stack);
		if(data != null)
			return data.getTrans();
		return super.getDisplayName(stack);
	}

	public static void setContract(ItemStack stack, ContractData data){
		stack.getOrCreateTag().put("contract", data.serialize());
	}

}
