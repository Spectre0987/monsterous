package net.spectre.vamp.items;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.properties.Prop;

public class ChaliceItem extends Item {

    public ChaliceItem(Properties properties) {
        super(properties);
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity entityLiving) {

        if(isFilled(stack)){
            entityLiving.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
                if(cap.getMonsterType() != MonsterRegistry.NONE.get()){
                    cap.setMonsterType(MonsterRegistry.NONE.get());
                    entityLiving.setHealth(0.5F);
                    entityLiving.addPotionEffect(new EffectInstance(Effects.NAUSEA, 30 * 20));
                }
            });
            return new ItemStack(VItems.CHALICE_OF_PURITY.get());
        }
        return super.onItemUseFinish(stack, worldIn, entityLiving);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        ItemStack stack = playerIn.getHeldItem(handIn);
        if(stack.getItem() == VItems.CHALICE_OF_PURITY_FILLED.get())
            playerIn.setActiveHand(handIn);
        else{
            //If empty
            Vector3d start = playerIn.getPositionVec().add(0, playerIn.getEyeHeight(), 0);

            BlockRayTraceResult result = worldIn.rayTraceBlocks(new RayTraceContext(start, start.add(playerIn.getLookVec().scale(6)), RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.SOURCE_ONLY, playerIn));

            if(result != null && !worldIn.isRemote){
                BlockState state = worldIn.getBlockState(result.getPos());
                if(state.getFluidState().isTagged(FluidTags.WATER)){
                    worldIn.setBlockState(result.getPos(), Blocks.AIR.getDefaultState());
                    playerIn.setHeldItem(handIn, new ItemStack(VItems.CHALICE_OF_PURITY_FILLED.get()));
                }
            }


        }
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return isFilled(stack) ? UseAction.DRINK : UseAction.NONE;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return isFilled(stack) ? 60 : 0;
    }

    public boolean isFilled(ItemStack stack){
        return stack.getItem() == VItems.CHALICE_OF_PURITY_FILLED.get();
    }

    public static ChaliceItem create(){
        return new ChaliceItem(Prop.Items.ONE.get());
    }

    public static ChaliceItem createFilled(){
        return new ChaliceItem(Prop.Items.ONE.get().group(null));
    }
}
