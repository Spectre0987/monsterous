package net.spectre.vamp.items;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.*;
import net.minecraft.util.ActionResult;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.monsters.MonsterRegistry;

public class AthameItem extends SwordItem{

	public AthameItem() {
		super(ItemTier.STONE, 3, -2.4F, (new Item.Properties()).maxStackSize(1).group(ItemGroup.COMBAT));
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		playerIn.setActiveHand(handIn);
		return ActionResult.resultSuccess(playerIn.getHeldItem(handIn));
	}

	@Override
	public UseAction getUseAction(ItemStack stack) {
		return UseAction.BLOCK;
	}

	@Override
	public int getUseDuration(ItemStack stack) {
		return 60;
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World worldIn, LivingEntity living, int timeLeft) {
		super.onPlayerStoppedUsing(stack, worldIn, living, timeLeft);
		
		//If complete
		if(timeLeft <= 0 && !worldIn.isRemote) {
			living.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
				if(cap.getMonsterType() == MonsterRegistry.DEMON.get()) {
					InventoryHelper.spawnItemStack(worldIn, living.getPosX(), living.getPosY(), living.getPosZ(), new ItemStack(VItems.DEMON_BLOOD.get()));
					living.attackEntityFrom(DamageSource.GENERIC, 21);
				}
			});
		}
	}

}
