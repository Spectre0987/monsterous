package net.spectre.vamp.items;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.spectre.vamp.itemgroups.VItemGroup;
import net.spectre.vamp.items.clothes.ClothesData;
import net.spectre.vamp.items.clothes.ClothesType;
import net.spectre.vamp.items.data.ClothesManager;
import net.spectre.vamp.properties.Prop;

import javax.annotation.Nullable;

public class ClothesItem extends ArmorItem {

    private ClothesType clothes;

    public ClothesItem(IArmorMaterial materialIn, EquipmentSlotType slot, ClothesType clothes) {
        super(materialIn, slot, Prop.Items.ONE.get().group(VItemGroup.MAIN));
        this.clothes = clothes;
    }

    public ClothesType getClothsType(){
        return this.clothes;
    }

    @Nullable
    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type) {
        ClothesData data = ClothesManager.getData(this.clothes);
        if(data != null)
            return data.getTexture();
        return null;
    }

    @Nullable
    @Override
    public <A extends BipedModel<?>> A getArmorModel(LivingEntity entityLiving, ItemStack itemStack, EquipmentSlotType armorSlot, A _default) {
        ClothesData data = ClothesManager.getData(this.clothes);
        if(data != null)
            return (A)data.getModel();
        return super.getArmorModel(entityLiving, itemStack, armorSlot, _default);
    }
}
