package net.spectre.vamp.items;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.itemgroups.VItemGroup;
import net.spectre.vamp.properties.Prop;

import java.util.function.Supplier;

public class ChalkItem extends Item {

    public ChalkItem() {
        super(Prop.Items.ONE.get().group(VItemGroup.MAIN).defaultMaxDamage(13));
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        if(context.getFace() == Direction.UP){
            context.getWorld().setBlockState(context.getPos().up(), this.getCircle(context.getWorld(), context.getPos()));
            context.getItem().damageItem(1, context.getPlayer(), player -> {});
        }
        return ActionResultType.SUCCESS;
    }

    public BlockState getCircle(World world, BlockPos pos){
        TileEntity te = world.getTileEntity(pos.down());

        if(te instanceof IInventory)
            return VBlocks.SUMMON_CIRCLE.get().getDefaultState();

        return VBlocks.DEMON_TRAP.get().getDefaultState();

    }

    public ItemStack getContractOffering(IInventory inv){
        for(int i = 0; i < inv.getSizeInventory(); ++i){

            ItemStack stack = inv.getStackInSlot(i);

            if(stack.getItem() == VItems.CONTRACT.get()){
                if(DemonicContract.getContract(stack) != null){
                    return stack;
                }
            }
        }
        return ItemStack.EMPTY;
    }

}
