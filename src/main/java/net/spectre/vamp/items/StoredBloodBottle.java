package net.spectre.vamp.items;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.UseAction;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.misc.ObjectWrapper;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.monsters.MonsterType;
import net.spectre.vamp.monsters.VampireMonster;
import net.spectre.vamp.properties.Prop;

public class StoredBloodBottle extends Item {

    public StoredBloodBottle() {
        super(Prop.Items.ONE.get().containerItem(Items.GLASS_BOTTLE));
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        playerIn.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
            if(cap.getMonsterType() == MonsterRegistry.VAMPIRE.get()){
                playerIn.setActiveHand(handIn);
            }
        });
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity entityLiving) {
        ObjectWrapper<Boolean> used = new ObjectWrapper<>(false);
        entityLiving.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
            if(cap.getMonsterType() == MonsterRegistry.VAMPIRE.get()){
                VampireMonster monster = cap.getMonsterObject(VampireMonster.class);
                monster.addBlood(5);
                used.set(true);
            }
        });

        return used.get() ? new ItemStack(this.getContainerItem()) : super.onItemUseFinish(stack, worldIn, entityLiving);
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.DRINK;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return 60;
    }
}
