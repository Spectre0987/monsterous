package net.spectre.vamp.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.monsters.DemonMonster;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.monsters.MonsterType;
import net.spectre.vamp.properties.Prop;

import javax.annotation.Nullable;
import java.util.UUID;

public class SignedDemonicContractItem extends Item {

    public SignedDemonicContractItem(){
        super(Prop.Items.ONE.get().maxDamage(13).group(null));
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {

        ItemStack stack = playerIn.getHeldItem(handIn);
        UUID signer = getSigner(stack);

        if(signer != null){
            playerIn.getCapability(Capabilities.MONSTER).ifPresent(cap -> {

                //If a demon is holding this contract
                if(cap.getMonsterType() == MonsterRegistry.DEMON.get()){

                    //If this demon has signed this contract
                    DemonMonster demon = (DemonMonster) cap.getTypeObject();
                    if(demon.getContractPlayers().contains(signer)){
                        if(!worldIn.isRemote && summonToPlayer(worldIn.getServer(), (ServerPlayerEntity) playerIn, signer))
                            stack.attemptDamageItem(1, playerIn.getRNG(), (ServerPlayerEntity) playerIn);
                    }

                }

            });
        }

        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    private boolean summonToPlayer(MinecraftServer server, ServerPlayerEntity demon, UUID id){
        PlayerEntity target = server.getPlayerList().getPlayerByUUID(id);
        //If Target is alive and on server
        if(target != null){
            //If in same dimension

            BlockPos targetPos = target.getPosition().offset(target.getHorizontalFacing().getOpposite(), 2);
            double x = targetPos.getX() + 0.5, y = targetPos.getY() + 1, z = targetPos.getZ() + 0.5;

            if(Helper.areInSameDimensions(demon, target)){
                demon.attemptTeleport(x, y, z, true);
                return true;
            }
            else{
                ServerWorld world = server.getWorld(target.world.getDimensionKey());

                demon.teleport(world, x, y, z, target.rotationYawHead, 0);

                return true;
            }
        }
        return false;
    }

    public static void setSigner(ItemStack stack, UUID id){
        stack.getOrCreateTag().putString("signer", id.toString());
    }

    @Nullable
    public static UUID getSigner(ItemStack stack){
        if(stack.hasTag() && stack.getTag().contains("signer"))
            return UUID.fromString(stack.getTag().getString("signer"));
        return null;
    }
}
