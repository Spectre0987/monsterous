package net.spectre.vamp.items;

import net.minecraft.item.IItemTier;
import net.minecraft.item.SwordItem;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.misc.IHurtMonsters;
import net.spectre.vamp.monsters.MonsterType;

import java.util.function.Supplier;

public class MonsterBlade extends SwordItem implements IHurtMonsters {

    private Supplier<MonsterType[]> types;

    public MonsterBlade(IItemTier tier, int attackDamageIn, float attackSpeedIn, Properties builderIn, Supplier<MonsterType[]> types) {
        super(tier, attackDamageIn, attackSpeedIn, builderIn);
        this.types = types;
    }

    @Override
    public boolean canHurt(MonsterType type) {
        for(MonsterType t : this.types.get()){
            if(type == t)
                return true;
        }
        return false;
    }
}
