package net.spectre.vamp.items;

import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.itemgroups.VItemGroup;
import net.spectre.vamp.items.clothes.ClothesType;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.monsters.MonsterType;
import net.spectre.vamp.properties.Prop;

public class VItems {

	
	public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Monsterous.MODID);
	
	
	public static final RegistryObject<MonsterPotion> BLOOD_BOTTLE = ITEMS.register("blood_bottle", () -> new MonsterPotion(MonsterRegistry.VAMPIRE::get));
	public static final RegistryObject<MonsterPotion> DEMON_BLOOD = ITEMS.register("demon_blood", () -> new MonsterPotion(MonsterRegistry.DEMON::get));
	public static final RegistryObject<MonsterPotion> ANGEL_POTION = ITEMS.register("angel_potion", () -> new MonsterPotion(MonsterRegistry.ANGEL::get));
	public static final RegistryObject<MonsterPotion> MERMAID_POTION = ITEMS.register("mermaid_potion", () -> new MonsterPotion(MonsterRegistry.MERMAID::get));
	public static final RegistryObject<MonsterPotion> NAGA_POTION = ITEMS.register("naga_potion", () -> new MonsterPotion(MonsterRegistry.NAGA::get));

	public static final RegistryObject<MonsterBlade> DEMON_KNIFE = ITEMS.register("demon_knife", () -> new MonsterBlade(ItemTier.IRON, 3, -2.8F, Prop.Items.ONE.get().group(VItemGroup.MAIN), () -> new MonsterType[]{MonsterRegistry.DEMON.get()}));
	public static final RegistryObject<BaseItem> CROSS = ITEMS.register("cross", BaseItem::create64);
	public static final RegistryObject<DemonicContract> CONTRACT = ITEMS.register("demonic_contract", DemonicContract::new);
	public static final RegistryObject<BaseItem> CONTRACT_FILLED = ITEMS.register("demonic_contract_filled", () -> new BaseItem(new Item.Properties().group(null).maxStackSize(1)));
	public static final RegistryObject<ChalkItem> CHALK = ITEMS.register("chalk", ChalkItem::new);
	public static final RegistryObject<StoredBloodBottle> STORED_BLOOD = ITEMS.register("stored_blood", StoredBloodBottle::new);
	public static final RegistryObject<ColtItem> COLT = ITEMS.register("colt", ColtItem::new);
	public static final RegistryObject<BaseItem> COLT_BULLET = ITEMS.register("colt_bullet", BaseItem::create16);
	public static final RegistryObject<ChaliceItem> CHALICE_OF_PURITY = ITEMS.register("chalice_of_purity", ChaliceItem::create);
	public static final RegistryObject<ChaliceItem> CHALICE_OF_PURITY_FILLED = ITEMS.register("chalice_of_purity_filled", ChaliceItem::createFilled);
	
	//Monster Arrows
	public static final RegistryObject<MonsterArrowItem> VAMPIRE_ARROW = ITEMS.register("arrows/vampire", () -> new MonsterArrowItem(() -> Lists.newArrayList(MonsterRegistry.VAMPIRE.get())));
	
	//Item Blocks
	public static final RegistryObject<BlockItem> DEMON_ROSE = ITEMS.register("demon_rose", () -> new BlockItem(VBlocks.DEMON_ROSE.get(), Prop.Items.BASE.get().group(ItemGroup.DECORATIONS)));
	public static final RegistryObject<CoffinItem> COFFIN = ITEMS.register("coffin", CoffinItem::new);

	//Armor items
	public static final RegistryObject<ArmorItem> CAPE = ITEMS.register("cape", () -> new ClothesItem(ArmorMaterial.CHAIN, EquipmentSlotType.CHEST, ClothesType.CAPE));
	public static final RegistryObject<ArmorItem> DRESS = ITEMS.register("vampire_dress", () -> new ClothesItem(ArmorMaterial.CHAIN, EquipmentSlotType.CHEST, ClothesType.DRESS));

	public static <T extends Block> RegistryObject<BlockItem> createBlockItem(RegistryObject<T> block){
		return ITEMS.register(block.getId().getPath(), () -> new BlockItem(block.get(), Prop.Items.BASE.get()));
	}

}
