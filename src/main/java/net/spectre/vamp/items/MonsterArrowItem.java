package net.spectre.vamp.items;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.spectre.vamp.entities.MonsterArrow;
import net.spectre.vamp.itemgroups.VItemGroup;
import net.spectre.vamp.monsters.MonsterType;

import java.util.List;
import java.util.function.Supplier;

public class MonsterArrowItem extends ArrowItem{

	private Supplier<List<MonsterType>> types;
	
	public MonsterArrowItem(Supplier<List<MonsterType>> types) {
		super(new Properties().group(VItemGroup.MAIN).maxStackSize(64));
		this.types = types;
	}

	@Override
	public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
		return new MonsterArrow(worldIn, shooter, types.get());
	}
	

}
