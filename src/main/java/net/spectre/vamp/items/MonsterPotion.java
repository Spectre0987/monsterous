package net.spectre.vamp.items;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.PacketDistributor;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.itemgroups.VItemGroup;
import net.spectre.vamp.monsters.MonsterType;
import net.spectre.vamp.network.Network;
import net.spectre.vamp.network.packets.CapUpdateMessage;
import net.spectre.vamp.network.packets.RequestMonsterUpdateMessage;

import java.util.function.Supplier;

public class MonsterPotion extends Item {

	Supplier<MonsterType> type;
	
	public MonsterPotion(Supplier<MonsterType> type) {
		super(new Properties().maxStackSize(1).group(VItemGroup.MAIN));
		this.type = type;
	}

	@Override
	public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity living) {
		stack.shrink(1);
		if(living instanceof PlayerEntity)
			((PlayerEntity)living).getCapability(Capabilities.MONSTER).ifPresent(cap -> {
				cap.setMonsterType(type.get());
				if(!worldIn.isRemote)
					Network.INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.noArg(), new CapUpdateMessage(living.getUniqueID(), cap.serializeNBT()));
			});
		return stack;
	}

	@Override
	public UseAction getUseAction(ItemStack stack) {
		return UseAction.DRINK;
	}

	@Override
	public int getUseDuration(ItemStack stack) {
		return 60;
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		playerIn.setActiveHand(handIn);
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

}
