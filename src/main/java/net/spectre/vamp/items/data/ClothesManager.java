package net.spectre.vamp.items.data;

import com.google.common.collect.Maps;
import net.spectre.vamp.items.clothes.ClothesData;
import net.spectre.vamp.items.clothes.ClothesType;

import java.util.Map;

public class ClothesManager {

    private static final Map<ClothesType, ClothesData> DATAS = Maps.newEnumMap(ClothesType.class);

    public static void addClothesData(ClothesType type, ClothesData data){
        DATAS.put(type, data);
    }

    public static ClothesData getData(ClothesType type){
        return DATAS.get(type);
    }


}
