package net.spectre.vamp.items.data;

import com.google.common.collect.Maps;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.text.TranslationTextComponent;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.constants.AttributeConstants;
import net.spectre.vamp.misc.IPlayerAction;

import java.util.HashMap;
import java.util.UUID;

public class ContractData {

    private static final HashMap<String, IPlayerAction> EFFECTS = Maps.newHashMap();

    String name;
    TranslationTextComponent trans;
    UUID owner;

    public ContractData(String name){
        this.name = name;
        this.trans = new TranslationTextComponent("contracts." + Monsterous.MODID + "." + name);
    }

    public String getId(){
        return name;
    }

    public TranslationTextComponent getTrans(){
        return this.trans;
    }

    public void setOwner(UUID id){
        this.owner = id;
    }

    public UUID getOwner(){
        return this.owner;
    }

    public CompoundNBT serialize(){
        CompoundNBT tag = new CompoundNBT();
        tag.putString("name", this.name);

        if(this.owner != null)
            tag.putString("owner", this.owner.toString());

        return tag;
    }

    public static ContractData read(CompoundNBT tag){
        ContractData data = new ContractData(tag.getString("name"));

        if(tag.contains("owner"))
            data.setOwner(UUID.fromString(tag.getString("owner")));

        return data;
    }

    public static void registerEffects(){
        EFFECTS.put("might", player -> {
            applyPotionEffect(player, Effects.STRENGTH);
        });

        EFFECTS.put("haste", player ->{
            applyPotionEffect(player, Effects.HASTE);
        });

        EFFECTS.put("speed", player ->{
            applyPotionEffect(player, Effects.SPEED);
        });
    }

    public void applyContract(PlayerEntity player){
        IPlayerAction action = EFFECTS.getOrDefault(this.name, null);
        if(action != null)
            action.act(player);
    }

    public static void applyPotionEffect(PlayerEntity player, Effect effect){
        EffectInstance effectInstance = new EffectInstance(effect, Integer.MAX_VALUE - 1, 0, true, false, false);
        player.addPotionEffect(effectInstance);
    }
}
