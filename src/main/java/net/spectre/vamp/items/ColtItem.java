package net.spectre.vamp.items;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.misc.TextHelper;
import net.spectre.vamp.properties.Prop;
import net.spectre.vamp.tags.VItemTags;

import javax.annotation.Nullable;
import java.util.List;

public class ColtItem extends Item {

    public ColtItem() {
        super(Prop.Items.ONE.get());
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        playerIn.setActiveHand(handIn);
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    @Override
    public void onPlayerStoppedUsing(ItemStack stack, World worldIn, LivingEntity entityLiving, int timeLeft) {
        if(entityLiving instanceof PlayerEntity){

            PlayerEntity player = (PlayerEntity)entityLiving;

            stack.getCapability(Capabilities.COLT).ifPresent(cap -> {

                //shoot gun
                if(!player.isSneaking()){
                    cap.fire(player);
                }
            });
        }

        super.onPlayerStoppedUsing(stack, worldIn, entityLiving, timeLeft);
    }

    @Override
    public void onUse(World worldIn, LivingEntity livingEntityIn, ItemStack stack, int count) {
        super.onUse(worldIn, livingEntityIn, stack, count);

        int timeUsed = Helper.getTimeUsing(stack, count);
        if(livingEntityIn instanceof PlayerEntity && livingEntityIn.isSneaking() && timeUsed > 1 && timeUsed % 30 == 0){
            stack.getCapability(Capabilities.COLT).ifPresent(colt -> {
                colt.reload((PlayerEntity) livingEntityIn);
            });
        }

    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        stack.getCapability(Capabilities.COLT).ifPresent(colt -> {
            tooltip.add(TextHelper.createToolTipTranslation(this, "", colt.getBulletCount()));
        });
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.BOW;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return 72000;
    }

    public static ItemStack findAmmo(PlayerEntity entity, boolean remove){
        for(ItemStack stack : entity.inventory.mainInventory){
            if(VItemTags.COLT_AMMO.contains(stack.getItem())) {
                ItemStack ammo = stack.copy();
                stack.shrink(1);
                return ammo;
            }
        }
        return ItemStack.EMPTY;
    }

}
