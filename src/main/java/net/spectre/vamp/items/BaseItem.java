package net.spectre.vamp.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.spectre.vamp.itemgroups.VItemGroup;

public class BaseItem extends Item{

	public BaseItem(Properties properties) {
		super(properties);
	}
	
	public static BaseItem create64() {
		return new BaseItem(new Item.Properties()
				.maxStackSize(64)
				.group(VItemGroup.MAIN));
	}

	public static BaseItem create16() {
		return new BaseItem(new Item.Properties()
				.maxStackSize(16)
				.group(VItemGroup.MAIN));
	}

}
