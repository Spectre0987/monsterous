package net.spectre.vamp.items.clothes;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class ClothesData {

    private String texture;
    private Object model;

    public ClothesData setTexture(ResourceLocation location){
        this.texture = location.getNamespace() + ":textures/entity/armor/" + location.getPath() + ".png";
        return this;
    }

    @OnlyIn(Dist.CLIENT)
    public ClothesData setModel(BipedModel<?> model){
        this.model = model;
        return this;
    }

    @OnlyIn(Dist.CLIENT)
    public BipedModel<? extends LivingEntity> getModel(){
        return (BipedModel<LivingEntity>) this.model;
    }

    public String getTexture() {
        return this.texture;
    }
}
