package net.spectre.vamp.items.clothes;

public enum ClothesType {
    CAPE,
    DRESS
}
