package net.spectre.vamp.items;

import net.minecraft.block.BedBlock;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.properties.BedPart;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.itemgroups.VItemGroup;
import net.spectre.vamp.properties.Prop;

public class CoffinItem extends BlockItem{

	public CoffinItem() {
		super(VBlocks.COFFIN.get(), Prop.Items.BASE.get().group(VItemGroup.MAIN));

	}

	@Override
	protected boolean onBlockPlaced(BlockPos pos, World worldIn, PlayerEntity player, ItemStack stack, BlockState state) {
		
		Direction face = state.get(BlockStateProperties.HORIZONTAL_FACING);
		
		worldIn.setBlockState(pos.offset(face), VBlocks.COFFIN.get()
				.getDefaultState().with(BedBlock.PART, BedPart.HEAD)
					.with(BlockStateProperties.HORIZONTAL_FACING, face));
		
		return super.onBlockPlaced(pos, worldIn, player, stack, state);
		
	}

	@Override
	protected boolean canPlace(BlockItemUseContext context, BlockState p_195944_2_) {
		if(!super.canPlace(context, p_195944_2_))
			return false;
		
		if(context.getWorld().getBlockState(context.getPos().offset(context.getNearestLookingDirection())).isReplaceable(context))
			return true;
		return false;
		
	}

}
