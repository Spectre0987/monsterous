package net.spectre.vamp.damagesources;

import net.minecraft.util.DamageSource;
import net.spectre.vamp.items.MonsterPotion;
import net.spectre.vamp.monsters.MonsterType;

import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

public class MonsterDamageSource extends DamageSource {

    private Supplier<Collection<MonsterType>> monsters;

    public MonsterDamageSource(String damageTypeIn, Supplier<Collection<MonsterType>> monsters) {
        super(damageTypeIn);
        this.monsters = monsters;
    }

    public boolean canHurt(MonsterType type){
        for(MonsterType t : this.monsters.get()){
            if(t == type)
                return true;
        }
        return false;
    }
}
