package net.spectre.vamp.damagesources;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.DamageSource;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.misc.IHurtMonsters;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.monsters.MonsterType;

import javax.annotation.Nullable;

public class DamageSources {

	public static final DamageSource SUN = new DamageSource(Monsterous.MODID + ".sun")
			.setDamageBypassesArmor()
			.setDamageIsAbsolute()
			.setFireDamage()
			.setDamageAllowedInCreativeMode();
	
	public static final DamageSource VAMPIRE = new DamageSource(Monsterous.MODID + ".bite")
			.setDamageBypassesArmor()
			.setDamageIsAbsolute();

	public static final DamageSource OUT_OF_HELL = new DamageSource(Monsterous.MODID + ".out_of_hell")
			.setDamageBypassesArmor()
			.setDamageIsAbsolute()
			.setDamageAllowedInCreativeMode();

	public static final DamageSource MONSTER_ARROW = new MonsterDamageSource(Monsterous.MODID + ".monster_arrow", () -> MonsterRegistry.REGISTRY.get().getValues());
	
	public static final DamageSource OUT_OF_CLOUDS = new DamageSource(Monsterous.MODID + ".out_of_clouds")
			.setDamageAllowedInCreativeMode()
			.setDamageBypassesArmor()
			.setDamageIsAbsolute()
			.setDamageAllowedInCreativeMode();

    public static final DamageSource DEMONIC_RAM = new DamageSource(Monsterous.MODID + ".demon_ram");

	public static final DamageSource MERMAID_CONSUME = new DamageSource(Monsterous.MODID + ".mermaid_eat");
	public static final MonsterDamageSource KILL_ALL_COLT = new MonsterDamageSource(Monsterous.MODID + ".colt", MonsterRegistry.REGISTRY.get()::getValues);
    public static final DamageSource SMITE = new MonsterDamageSource(Monsterous.MODID + ".smite", MonsterRegistry.REGISTRY.get()::getValues);


    public static boolean canDamage(MonsterType type, DamageSource source, @Nullable Entity attacker) {
		
		//If a a monster damage source
		if (source instanceof MonsterDamageSource) {
			if(((MonsterDamageSource)source).canHurt(type))
				return true;
		}

		if(attacker instanceof LivingEntity){
			LivingEntity livingAttacker = (LivingEntity)attacker;
			if(livingAttacker.getHeldItemMainhand().getItem() instanceof IHurtMonsters){
				if(((IHurtMonsters)livingAttacker.getHeldItemMainhand().getItem()).canHurt(type))
					return true;
			}
		}
		
		
		if(type == MonsterRegistry.VAMPIRE.get()) {
			return isNormalDamageExceptions(source) || source.isFireDamage() || source == DamageSource.STARVE;
		}
		
		if(type == MonsterRegistry.DEMON.get())
			return isNormalDamageExceptions(source) || source == DamageSource.DROWN;
		
		if(type == MonsterRegistry.ANGEL.get())
			return isNormalDamageExceptions(source) || source == OUT_OF_CLOUDS;
		
		return true;
	}
	
	public static boolean isNormalDamageExceptions(DamageSource source) {
		return source.canHarmInCreative() || source.isMagicDamage();
	}
	
}
