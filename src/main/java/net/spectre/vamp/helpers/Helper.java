package net.spectre.vamp.helpers;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceContext.BlockMode;
import net.minecraft.util.math.RayTraceContext.FluidMode;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.world.World;
import net.spectre.vamp.Monsterous;

import java.util.Optional;
import java.util.function.Predicate;

public class Helper {


	public static boolean isDay(long time) {
		return time > 23031 || time < 12541;
	}

	public static boolean isDay(World world) {
		return isDay(world.getDayTime());
	}

	public static boolean inEitherHand(Predicate<ItemStack> stack, LivingEntity player) {
		return stack.test(player.getHeldItemMainhand()) || stack.test(player.getHeldItemOffhand());
	}

	public static ResourceLocation createRL(String name) {
		return new ResourceLocation(Monsterous.MODID, name);
	}

	public static int getRotationFromDir(Direction direction) {
		switch(direction) {
			case EAST: return 90;
			case SOUTH: return 180;
			case WEST: return 270;
			default: return 0;
		}
	}

	public static RayTraceContext createRayContext(PlayerEntity player, double dist) {
		Vector3d start = player.getPositionVec().add(0, player.getEyeHeight(), 0);
		return new RayTraceContext(start, start.add(player.getLookVec().scale(dist)), BlockMode.COLLIDER, FluidMode.NONE, player);
	}

	public static Entity rayTraceEntity(Entity tracer, double dist) {
		
		Vector3d start = tracer.getPositionVec().add(0, tracer.getEyeHeight(), 0);
		
		double foundDist = dist;
		Entity target = null;
		
		for(Entity e : tracer.world.getEntitiesInAABBexcluding(tracer, tracer.getBoundingBox().grow(dist + 1), e -> e instanceof LivingEntity)) {
			
			Optional<Vector3d> result = e.getBoundingBox().rayTrace(start, start.add(tracer.getLookVec().scale(dist)));
			Vector3d pos = result.orElse(null);
			if(pos != null) {
				//If first in ray, then closest
				if(target == null) {
					target = e;
					foundDist = start.distanceTo(pos);
				}
				else {
					//If in ray and closer
					double currentDist = start.distanceTo(pos);
					if(currentDist < foundDist) {
						target = e;
						foundDist = currentDist;  
					}
				}
			}
		}
		
		return target;
	}

    public static boolean areInSameDimensions(Entity entity1, Entity entity2) {
		return entity1.world.getDimensionKey().getLocation().equals(entity2.world.getDimensionKey().getLocation());
    }

	public static void forceEntity(Entity entity, Vector3d point, float force, boolean push) {

		if(entity == null)
			return;

		Vector3d motion = null;
		if(push)
			motion = entity.getPositionVec().subtract(point).normalize().scale(force);
		else motion = point.subtract(entity.getPositionVec()).normalize().scale(force);

		entity.setMotion(entity.getMotion().add(motion));

	}

    public static boolean isShielding(LivingEntity e) {
		return e.getActiveItemStack().isShield(e);
    }

    public static int getTimeUsing(ItemStack stack, int timeLeft) {
		return stack.getUseDuration() - timeLeft;
    }

	public static void playSoundAtPlayer(PlayerEntity player, SoundEvent event, SoundCategory category, float volume) {
		player.world.playSound(null, player.getPosition(), event, category, volume, 1.0F);
	}

    public static Vector3d toVec3d(Vector3i pos, boolean center) {
		return new Vector3d(pos.getX() + (center ? 0.5 : 0), pos.getY() + (center ? 0.5 : 0), pos.getZ() + (center ? 0.5 : 0));
    }

    public static Vector3d getDirectionVector(Vector3d start, Vector3d end){
		return start.subtract(end).normalize();
	}

    public static String formatPosition(BlockPos pos) {
		return pos.getX() + ", " + pos.getY() + ", " + pos.getZ();
    }
}
