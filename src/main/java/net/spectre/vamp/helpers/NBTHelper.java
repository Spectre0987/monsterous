package net.spectre.vamp.helpers;

import net.minecraft.nbt.CompoundNBT;

import javax.annotation.Nullable;
import java.util.UUID;

public class NBTHelper {


    @Nullable
    public static UUID readUUID(String key, CompoundNBT nbt){
        if(nbt.contains(key))
            return UUID.fromString(nbt.getString(key));
        return null;
    }

    public static boolean writeUUID(String key, @Nullable UUID id, CompoundNBT nbt){
        if(id != null){
            nbt.putString(key, id.toString());
            return true;
        }
        return false;
    }


}
