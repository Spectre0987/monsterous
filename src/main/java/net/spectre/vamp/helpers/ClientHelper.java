package net.spectre.vamp.helpers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.network.play.NetworkPlayerInfo;
import net.minecraft.util.MovementInput;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.UUID;

@OnlyIn(Dist.CLIENT)
public class ClientHelper {

	public static void stopAllMovement(MovementInput move) {
		move.moveForward = move.moveStrafe = 0.0F;
		move.jump = false;
	}

    public static String getUsernameOrUUID(UUID owner) {
		NetworkPlayerInfo info = Minecraft.getInstance().getConnection().getPlayerInfo(owner);
		if(info != null)
			return info.getDisplayName() != null ? info.getDisplayName().getString() : owner.toString();
		return owner.toString();
    }
}
