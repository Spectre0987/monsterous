package net.spectre.vamp;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig.Type;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.cap.*;
import net.spectre.vamp.cap.items.ColtCap;
import net.spectre.vamp.cap.items.IColt;
import net.spectre.vamp.config.Config;
import net.spectre.vamp.entities.VEntities;
import net.spectre.vamp.items.VItems;
import net.spectre.vamp.items.data.ContractData;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.network.Network;
import net.spectre.vamp.powers.Powers;
import net.spectre.vamp.proxy.EmptyProxy;
import net.spectre.vamp.proxy.IProxy;
import net.spectre.vamp.recipes.Recipes;
import net.spectre.vamp.sounds.Sounds;
import net.spectre.vamp.tiles.VTiles;

@Mod(Monsterous.MODID)
public class Monsterous {

	public static final String MODID = "monsterous";

	public static IProxy PROXY = new EmptyProxy();
	
	public Monsterous() {
		
		IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
		
		bus.addListener(Monsterous::common);
		
		VBlocks.BLOCKS.register(bus);
		VItems.ITEMS.register(bus);
		VTiles.TILES.register(bus);
		Recipes.RECIPES.register(bus);
		VEntities.ENTITIES.register(bus);
		Powers.POWERS.register(bus);
		MonsterRegistry.MONSTER_TYPES.register(bus);
		Sounds.SOUNDS.register(bus);
	}
	
	public static void common(FMLCommonSetupEvent event) {
		
		Network.registerPackets();
		CapabilityManager.INSTANCE.register(IMonster.class, new IMonster.Storage(), () -> new MonsterCapability(null));
		CapabilityManager.INSTANCE.register(ISmiteable.class, new ISmiteable.Storage(), () -> new SmiteCap(null, 0));
		CapabilityManager.INSTANCE.register(IMinion.class, new BasicStorage<IMinion, CompoundNBT>(), () -> new MinionCapability(null));
		CapabilityManager.INSTANCE.register(IColt.class, new IColt.Storage(), () -> new ColtCap(ItemStack.EMPTY));
		ModLoadingContext.get().registerConfig(Type.COMMON, Config.SERVER);
		ContractData.registerEffects();

	}
}
