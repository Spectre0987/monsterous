package net.spectre.vamp.client.renderers.tiles;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.spectre.vamp.client.models.tiles.CoffinModel;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.tiles.CoffinTile;

public class CoffinRenderer extends TileEntityRenderer<CoffinTile> {

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/coffin.png");
	public static final CoffinModel MODEL = new CoffinModel();

	public CoffinRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(CoffinTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {

		if(tile.isHead()) {
			matrixStackIn.push();
			matrixStackIn.translate(0.5, 1.5, 0.5);
			matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
			matrixStackIn.rotate(Vector3f.YP.rotationDegrees(Helper.getRotationFromDir(tile.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING))));
			MODEL.renderCoffin(matrixStackIn, bufferIn.getBuffer(RenderType.getEntityCutoutNoCull(TEXTURE)), tile, combinedLightIn);
			matrixStackIn.pop();
		}

	}
}
