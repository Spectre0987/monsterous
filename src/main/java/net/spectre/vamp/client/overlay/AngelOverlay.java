package net.spectre.vamp.client.overlay;

import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Items;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.monsters.AngelMonster;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.MonsterRegistry;

public class AngelOverlay extends SmoothBarOverlay {

    public AngelOverlay() {
        super(MonsterRegistry.ANGEL.get(), RenderGameOverlayEvent.ElementType.FOOD, false);
        this.setSize(60, 7);
        this.setUV(0, 16);
        this.setLocation(60, 70);

    }

    @Override
    public void render(IMonsterType monster) {

        if(monster instanceof AngelMonster)
            this.setPercent(((AngelMonster)monster).getPurityPercent());

        this.setLocation(60, 50);

        super.render(monster);
    }
}
