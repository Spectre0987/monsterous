package net.spectre.vamp.client.overlay;

import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.MonsterType;
import org.lwjgl.opengl.GL11;

public class SmoothBarOverlay extends BaseOverlay{

    int x, y;
    int u, v, width, height;
    double percent = 1.0F;

    public SmoothBarOverlay(MonsterType type, RenderGameOverlayEvent.ElementType element, boolean cancel) {
        super(type, element, cancel);
    }

    public void setPercent(float percent){
        this.percent = MathHelper.clamp(percent, 0.0F, 1.0F);
    }

    public void setUV(int u, int v){
        this.u = u;
        this.v = v;
    }

    public void setSize(int width, int height){
        this.width = width;
        this.height = height;
    }

    public void setLocation(int x, int y){
        this.x = x;
        this.y = y;
    }

    @Override
    public void render(IMonsterType monster) {

        Minecraft.getInstance().getTextureManager().bindTexture(VampOverlay.TEXTURE);

        BufferBuilder bb = Tessellator.getInstance().getBuffer();

        bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);

        MainWindow window = Minecraft.getInstance().getMainWindow();
        int x = (window.getScaledWidth() + this.x) / 2, y = window.getScaledHeight() - this.y;

        float minU = u / 256.0F, maxU = (u + width) / 256.0F;
        float minV = v / 256.0F, maxV = (v + height) / 256.0F;

        //Base
        bb.pos(x, y, 0).tex(minU, minV).endVertex();
        bb.pos(x, y + height, 0).tex(minU, maxV).endVertex();
        bb.pos(x + width, y + height, 0).tex(maxU, maxV).endVertex();
        bb.pos(x + width, y, 0).tex(maxU, minV).endVertex();

        minV = (v + height) / 256.0F;
        maxV = (v + (height * 2)) / 256.0F;

        maxU = (u + (width * (float)percent)) / 256.0F;

        //fill
        bb.pos(x, y, 0).tex(minU, minV).endVertex();
        bb.pos(x, y + height, 0).tex(minU, maxV).endVertex();
        bb.pos(x + (width * percent), y + height, 0).tex(maxU, maxV).endVertex();
        bb.pos(x + (width * percent), y, 0).tex(maxU, minV).endVertex();

        Tessellator.getInstance().draw();

    }
}
