package net.spectre.vamp.client.overlay;

import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.MonsterType;

public abstract class BaseOverlay {

	private MonsterType type;
	private ElementType element;
	private boolean cancel;
	
	public BaseOverlay(MonsterType type, ElementType element, boolean cancel) {
		this.type = type;
		this.element = element;
		this.cancel = cancel;
	}
	
	public boolean shouldCancel() {
		return this.cancel;
	}
	
	public ElementType getElementType() {
		return this.element;
	}
	
	public MonsterType getMonster() {
		return this.type;
	}
	
	public abstract void render(IMonsterType monster);
	
}
