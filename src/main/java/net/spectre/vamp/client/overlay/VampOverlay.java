package net.spectre.vamp.client.overlay;

import net.spectre.vamp.monsters.MonsterRegistry;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.VampireMonster;

public class VampOverlay extends BaseOverlay{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Monsterous.MODID, "textures/overlay/monster.png");

	public VampOverlay() {
		super(MonsterRegistry.VAMPIRE.get(), ElementType.FOOD, true);
	}
	
	public void render(IMonsterType monster) {
		
		if(!(monster instanceof VampireMonster))
			return;
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		
		MainWindow win = Minecraft.getInstance().getMainWindow();
		int width = win.getScaledWidth();
		int height = win.getScaledHeight();
		
		int sizeX = 60, sizeY = 7;
		
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		
		int x = width / 2 + 30, y = height - 40;
		
		float minU = 0, minV = 0, maxU = sizeX / 256.0F, maxV = sizeY / 256.0F;
		float offsetY = 8 / 256.0F;
		
		//Base
		bb.pos(x, y, 0).tex(minU, minV).endVertex();
		bb.pos(x, y + sizeY, 0).tex(minU, maxV).endVertex();
		bb.pos(x + sizeX, y + sizeY, 0).tex(maxU, maxV).endVertex();
		bb.pos(x + sizeX, y, 0).tex(maxU, minV).endVertex();
		
		//Filled
		float scale = ((VampireMonster)monster).getBlood() / 20.0F;
		float maxScaled = minU + ((sizeX * scale) / 256.0F);
		
		bb.pos(x, y, 0).tex(minU, minV + offsetY).endVertex();
		bb.pos(x, y + sizeY, 0).tex(minU, maxV + offsetY).endVertex();
		bb.pos(x + (sizeX * scale), y + sizeY, 0).tex(maxScaled, maxV + offsetY).endVertex();
		bb.pos(x + (sizeX * scale), y, 0).tex(maxScaled, minV + offsetY).endVertex();
		
		Tessellator.getInstance().draw();
	}
}
