package net.spectre.vamp.client.overlay;

import net.spectre.vamp.monsters.MonsterRegistry;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.spectre.vamp.monsters.DemonMonster;
import net.spectre.vamp.monsters.IMonsterType;

public class DemonOverlay extends BaseOverlay{

	public DemonOverlay() {
		super(MonsterRegistry.DEMON.get(), ElementType.FOOD, true);
	}

	@Override
	public void render(IMonsterType monster) {
		
		if(!(monster instanceof DemonMonster))
			return;
		
		Minecraft.getInstance().getTextureManager().bindTexture(VampOverlay.TEXTURE);
		
		MainWindow window = Minecraft.getInstance().getMainWindow();
		
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		
		int x = (window.getScaledWidth() + 60) / 2, y = window.getScaledHeight() - 38,
			width = 60, height = 7;
		
		float minU = 61 / 256.0F, minV = 0, sizeU = width / 256.0F, sizeV = height / 256.0F;
		
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		bb.pos(x, y, 0).tex(minU, minV).endVertex();
		bb.pos(x, y + height, 0).tex(minU, minV + sizeV).endVertex();
		bb.pos(x + width, y + height, 0).tex(minU + sizeU, minV + sizeV).endVertex();
		bb.pos(x + width, y, 0).tex(minU + sizeU, minV).endVertex();
		
		float offsetY = (height / 256.0F);
		float scale = ((DemonMonster)monster).getCorruptionBar();
		
		bb.pos(x, y, 0).tex(minU, minV + offsetY).endVertex();
		bb.pos(x, y + height, 0).tex(minU, minV + sizeV + offsetY).endVertex();
		bb.pos(x + width * scale, y + height, 0).tex(minU + (sizeU * scale), minV + sizeV + offsetY).endVertex();
		bb.pos(x + width * scale, y, 0).tex(minU + (sizeU * scale), minV + offsetY).endVertex();
		
		Tessellator.getInstance().draw();
		
	}

}
