package net.spectre.vamp.client.overlay;

import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.Tags;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.MermaidMonster;
import net.spectre.vamp.monsters.MonsterRegistry;

public class MermaidOverlay extends SmoothBarOverlay{

    public MermaidOverlay() {
        super(MonsterRegistry.MERMAID.get(), RenderGameOverlayEvent.ElementType.FOOD, false);
        this.setUV(63, 16);
        this.setSize(59, 8);
    }

    @Override
    public void render(IMonsterType monster) {
        super.render(monster);

        if(monster instanceof MermaidMonster)
            this.setPercent(((MermaidMonster)monster).getDrynessPercent());

        this.setLocation(55,48);

    }
}
