package net.spectre.vamp.client.particles;

import net.minecraft.client.particle.*;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;
import net.minecraft.particles.ParticleTypes;

import javax.annotation.Nullable;

public class BubbleParticle extends SpriteTexturedParticle {

    private IAnimatedSprite animSprite;

    protected BubbleParticle(ClientWorld world, double x, double y, double z) {
        super(world, x, y, z);
        this.maxAge = 60;
        this.motionY = 0.1;
    }

    @Override
    public IParticleRenderType getRenderType() {
        return IParticleRenderType.PARTICLE_SHEET_OPAQUE;
    }

    public void setAnimSprite(IAnimatedSprite sprite){
        this.animSprite = sprite;
        this.setSprite(sprite.get(this.age, this.maxAge));
    }

    @Override
    public void tick() {
        super.tick();

        if(this.age < this.maxAge - 10)
            this.setSprite(animSprite.get(0, maxAge));
        else this.setSprite(animSprite.get(maxAge, maxAge));

    }

    public static class Factory implements IParticleFactory<BasicParticleType> {

        private final IAnimatedSprite sprite;

        public Factory(IAnimatedSprite sprite){
            this.sprite = sprite;
        }

        @Nullable
        @Override
        public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed) {
            BubbleParticle part = new BubbleParticle(worldIn, x, y, z);
            part.setAnimSprite(this.sprite);
            return part;
        }
    }
}
