package net.spectre.vamp.client.particles;

import net.minecraft.client.particle.*;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.particles.BasicParticleType;

import javax.annotation.Nullable;

public class CauldronSmokeParticle extends SpriteTexturedParticle {

    protected CauldronSmokeParticle(ClientWorld world, double x, double y, double z) {
        super(world, x, y, z);
        this.age = 0;
        this.maxAge = 60;
    }

    @Override
    public void tick(){
        super.tick();
        this.motionY -= 0.1;
    }

    @Override
    public IParticleRenderType getRenderType() {
        return IParticleRenderType.PARTICLE_SHEET_OPAQUE;
    }

    public static class Factory implements IParticleFactory<BasicParticleType> {

        private IAnimatedSprite sprite;

        public Factory(IAnimatedSprite sprite){
            this.sprite = sprite;
        }

        @Nullable
        @Override
        public Particle makeParticle(BasicParticleType typeIn, ClientWorld worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed) {
            CauldronSmokeParticle part = new CauldronSmokeParticle(worldIn, x, y, z);
            part.setSprite(this.sprite.get(part.age, part.maxAge));
            part.motionX = xSpeed;
            part.motionY = ySpeed;
            part.motionZ = zSpeed;
            return part;
        }
    }

}
