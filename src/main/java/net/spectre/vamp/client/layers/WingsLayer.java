package net.spectre.vamp.client.layers;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.client.models.body.WingsModel;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.monsters.MonsterType;

public class WingsLayer extends LayerRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> {

	public static final ResourceLocation DEMON_TEXTURE = Helper.createRL("textures/entity/wings/demon.png");
	public static final ResourceLocation ANGEL_TEXTURE = Helper.createRL("textures/entity/wings/angel.png");
	public static final WingsModel MODEL = new WingsModel();
	
	
	public WingsLayer(PlayerRenderer renderer) {
		super(renderer);
	}
	
	private ResourceLocation getTextureForMonster(MonsterType type) {
		if(type == MonsterRegistry.ANGEL.get())
			return ANGEL_TEXTURE;
		return DEMON_TEXTURE;
	}
	
	private boolean hasWings(MonsterType type) {

		if(type == MonsterRegistry.ANGEL.get() || type == MonsterRegistry.DEMON.get())
			return true;

		return false;
	}

	@Override
	public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, AbstractClientPlayerEntity player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) {

		player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {

			if(this.hasWings(cap.getMonsterType())) {
				this.getEntityModel().setModelAttributes(MODEL);
				Minecraft.getInstance().getTextureManager().bindTexture(this.getTextureForMonster(cap.getMonsterType()));
				MODEL.setLivingAnimations(player, limbSwing, limbSwingAmount, partialTicks);
				MODEL.render(matrixStackIn, bufferIn.getBuffer(RenderType.getEntityCutout(getTextureForMonster(cap.getMonsterType()))), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
			}

		});
	}
}
