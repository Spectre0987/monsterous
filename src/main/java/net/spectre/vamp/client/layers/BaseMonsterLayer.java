package net.spectre.vamp.client.layers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.cap.MonsterCapability;
import net.spectre.vamp.monsters.MonsterType;

import java.util.function.Predicate;

public class BaseMonsterLayer extends LayerRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> {

    public BipedModel<AbstractClientPlayerEntity> model;
    public ResourceLocation texture;
    public Predicate<MonsterType> types;

    public BaseMonsterLayer(PlayerRenderer renderer, BipedModel<AbstractClientPlayerEntity> model, ResourceLocation texture, Predicate<MonsterType> predicate) {
        super(renderer);
        this.model = model;
        this.texture = buildLocation(texture);
        this.types = predicate;
    }

    public ResourceLocation buildLocation(ResourceLocation loc){
        return new ResourceLocation(loc.getNamespace(), "textures/entity/" + loc.getPath() + ".png");
    }

    @Override
    public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, AbstractClientPlayerEntity player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) {
        player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
            if(this.types.test(cap.getMonsterType())){
                BipedModel<AbstractClientPlayerEntity> model = this.getModelForPlayer(player);
                this.getEntityModel().setModelAttributes(model);
                model.setLivingAnimations(player, limbSwing, limbSwingAmount, partialTicks);
                model.render(matrixStackIn, bufferIn.getBuffer(this.getRenderType(this.getTextureForPlayer(player))), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
                this.removeParts(this.getEntityModel());
            }
        });
    }

    public BipedModel<AbstractClientPlayerEntity> getModelForPlayer(PlayerEntity player){
        return this.model;
    }

    public ResourceLocation getTextureForPlayer(PlayerEntity player){
        return this.texture;
    }

    public RenderType getRenderType(ResourceLocation loc){
        return RenderType.getEntityCutout(loc);
    }

    public void removeParts(PlayerModel<?> player){}

}
