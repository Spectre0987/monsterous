package net.spectre.vamp.client.layers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3f;
import net.spectre.vamp.Constants;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.client.models.body.GlowEyeModel;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.monsters.MonsterType;

public class GlowEyeLayer<T extends LivingEntity, M extends BipedModel<T>> extends LayerRenderer<T, M> {

    private static final ResourceLocation TEXTURE = Helper.createRL("textures/entity/eyes.png");
    private final GlowEyeModel<T> model = new GlowEyeModel<>();

    public GlowEyeLayer(IEntityRenderer<T, M> entityRendererIn) {
        super(entityRendererIn);
    }

    @Override
    public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) {
        entity.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
            if(cap.getMonsterType() == MonsterRegistry.DEMON.get()){
                this.getEntityModel().setModelAttributes(model);

                Vector3f color = this.getColor(cap.getMonsterType());
                float alpha = calcAlpha(entity);
                model.render(matrixStackIn, bufferIn.getBuffer(RenderType.getEntityCutoutNoCull(TEXTURE)), Constants.MAX_LIGHT, OverlayTexture.NO_OVERLAY, color.getX(), color.getY(), color.getZ(), alpha);
            }
        });
    }

    public Vector3f getColor(MonsterType type){
        return new Vector3f(1.0F, 0.0F, 0.0F);
    }

    public float calcAlpha(T entity){
        if(Minecraft.getInstance().player == null)
            return 0;

        float dist = (float)Math.sqrt(Minecraft.getInstance().player.getPositionVec().squareDistanceTo(entity.getPositionVec()));


        if(dist > 30)
            return 1.0F;

        if(dist < 30)
            return 0.0F;

        //Transition, at this point dist is 25-30

        return 1.0F - (30.0F - dist) / 10.0F;


    }
}
