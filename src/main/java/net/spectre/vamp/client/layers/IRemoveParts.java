package net.spectre.vamp.client.layers;

import net.minecraft.client.renderer.model.Model;
import net.minecraft.entity.player.PlayerEntity;

public interface IRemoveParts<T extends Model> {

    void toggle(T model, boolean show);
    boolean shouldRender(PlayerEntity player);
}
