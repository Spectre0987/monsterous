package net.spectre.vamp.client.layers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.Constants;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.client.models.body.SmiteModel;
import net.spectre.vamp.helpers.Helper;

public class SmiteLayer<T extends LivingEntity, M extends BipedModel<T>> extends LayerRenderer<T, M> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/entity/smite.png");
    public final SmiteModel<T> MODEL = new SmiteModel<>();

    public SmiteLayer(IEntityRenderer<T, M> entityRendererIn) {
        super(entityRendererIn);
    }

    @Override
    public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) {
        entity.getCapability(Capabilities.SMITE).ifPresent(cap -> {
            if(cap.isBeingSmote()){
                this.getEntityModel().setModelAttributes(MODEL);
                MODEL.setRotationAngles(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
                MODEL.render(matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(TEXTURE)), Constants.MAX_LIGHT, OverlayTexture.NO_OVERLAY, cap.getSmiteColor().getRed(), cap.getSmiteColor().getGreen(), cap.getSmiteColor().getBlue(), 0.75F);
            }
        });
    }
}
