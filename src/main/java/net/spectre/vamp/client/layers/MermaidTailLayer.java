package net.spectre.vamp.client.layers;

import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.entity.player.PlayerEntity;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.client.models.body.MermaidTailModel;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.misc.ObjectWrapper;
import net.spectre.vamp.monsters.MonsterRegistry;

public class MermaidTailLayer extends BaseMonsterLayer implements IRemoveParts<PlayerModel<?>>{

    public MermaidTailLayer(PlayerRenderer renderer) {
        super(renderer, new MermaidTailModel(), Helper.createRL("tail"), type -> type == MonsterRegistry.MERMAID.get());
    }

    @Override
    public void removeParts(PlayerModel<?> player) {
        super.removeParts(player);
    }

    @Override
    public void toggle(PlayerModel<?> model, boolean show) {
        model.bipedLeftLeg.showModel = show;
        model.bipedLeftLegwear.showModel = show;
        model.bipedRightLeg.showModel = show;
        model.bipedRightLegwear.showModel = show;
    }

    @Override
    public boolean shouldRender(PlayerEntity player) {

        ObjectWrapper<Boolean> result = new ObjectWrapper(false);
        player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
            if(cap.getMonsterType() == MonsterRegistry.MERMAID.get())
                result.set(true);
        });

        return result.get();
    }
}
