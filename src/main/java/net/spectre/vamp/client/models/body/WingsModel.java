package net.spectre.vamp.client.models.body;// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.monsters.IFlyingMonster;

public class WingsModel extends BipedModel<AbstractClientPlayerEntity> {

	private final ModelRenderer wings;
	private final ModelRenderer leftwing;
	private final ModelRenderer rightwing;

	public WingsModel() {
		super(0.0625F);
		textureWidth = 32;
		textureHeight = 32;

		bipedBody = new ModelRenderer(this);
		bipedBody.setRotationPoint(0.0F, 0.0F, 0.0F);
		

		wings = new ModelRenderer(this);
		wings.setRotationPoint(2.0F, 3.0F, 2.0F);
		bipedBody.addChild(wings);
		wings.setTextureOffset(8, 0).addBox(-4.0F, -2.0F, 0.0F, 4.0F, 4.0F, 2.0F, 0.0F, false);

		leftwing = new ModelRenderer(this);
		leftwing.setRotationPoint(-0.0833F, -2.0F, 1.75F);
		wings.addChild(leftwing);
		leftwing.setTextureOffset(14, 6).addBox(0.0833F, 0.0F, -1.0F, 3.0F, 5.0F, 1.0F, 0.0F, false);
		leftwing.setTextureOffset(4, 15).addBox(3.0833F, -3.0F, -1.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);
		leftwing.setTextureOffset(0, 0).addBox(4.0833F, -5.0F, -1.0F, 3.0F, 14.0F, 1.0F, 0.0F, false);
		leftwing.setTextureOffset(0, 15).addBox(9.0833F, -3.0F, -1.0F, 1.0F, 15.0F, 1.0F, 0.0F, false);
		leftwing.setTextureOffset(8, 8).addBox(7.0833F, -4.0F, -1.0F, 2.0F, 14.0F, 1.0F, 0.0F, false);
		leftwing.setTextureOffset(14, 14).addBox(10.0833F, -2.0F, -1.0F, 1.0F, 16.0F, 1.0F, 0.0F, false);

		rightwing = new ModelRenderer(this);
		rightwing.setRotationPoint(-4.0833F, -2.0F, 1.75F);
		wings.addChild(rightwing);
		setRotationAngle(rightwing, 0.0F, 3.1416F, 0.0F);
		rightwing.setTextureOffset(14, 6).addBox(-0.0833F, 0.0F, 0.0F, 3.0F, 5.0F, 1.0F, 0.0F, false);
		rightwing.setTextureOffset(4, 15).addBox(2.9167F, -3.0F, 0.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);
		rightwing.setTextureOffset(0, 0).addBox(3.9167F, -5.0F, 0.0F, 3.0F, 14.0F, 1.0F, 0.0F, false);
		rightwing.setTextureOffset(0, 15).addBox(8.9167F, -3.0F, 0.0F, 1.0F, 15.0F, 1.0F, 0.0F, false);
		rightwing.setTextureOffset(8, 8).addBox(6.9167F, -4.0F, 0.0F, 2.0F, 14.0F, 1.0F, 0.0F, false);
		rightwing.setTextureOffset(14, 14).addBox(9.9167F, -2.0F, 0.0F, 1.0F, 16.0F, 1.0F, 0.0F, false);

		this.setVisible(false);
		this.bipedBody.showModel = true;
	}

	@Override
	public void setLivingAnimations(AbstractClientPlayerEntity entityIn, float limbSwing, float limbSwingAmount, float partialTick) {
		super.setLivingAnimations(entityIn, limbSwing, limbSwingAmount, partialTick);

		entityIn.getCapability(Capabilities.MONSTER).ifPresent(cap -> {

			if(cap.getTypeObject() instanceof IFlyingMonster){
				IFlyingMonster fly = (IFlyingMonster) cap.getTypeObject();

				if(fly.getPose() == IFlyingMonster.FlyingPose.FLYING){

					this.rightwing.rotateAngleY = (float)Math.toRadians((Math.sin(entityIn.ticksExisted * 0.3) * 20) - 140);
					this.leftwing.rotateAngleY = (float)Math.toRadians((Math.sin(entityIn.ticksExisted * 0.3) * -20) - 40);

				}
				else if(fly.getPose() == IFlyingMonster.FlyingPose.LANDED){
					this.rightwing.rotateAngleY = (float) Math.toRadians(270 - (Math.cos(entityIn.ticksExisted * 0.05) * 10) - 10);
					this.leftwing.rotateAngleY = -(float) Math.toRadians(90 - (Math.cos(entityIn.ticksExisted * 0.05) * 10) - 10);
				}

			}

		});

	}

	public double getSpeed(Entity e){
		double disX = Math.abs(e.getPosX() - e.prevPosX);
		double disY = Math.abs(e.getPosY() - e.prevPosY);
		double disZ = Math.abs(e.getPosZ() - e.prevPosZ);
		return disX + disY + disZ;
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}