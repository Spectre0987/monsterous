package net.spectre.vamp.client.models.armor;
// Made with Blockbench 3.9.2
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports


import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;

public class CapeModel extends BipedModel<LivingEntity> {

	public CapeModel() {
		super(0.0F);
		textureWidth = 64;
		textureHeight = 64;

		bipedBody = new ModelRenderer(this);
		bipedBody.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedBody.setTextureOffset(0, 0).addBox(-5.0F, 0.0F, -3.0F, 10.0F, 22.0F, 6.0F, 0.0F, false);
		bipedBody.setTextureOffset(0, 31).addBox(-6.0F, -5.0F, -3.0F, 12.0F, 5.0F, 7.0F, 0.0F, false);

		this.setVisible(false);
		this.bipedBody.showModel = true;
	}
}