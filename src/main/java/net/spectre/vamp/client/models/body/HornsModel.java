package net.spectre.vamp.client.models.body;// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;

public class HornsModel extends BipedModel<AbstractClientPlayerEntity> {

	private final ModelRenderer horns;
	private final ModelRenderer bone2;

	public HornsModel() {
		super(0.0625F);
		textureWidth = 16;
		textureHeight = 16;

		bipedHead = new ModelRenderer(this);
		bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
		

		horns = new ModelRenderer(this);
		horns.setRotationPoint(0.0F, -7.0F, -3.0F);
		bipedHead.addChild(horns);
		setRotationAngle(horns, 1.0472F, 0.0F, 0.0F);
		horns.setTextureOffset(0, 0).addBox(1.5F, -4.0F, -1.5F, 2.0F, 4.0F, 2.0F, -0.25F, false);
		horns.setTextureOffset(0, 6).addBox(-3.5F, -4.0F, -1.5F, 2.0F, 4.0F, 2.0F, -0.25F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, -3.4372F, -0.0884F);
		horns.addChild(bone2);
		setRotationAngle(bone2, -1.5272F, 0.0F, 0.0F);
		bone2.setTextureOffset(8, 0).addBox(2.0F, -3.3531F, -0.3632F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		bone2.setTextureOffset(8, 8).addBox(-3.0F, -3.3531F, -0.3632F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		this.setVisible(false);
		this.bipedHead.showModel = true;

	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}