package net.spectre.vamp.client.models;// Made with Blockbench 3.8.3
// Exported for Minecraft version 1.15 - 1.16
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.helpers.Helper;

public class NetModel extends Model {
    public static final NetModel INSTANCE = new NetModel();
	public static final ResourceLocation TEXTURE = Helper.createRL("textures/misc/net.png");

	private final ModelRenderer bb_main;

	public NetModel() {
		super(RenderType::getEntityCutoutNoCull);
		textureWidth = 128;
		textureHeight = 128;

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.setTextureOffset(0, 0).addBox(-9.0F, -33.0F, -9.0F, 18.0F, 32.0F, 18.0F, 0.0F, false);
	}


	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {
		bb_main.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);
	}
}