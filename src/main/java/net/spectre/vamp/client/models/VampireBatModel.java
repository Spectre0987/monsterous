package net.spectre.vamp.client.models;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.BatModel;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.Pose;
import net.minecraft.entity.passive.BatEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class VampireBatModel extends BatModel {

    private static DummyBat DUMMY_BAT = null;

    public VampireBatModel(){
        this.isChild = false;
        this.isSitting = false;
    }

    public void animate(PlayerEntity player){
        //this.setLivingAnimations(this.getDummyBat(), player.limbSwing, player.limbSwingAmount, Minecraft.getInstance().getRenderPartialTicks());

        float partialTicks = Minecraft.getInstance().getRenderPartialTicks();
        float f = MathHelper.interpolateAngle(partialTicks, player.prevRenderYawOffset, player.renderYawOffset);
        float f1 = MathHelper.interpolateAngle(partialTicks, player.prevRotationYawHead, player.rotationYawHead);
        float trueHeadYaw = f1 - f;

        this.setRotationAngles(this.getDummyBat(), player.limbSwing, player.limbSwingAmount, player.ticksExisted, trueHeadYaw, player.rotationPitch);
    }

    public DummyBat getDummyBat(){
        if(DUMMY_BAT == null)
            DUMMY_BAT = new DummyBat(Minecraft.getInstance().world);

        return DUMMY_BAT;
    }

    public static class DummyBat extends BatEntity{

        public DummyBat(World worldIn) {
            super(EntityType.BAT, worldIn);
        }

        @Override
        public boolean getIsBatHanging() {
            return false;
        }
    }

}
