package net.spectre.vamp.client.models.body;// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;

public class SmiteModel<T extends LivingEntity> extends BipedModel<T> {

	public SmiteModel() {
		super(0.0625F);
		textureWidth = 256;
		textureHeight = 256;

		bipedHead = new ModelRenderer(this);
		bipedHead.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedHead.setTextureOffset(59, 59).addBox(-3.0F, -5.0F, -59.0F, 2.0F, 2.0F, 55.0F, 0.0F, false);
		bipedHead.setTextureOffset(0, 57).addBox(1.0F, -5.0F, -59.0F, 2.0F, 2.0F, 55.0F, 0.0F, false);
		bipedHead.setTextureOffset(0, 0).addBox(-1.0F, -3.0F, -59.0F, 2.0F, 2.0F, 55.0F, 0.0F, false);

		this.setVisible(false);
		this.bipedHead.showModel = true;
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}