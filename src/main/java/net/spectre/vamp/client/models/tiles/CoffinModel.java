package net.spectre.vamp.client.models.tiles;// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.spectre.vamp.tiles.CoffinTile;

public class CoffinModel extends Model {
	private final ModelRenderer Body;
	private final ModelRenderer Door;
	private final ModelRenderer cube_r1;

	public CoffinModel() {
		super(RenderType::getEntityCutoutNoCull);
		textureWidth = 128;
		textureHeight = 128;

		Body = new ModelRenderer(this);
		Body.setRotationPoint(0.0F, 19.0556F, 7.5F);
		setRotationAngle(Body, 1.5708F, 0.0F, 3.1416F);
		Body.setTextureOffset(0, 38).addBox(-8.0F, 2.5F, -6.0556F, 1.0F, 17.0F, 11.0F, 0.0F, true);
		Body.setTextureOffset(0, 38).addBox(7.0F, 2.5F, -6.0556F, 1.0F, 17.0F, 11.0F, 0.0F, false);
		Body.setTextureOffset(59, 61).addBox(-8.0F, -14.5F, -6.0556F, 1.0F, 6.0F, 11.0F, 0.0F, true);
		Body.setTextureOffset(59, 61).addBox(7.0F, -14.5F, -6.0556F, 1.0F, 6.0F, 11.0F, 0.0F, false);
		Body.setTextureOffset(13, 61).addBox(-9.0F, -9.5F, -6.0556F, 1.0F, 13.0F, 11.0F, 0.0F, true);
		Body.setTextureOffset(13, 61).addBox(8.0F, -9.5F, -6.0556F, 1.0F, 13.0F, 11.0F, 0.0F, false);
		Body.setTextureOffset(70, 28).addBox(-8.0F, -15.5F, -6.0556F, 16.0F, 1.0F, 11.0F, 0.0F, false);
		Body.setTextureOffset(36, 0).addBox(-8.0F, 19.5F, -6.0556F, 16.0F, 1.0F, 11.0F, 0.0F, false);
		Body.setTextureOffset(36, 36).addBox(-8.0F, -15.5F, 3.9444F, 16.0F, 35.0F, 1.0F, 0.0F, false);

		Door = new ModelRenderer(this);
		Door.setRotationPoint(-9.0F, 13.0F, 8.0F);
		setRotationAngle(Door, 1.5708F, 0.0F, 3.1416F);
		Door.setTextureOffset(0, 0).addBox(-17.0F, -16.0F, -2.0F, 16.0F, 36.0F, 2.0F, 0.0F, false);
		Door.setTextureOffset(36, 12).addBox(-18.0F, -10.0F, -2.0F, 18.0F, 13.0F, 2.0F, 0.0F, false);
		Door.setTextureOffset(37, 29).addBox(-17.0F, -5.0F, -2.75F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(-17.25F, -1.725F, -2.625F);
		Door.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.1745F, 0.0F, 0.2182F);
		cube_r1.setTextureOffset(41, 29).addBox(0.1927F, -3.2401F, 0.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		Body.render(matrixStack, buffer, packedLight, packedOverlay);
		Door.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void renderCoffin(MatrixStack matrix, IVertexBuilder buffer, CoffinTile tile, int packedLight){

		this.Door.rotateAngleZ = (float) Math.toRadians(tile.isOpen() ? 60 : 180);

		this.render(matrix, buffer, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}