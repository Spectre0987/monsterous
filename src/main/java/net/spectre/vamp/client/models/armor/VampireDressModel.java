package net.spectre.vamp.client.models.armor;// Made with Blockbench 3.9.2
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports


import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;

public class VampireDressModel extends BipedModel<LivingEntity> {

	public VampireDressModel() {
		super(0.0F);
		textureWidth = 128;
		textureHeight = 128;

		bipedBody = new ModelRenderer(this);
		bipedBody.setRotationPoint(0.0F, 0.0F, 0.0F);
		bipedBody.setTextureOffset(0, 12).addBox(-4.5F, 0.0F, -2.5F, 9.0F, 24.0F, 5.0F, 0.0F, false);
		bipedBody.setTextureOffset(28, 12).addBox(-4.5F, -4.0F, -2.5F, 9.0F, 5.0F, 7.0F, 0.0F, false);
		bipedBody.setTextureOffset(0, 0).addBox(-12.5F, 24.0F, -2.5F, 24.0F, 0.0F, 12.0F, 0.0F, false);
	}

}