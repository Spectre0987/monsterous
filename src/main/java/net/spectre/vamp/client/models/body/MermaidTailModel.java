package net.spectre.vamp.client.models.body;// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Pose;

public class MermaidTailModel extends BipedModel<AbstractClientPlayerEntity> {

	private final ModelRenderer tail;
	private final ModelRenderer section1;
	private final ModelRenderer leftLeg_r1;
	private final ModelRenderer section2;
	private final ModelRenderer leftLeg_r2;
	private final ModelRenderer fin;

	public MermaidTailModel() {
		super(0.0625F);
		textureWidth = 32;
		textureHeight = 32;

		bipedBody = new ModelRenderer(this);
		bipedBody.setRotationPoint(0.0F, 0.0F, 0.0F);
		

		tail = new ModelRenderer(this);
		tail.setRotationPoint(0.05F, 12.5F, -2.0F);
		bipedBody.addChild(tail);
		setRotationAngle(tail, 0.3491F, 0.0F, 0.0F);
		tail.setTextureOffset(0, 0).addBox(-3.95F, -0.5F, 0.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);

		section1 = new ModelRenderer(this);
		section1.setRotationPoint(-1.95F, -0.5F, 2.0F);
		tail.addChild(section1);
		

		leftLeg_r1 = new ModelRenderer(this);
		leftLeg_r1.setRotationPoint(2.0F, 6.25F, -2.0F);
		section1.addChild(leftLeg_r1);
		setRotationAngle(leftLeg_r1, 0.5672F, 0.0F, 0.0F);
		leftLeg_r1.setTextureOffset(0, 10).addBox(-3.5F, -0.25F, 0.25F, 7.0F, 6.0F, 3.0F, 0.0F, false);

		section2 = new ModelRenderer(this);
		section2.setRotationPoint(2.0F, 10.1427F, 2.0531F);
		section1.addChild(section2);
		

		leftLeg_r2 = new ModelRenderer(this);
		leftLeg_r2.setRotationPoint(0.0F, -1.2265F, -7.4071F);
		section2.addChild(leftLeg_r2);
		setRotationAngle(leftLeg_r2, 0.5672F, 0.0F, 0.0F);
		leftLeg_r2.setTextureOffset(0, 19).addBox(-4.5F, 8.3035F, 6.0113F, 9.0F, 6.0F, 0.0F, 0.0F, false);
		leftLeg_r2.setTextureOffset(18, 18).addBox(-2.5F, 5.3035F, 5.0113F, 5.0F, 3.0F, 2.0F, 0.0F, false);

		fin = new ModelRenderer(this);
		fin.setRotationPoint(0.0F, 0.0F, 0.0F);
		section2.addChild(fin);

		this.setVisible(false);
		this.bipedBody.showModel = true;
		
	}

	@Override
	public void setLivingAnimations(AbstractClientPlayerEntity entityIn, float limbSwing, float limbSwingAmount, float partialTick) {
		super.setLivingAnimations(entityIn, limbSwing, limbSwingAmount, partialTick);

		if(entityIn.getPose() == Pose.SWIMMING){
			this.tail.rotateAngleX = getAngle(limbSwing, 20.0F);
			this.section1.rotateAngleX = getAngle(limbSwing, 15);
			this.section2.rotateAngleX = getAngle(limbSwing, 10);
			this.fin.rotateAngleX = getAngle(limbSwing, 5);
		}
		else{
			this.tail.rotateAngleX = (float) Math.toRadians(0);
			this.section1.rotateAngleX = (float) Math.toRadians(0);
			this.section2.rotateAngleX = (float) Math.toRadians(45);
			this.fin.rotateAngleX = (float) Math.toRadians(Math.sin(limbSwing) * 10);
		}


	}

	public float getAngle(float limbSwing, float angle){
		return (float) Math.toRadians((Math.sin(limbSwing * 0.4) * angle) - (angle / 2.0F));
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}