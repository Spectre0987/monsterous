package net.spectre.vamp.client.helpers;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.BufferBuilder;

import java.util.function.Function;

public class GuiHelper {


    public static void renderCooldown(BufferBuilder bb, int x, int y, double percent) {

        Function<IVertexBuilder, IVertexBuilder> color = (buf) -> buf.color(0F, 1.0F, 0F, 0.5F);

        color.apply(bb.pos(x - 8, y + (16 * percent), 20)).endVertex();
        color.apply(bb.pos(x - 8, y + 16, 20)).endVertex();
        color.apply(bb.pos(x - 7, y + 16, 20)).endVertex();
        color.apply(bb.pos(x - 7, y + (16 * percent), 20)).endVertex();

    }

}
