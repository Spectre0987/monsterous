package net.spectre.vamp.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.LivingEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ParticleFactoryRegisterEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.client.layers.*;
import net.spectre.vamp.client.models.armor.CapeModel;
import net.spectre.vamp.client.models.armor.VampireDressModel;
import net.spectre.vamp.client.models.body.HornsModel;
import net.spectre.vamp.client.overlay.*;
import net.spectre.vamp.client.particles.BubbleParticle;
import net.spectre.vamp.client.particles.CauldronSmokeParticle;
import net.spectre.vamp.client.renderers.tiles.CoffinRenderer;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.items.clothes.ClothesData;
import net.spectre.vamp.items.clothes.ClothesType;
import net.spectre.vamp.items.data.ClothesManager;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.monsters.MonsterType;
import net.spectre.vamp.proxy.ClientProxy;
import net.spectre.vamp.tiles.VTiles;
import org.lwjgl.glfw.GLFW;

import java.util.HashMap;

@Mod.EventBusSubscriber(modid = Monsterous.MODID, bus = Bus.MOD, value = Dist.CLIENT)
public class ModelRegistry {

	public static final KeyBinding SCROLL_NEXT = new KeyBinding(Monsterous.MODID + ".power.next", GLFW.GLFW_KEY_R, Monsterous.MODID);
	public static final KeyBinding SCROLL_PREV = new KeyBinding(Monsterous.MODID + ".power.prev", GLFW.GLFW_KEY_F, Monsterous.MODID);
	public static final KeyBinding SCROLL_ACT = new KeyBinding(Monsterous.MODID + ".power.do", GLFW.GLFW_KEY_C, Monsterous.MODID);
	
	public static final HashMap<MonsterType, BaseOverlay> MONSTER_OVERLAY = new HashMap<MonsterType, BaseOverlay>();
	
	@SubscribeEvent
	public static void registerModels(FMLClientSetupEvent event) {

		Monsterous.PROXY = new ClientProxy();
		
		ClientRegistry.registerKeyBinding(SCROLL_ACT);
		ClientRegistry.registerKeyBinding(SCROLL_NEXT);
		ClientRegistry.registerKeyBinding(SCROLL_PREV);
		
		
		for(PlayerRenderer render : Minecraft.getInstance().getRenderManager().getSkinMap().values()) {
			render.addLayer(new WingsLayer(render));
			//Add horns
			render.addLayer(new BaseMonsterLayer(render, new HornsModel(), Helper.createRL("horns"), type -> type == MonsterRegistry.DEMON.get()));
			render.addLayer(new MermaidTailLayer(render));
			render.addLayer(new SmiteLayer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>>(render));
			render.addLayer(new GlowEyeLayer<>(render));
			//render.addLayer(new HaloLayer(render));
		}

		//Add Smite layer to all bipeds
		for(EntityRenderer<?> render : Minecraft.getInstance().getRenderManager().renderers.values()){
			if(render instanceof LivingRenderer){
				LivingRenderer<?, ?> live = ((LivingRenderer<?, ?>) render);
				if(live.getEntityModel() instanceof BipedModel){
					addRenderLayer((LivingRenderer<LivingEntity, BipedModel<LivingEntity>>)live);
				}
			}
		}

		ClothesManager.addClothesData(ClothesType.CAPE, new ClothesData().setModel(new CapeModel()).setTexture(Helper.createRL("cape")));
		ClothesManager.addClothesData(ClothesType.DRESS, new ClothesData().setModel(new VampireDressModel()).setTexture(Helper.createRL("vampire_dress")));

		//ItemPredicate.register();

		RenderTypeLookup.setRenderLayer(VBlocks.DEMON_ROSE.get(), RenderType.getCutout());
		RenderTypeLookup.setRenderLayer(VBlocks.DEMON_TRAP.get(), RenderType.getCutout());
		RenderTypeLookup.setRenderLayer(VBlocks.CLOUD.get(), RenderType.getTranslucent());
		RenderTypeLookup.setRenderLayer(VBlocks.SUMMON_CIRCLE.get(), RenderType.getCutout());

		//Overlays
		registerOverlay(MonsterRegistry.ANGEL.get(), new AngelOverlay());
		registerOverlay(MonsterRegistry.MERMAID.get(), new MermaidOverlay());
		registerOverlay(MonsterRegistry.VAMPIRE.get(), new VampOverlay());
		registerOverlay(MonsterRegistry.DEMON.get(), new DemonOverlay());

		ClientRegistry.bindTileEntityRenderer(VTiles.COFFIN.get(), CoffinRenderer::new);
	}

	public static <T extends LivingEntity, M extends BipedModel<T>> void addRenderLayer(LivingRenderer<T, M> render){
		render.addLayer(new SmiteLayer<>(render));
	}

	public static void registerOverlay(MonsterType type, BaseOverlay overlay){
		MONSTER_OVERLAY.put(type, overlay);
	}
}
