package net.spectre.vamp.misc;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextProcessing;
import net.minecraft.util.text.TranslationTextComponent;

public class TextHelper {

    public static ITextComponent createToolTipTranslation(Item item, String extra, Object... objects) {
        ResourceLocation key = item.getRegistryName();
        String translationKey = "tooltip." + key.getNamespace() + "." + key.getPath().replaceAll("/", ".") + (extra.isEmpty() ? "" : ("." + extra));
        return objects == null ? new TranslationTextComponent(translationKey) : new TranslationTextComponent(translationKey, objects);
    }

    public static ITextComponent createToolTipTranslation(Item item, String extra) {
        return createToolTipTranslation(item, extra, null);
    }

    public static ITextComponent createToolTipTranslation(Item item) {
        return createToolTipTranslation(item, "", null);
    }

}
