package net.spectre.vamp.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;
import net.spectre.vamp.powers.Power;
import net.spectre.vamp.powers.Powers;

public class PowerCooldown implements INBTSerializable<CompoundNBT> {

    private Power power;
    private int cooldown;

    public PowerCooldown(Power power){
        this.power = power;
        this.cooldown = power.getMaxCoolDown();
    }

    public PowerCooldown(CompoundNBT tag){
        this.deserializeNBT(tag);
    }

    public void tick(){
        if(this.cooldown > 0)
            --this.cooldown;
    }

    public void startCooldown(){
        this.cooldown = power.getMaxCoolDown();
    }

    public void reset(){
        this.cooldown = 0;
    }

    public boolean canUsePower(){
        System.out.println("Tryed to use power " + power.getRegistryName().toString() + " with cooldown " + this.cooldown);
        return this.cooldown <= 0;
    }

    public float getCooldownPercent(){
        return this.cooldown / (float)power.getMaxCoolDown();
    }

    public boolean matches(Power power) {
        return power == this.power;
    }

    public void setCooldown(int cooldown){
        this.cooldown = cooldown;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        if(power != null)
            tag.putString("id", power.getRegistryName().toString());
        tag.putInt("cooldown", this.cooldown);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        this.power = Powers.POWER_REGISTRY.get().getValue(new ResourceLocation(tag.getString("id")));
        this.cooldown = tag.getInt("cooldown");
    }
}
