package net.spectre.vamp.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.INBTSerializable;

public class DimensionPosition implements INBTSerializable<CompoundNBT> {

    private ResourceLocation world;
    private BlockPos pos;

    public DimensionPosition(ResourceLocation world, BlockPos pos){
        this.world = world;
        this.pos = pos;
    }
    public DimensionPosition(World world, BlockPos pos){
        this(world.getDimensionKey().getRegistryName(), pos);
    }


    public DimensionPosition(CompoundNBT nbt){
        this.deserializeNBT(nbt);
    }

    public BlockPos getPosition(){
        return this.pos;
    }

    @Override
    public CompoundNBT serializeNBT() {

        CompoundNBT tag = new CompoundNBT();
        tag.putString("dimension", this.world.toString());
        tag.putLong("position", this.pos.toLong());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.world = new ResourceLocation(nbt.getString("dimension"));
        this.pos = BlockPos.fromLong(nbt.getLong("position"));
    }
}
