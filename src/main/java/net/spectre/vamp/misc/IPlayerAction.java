package net.spectre.vamp.misc;

import net.minecraft.entity.player.PlayerEntity;

@FunctionalInterface
public interface IPlayerAction {
    
    void act(PlayerEntity player);
    
}
