package net.spectre.vamp.misc;

import com.google.common.collect.Maps;
import net.minecraft.loot.*;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.items.VItems;

import java.util.HashMap;

public class LootTableEdit {

    private static final HashMap<ResourceLocation, LootPool> EDITS = Maps.newHashMap();

    static{
        EDITS.put(LootTables.CHESTS_SIMPLE_DUNGEON, genDungeonLoot());
        EDITS.put(LootTables.CHESTS_BURIED_TREASURE, genBuriedTreasureLoot());
        EDITS.put(LootTables.CHESTS_NETHER_BRIDGE, genNetherLoot());
    }

    public static LootPool getLootPool(ResourceLocation name) {
        return EDITS.get(name);
    }

    public static LootPool genDungeonLoot(){
        LootEntry.Builder vamp = ItemLootEntry.builder(VItems.BLOOD_BOTTLE.get())
                .weight(25);

        LootEntry.Builder empty = EmptyLootEntry.func_216167_a()
                .weight(75);

        return LootPool.builder()
                .name(Monsterous.MODID + ":vamp_edit")
                .rolls(ConstantRange.of(1))
                .addEntry(vamp)
                .addEntry(empty)
                .build();
    }

    public static LootPool genBuriedTreasureLoot(){
        LootEntry.Builder mermaid = ItemLootEntry.builder(VItems.MERMAID_POTION.get())
                .weight(75);

        LootEntry.Builder empty = EmptyLootEntry.func_216167_a()
                .weight(25);

        return LootPool.builder()
                .name(Monsterous.MODID + ":mermaid_edit")
                .rolls(ConstantRange.of(1))
                .addEntry(mermaid)
                .addEntry(empty)
                .build();
    }

    public static LootPool genNetherLoot(){
        LootEntry.Builder blood = ItemLootEntry.builder(VItems.DEMON_BLOOD.get())
                .weight(50);

        LootEntry.Builder demonKnife = ItemLootEntry.builder(VItems.DEMON_KNIFE.get())
                .weight(10);

        LootEntry.Builder empty = ItemLootEntry.builder(VItems.CROSS.get())
                .weight(35);

        return LootPool.builder()
                .name(Monsterous.MODID + ":hell_edit")
                .rolls(ConstantRange.of(1))
                .addEntry(blood)
                .addEntry(demonKnife)
                .addEntry(empty)
                .build();
    }
}
