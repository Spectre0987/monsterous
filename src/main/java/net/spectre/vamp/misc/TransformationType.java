package net.spectre.vamp.misc;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;

public enum TransformationType {

    NONE(0, null),
    BAT(4800, EntitySize.fixed(0.25F, 0.25F));

    private int time;
    private EntitySize size;

    TransformationType(int time, EntitySize size){
        this.time = time;
        this.size = size;
    }

    public int getTransformationTime() {
        return time;
    }

    public EntitySize getSize(){
        return this.size;
    }
}
