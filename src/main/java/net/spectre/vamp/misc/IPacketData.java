package net.spectre.vamp.misc;

import net.minecraft.network.PacketBuffer;

public interface IPacketData {

    void encode(PacketBuffer buf);
    void decode(PacketBuffer buf);

}
