package net.spectre.vamp.misc;

public class ObjectWrapper<T> {

    T value;

    public ObjectWrapper(T value){
        this.value = value;
    }

    public void set(T value){
        this.value = value;
    }

    public T get(){
        return this.value;
    }

}
