package net.spectre.vamp.misc;

import java.util.function.Supplier;

public class NonRegistry<T> {

    private final Supplier<T> objectSupplier;
    private T object;

    public NonRegistry(Supplier<T> tSupplier){
        this.objectSupplier = tSupplier;
    }

    public T get(){
        if(object == null)
            object = objectSupplier.get();
        return object;
    }

    public static <T> NonRegistry<T> of(Supplier<T> creator){
        return new NonRegistry<T>(creator);
    }

}
