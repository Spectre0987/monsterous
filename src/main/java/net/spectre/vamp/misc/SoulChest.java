package net.spectre.vamp.misc;

import com.google.common.collect.Lists;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.items.ItemStackHandler;

import java.util.List;
import java.util.UUID;

public class SoulChest implements INBTSerializable<CompoundNBT> {

    private int timeToUnlock;
    private ItemStackHandler inv;
    private UUID owner;
    private UUID demon;

    public SoulChest(List<ItemStack> stacks){
        inv = new ItemStackHandler(stacks.size());
        for(int i = 0; i < stacks.size(); ++i){
            inv.setStackInSlot(i, stacks.get(i).copy());
        }
    }

    public SoulChest(CompoundNBT nbt){
        this.deserializeNBT(nbt);
    }

    public boolean isUnlocked(){
        return this.timeToUnlock <= 0;
    }

    public boolean canOpen(PlayerEntity player){
        if(player.getUniqueID().equals(this.owner))
            return true;
        if(player.getUniqueID().equals(this.demon) && this.isUnlocked())
            return true;
        return false;
    }

    public ItemStackHandler getInventory(){
        return this.inv;
    }

    public void setDemon(UUID id){
        this.demon = id;
    }
    public void setOwner(UUID id){
        this.owner = id;
    }

    public UUID getOwner(){
        return this.owner;
    }

    public UUID getDemon(){
        return this.demon;
    }

    public void setupTimer(){
        //5 minutes
        this.timeToUnlock = 5 * 60 * 20;
    }

    public void tick(){
        if(this.timeToUnlock > 0)
            --this.timeToUnlock;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT nbt = new CompoundNBT();
        nbt.putUniqueId("owner", this.owner);
        nbt.putUniqueId("demon", this.demon);
        nbt.putInt("time", this.timeToUnlock);
        nbt.put("inv", this.inv.serializeNBT());
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.owner = nbt.getUniqueId("owner");
        this.demon = nbt.getUniqueId("demon");
        this.timeToUnlock = nbt.getInt("time");

        this.inv = new ItemStackHandler();
        inv.deserializeNBT(nbt.getCompound("inv"));
    }

    public static SoulChest createFromPlayer(PlayerEntity player, UUID demon){

        List<ItemStack> items = Lists.newArrayList();
        items.addAll(player.inventory.mainInventory);
        items.addAll(player.inventory.armorInventory);
        items.addAll(player.inventory.offHandInventory);

        SoulChest chest = new SoulChest(items);
        chest.setDemon(demon);
        chest.setOwner(player.getUniqueID());
        chest.setupTimer();
        return chest;
    }
}
