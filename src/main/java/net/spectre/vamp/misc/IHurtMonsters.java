package net.spectre.vamp.misc;

import net.spectre.vamp.monsters.MonsterType;

public interface IHurtMonsters {

    boolean canHurt(MonsterType type);
}
