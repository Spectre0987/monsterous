package net.spectre.vamp.datagen.gens;

import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.ItemTagsProvider;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.items.VItems;
import net.spectre.vamp.tags.VItemTags;

import javax.annotation.Nullable;

public class VItemTagProvider extends ItemTagsProvider {

    public VItemTagProvider(DataGenerator dataGenerator, @Nullable ExistingFileHelper existingFileHelper) {
        super(dataGenerator, new BlockTagsProvider(dataGenerator), Monsterous.MODID, existingFileHelper);
    }

    @Override
    protected void registerTags() {
        this.getOrCreateBuilder(VItemTags.COLT_AMMO).add(VItems.COLT_BULLET.get());

        this.getOrCreateBuilder(VItemTags.HOLY_ITEMS)
                .add(VItems.CROSS.get())
                .add(Items.TOTEM_OF_UNDYING);

        this.getOrCreateBuilder(VItemTags.HOT_DRINKS)
                .add(Items.SUSPICIOUS_STEW)
                .add(Items.MUSHROOM_STEW)
                .add(Items.RABBIT_STEW)
                .add(Items.BEETROOT_SOUP);
    }
}
