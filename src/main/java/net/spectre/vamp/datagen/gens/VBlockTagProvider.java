package net.spectre.vamp.datagen.gens;

import net.minecraft.block.Blocks;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.tags.VBlockTags;

public class VBlockTagProvider extends BlockTagsProvider {

    public VBlockTagProvider(DataGenerator generatorIn, ExistingFileHelper fileHelper) {
        super(generatorIn, Monsterous.MODID, fileHelper);
    }

    @Override
    protected void registerTags() {
        this.getOrCreateBuilder(VBlockTags.HEAT_SOURCE)
                .add(Blocks.FIRE)
                .add(Blocks.SOUL_CAMPFIRE)
                .add(Blocks.SOUL_FIRE)
                .add(Blocks.CAMPFIRE)
                .add(Blocks.MAGMA_BLOCK);
    }
}
