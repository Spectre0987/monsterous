package net.spectre.vamp.datagen.gens;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.IDataProvider;

public abstract class BasicDataProvider implements IDataProvider {

    private DataGenerator generator;
    private String name;

    public BasicDataProvider(DataGenerator generator, String name){
        this.generator = generator;
        this.name = name;
    }

    public DataGenerator getGenerator(){
        return this.generator;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
