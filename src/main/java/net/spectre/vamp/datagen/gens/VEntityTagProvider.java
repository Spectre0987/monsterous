package net.spectre.vamp.datagen.gens;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.EntityTypeTagsProvider;
import net.minecraft.entity.EntityType;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.tags.VEntityTags;

import javax.annotation.Nullable;

public class VEntityTagProvider extends EntityTypeTagsProvider {

    public VEntityTagProvider(DataGenerator generator, @Nullable ExistingFileHelper existingFileHelper) {
        super(generator, Monsterous.MODID, existingFileHelper);
    }


    @Override
    protected void registerTags() {
        this.getOrCreateBuilder(VEntityTags.MERMAID_EAT)
                .add(EntityType.CHICKEN,
                        EntityType.COD,
                        EntityType.GUARDIAN,
                        EntityType.SALMON,
                        EntityType.SQUID,
                        EntityType.TROPICAL_FISH);

        this.getOrCreateBuilder(VEntityTags.HELL_FRIEND)
                .add(EntityType.BLAZE)
                .add(EntityType.GHAST)
                .add(EntityType.PIGLIN)
                .add(EntityType.MAGMA_CUBE);
    }
}
