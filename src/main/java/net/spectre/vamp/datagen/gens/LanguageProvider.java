package net.spectre.vamp.datagen.gens;

import net.minecraft.data.DataGenerator;
import net.minecraft.util.DamageSource;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.items.VItems;

public class LanguageProvider extends net.minecraftforge.common.data.LanguageProvider {


    public LanguageProvider(DataGenerator gen) {
        super(gen, Monsterous.MODID, "en_us");
    }

    @Override
    protected void addTranslations() {
        add(VItems.ANGEL_POTION.get(), "Bottle of Angelic Grace");
        add(VItems.COLT_BULLET.get(), "Cursed Bullet");
        add(VItems.COLT.get(), "The Colt");
        add(VItems.CHALICE_OF_PURITY_FILLED.get(), "Filled Holy Grail");
        add(VItems.CHALICE_OF_PURITY.get(), "Holy Grail");
        add(VItems.BLOOD_BOTTLE.get(), "Bottle of Tainted Blood");
        add(VItems.CAPE.get(), "Gothic Cape");
        add(VItems.CHALK.get(), "Chalk");
        add(VItems.COFFIN.get(), "Coffin");
        add(VItems.CONTRACT.get(), "Demonic Contract");
        add(VItems.CONTRACT_FILLED.get(), "Signed Demonic Contract");
        add(VItems.CROSS.get(), "Cross");
        add(VItems.DEMON_BLOOD.get(), "Bottle of Demonic Essence");
        add(VItems.DEMON_KNIFE.get(), "Demon-Killing Knife");
        add(VItems.DEMON_ROSE.get(), "Demonic Rose");
        add(VItems.DRESS.get(), "Gothic Dress");
        add(VItems.MERMAID_POTION.get(), "Potion of the Merpeople");
        add(VItems.NAGA_POTION.get(), "Potion of Snakes");
        add(VItems.STORED_BLOOD.get(), "Bottle of Fresh Blood");
        add(VItems.VAMPIRE_ARROW.get(), "Dead Man's Blood Tipped Arrow");

        add(VBlocks.CLOUD.get(), "Solid Cloud");
        add(VBlocks.DEMON_TRAP.get(), "Demon Trap");
        add(VBlocks.SUMMON_CIRCLE.get(), "Demon Summoning Circle");

        addDamageTranslations(DamageSources.DEMONIC_RAM, "%s was rammed", "%s was rammed by %s");
        addDamageTranslations(DamageSources.OUT_OF_HELL, "%s Forgot to Return Home", "%s Forgot to Return Home");
        addDamageTranslations(DamageSources.KILL_ALL_COLT, "%s Was Taught Gun Safety", "%s Was Taught Gun Safety by %s");
        addDamageTranslations(DamageSources.MERMAID_CONSUME, "%s was drowned and eaten, if they were lucky in that order", "%s was drowned and eaten, if they were lucky in that order");
        addDamageTranslations(DamageSources.OUT_OF_CLOUDS, "% should have had their heads in the clouds");
        addDamageTranslations(DamageSources.SMITE, "%s was shown angelic grace", "%s experienced %s's wrath");
        addDamageTranslations(DamageSources.SUN, "%s forgot to apply sunscreen");
        addDamageTranslations(DamageSources.VAMPIRE, "$s was sucked dry", "%s was sucked dry by %s");

        add("itemGroup.monsterous", "Monstrous");

        //Tooltip
        add("tooltip.monsterous.colt", "Chambers loaded %s");
        add("tooltip.monsterous.waypoint", "Bound to %s");

        //Power messages
        add("monsterous.power.demon.hell.unsafe", "Area Unsafe!");


        //Contracts
        add("contracts.monsterous.haste", "Demonic Contract of Haste");
        add("contracts.monsterous.might", "Demonic Contract of Might");

    }

    public void addDamageTranslations(DamageSource source, String death, String killed){
        String base = "death.attack." + source.getDamageType();
        String killedString = base + ".player";

        add(base, death);
        add(killedString, killed);
    }

    public void addDamageTranslations(DamageSource source, String death){
        addDamageTranslations(source, death, death);
    }
}
