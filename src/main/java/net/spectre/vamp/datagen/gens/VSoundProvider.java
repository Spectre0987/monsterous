package net.spectre.vamp.datagen.gens;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.datagen.DataGen;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.sounds.Sounds;

import java.io.IOException;

public class VSoundProvider extends BasicDataProvider {

    public VSoundProvider(DataGenerator generator){
        super(generator, "Monsterous Sound Provider");
    }

    @Override
    public void act(DirectoryCache cache) throws IOException {

        JsonObject root = new JsonObject();

        this.addSound(root, Sounds.REVOLVER_COCK.get(), Helper.createRL("colt/cock"));
        this.addSound(root, Sounds.REVOLVER_LOAD.get(), Helper.createRL("colt/load"));
        this.addSound(root, Sounds.REVOLVER_SHOOT.get(), Helper.createRL("colt/shoot"));


        IDataProvider.save(DataGen.GSON, cache, root, this.getGenerator().getOutputFolder().resolve("assets/" + Monsterous.MODID + "/sounds.json"));
    }


    public void addSound(JsonObject root, SoundEvent event, ResourceLocation path){
        JsonObject sound = new JsonObject();
        sound.add("subtitle", new JsonPrimitive("subtitle." + event.getRegistryName().getNamespace() + "." + event.getRegistryName().getPath().replaceAll("/", ".")));
            JsonArray sounds = new JsonArray();
            sounds.add(path.toString());
            sound.add("sounds", sounds);

        root.add(event.getName().getPath(), sound);
    }
}
