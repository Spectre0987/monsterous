package net.spectre.vamp.datagen.gens;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.spectre.vamp.datagen.DataGen;
import net.spectre.vamp.items.VItems;

import java.io.IOException;
import java.nio.file.Path;

public class ItemModelProvider implements IDataProvider {

    private DataGenerator generator;

    public ItemModelProvider(DataGenerator generator){
        this.generator = generator;
    }

    @Override
    public void act(DirectoryCache cache) throws IOException {

    }

    public void generateBlockItemModel(Item item, DirectoryCache cache) throws IOException{

        ResourceLocation key = item.getRegistryName();

        JsonObject root = new JsonObject();
        root.add("parent", new JsonPrimitive(key.getNamespace() + ":block/" + key.getPath()));

        IDataProvider.save(DataGen.GSON, cache, root, getPath(item));

    }

    public Path getPath(Item item){
        return this.getPath(item.getRegistryName(), this.generator.getOutputFolder());
    }

    public Path getPath(ResourceLocation key, Path base){
        return base.resolve("assets/"  + key.getNamespace() + "/models/item/" + key.getPath() + ".json");
    }

    @Override
    public String getName() {
        return null;
    }
}
