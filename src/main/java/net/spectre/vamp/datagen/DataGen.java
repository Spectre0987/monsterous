package net.spectre.vamp.datagen;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.datagen.gens.*;

@Mod.EventBusSubscriber(modid = Monsterous.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGen {

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    @SubscribeEvent
    public static void registerDataGens(GatherDataEvent event){

        event.getGenerator().addProvider(new VEntityTagProvider(event.getGenerator(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(new VItemTagProvider(event.getGenerator(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(new VBlockTagProvider(event.getGenerator(), event.getExistingFileHelper()));
        event.getGenerator().addProvider(new ItemModelProvider(event.getGenerator()));

        event.getGenerator().addProvider(new VSoundProvider(event.getGenerator()));

        event.getGenerator().addProvider(new LanguageProvider(event.getGenerator()));

    }

}
