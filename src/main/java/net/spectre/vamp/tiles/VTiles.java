package net.spectre.vamp.tiles;

import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.blocks.TileBlock;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.misc.ObjectWrapper;

import java.util.function.Supplier;

public class VTiles {

	public static final DeferredRegister<TileEntityType<?>> TILES = DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, Monsterous.MODID);
	
	public static final RegistryObject<TileEntityType<CoffinTile>> COFFIN = TILES.register("coffin", () -> create(CoffinTile::new, VBlocks.COFFIN.get()));


    public static <T extends TileEntity> TileEntityType<T> create(Supplier<T> sup, Block... blocks){
		TileEntityType<T> type = TileEntityType.Builder.create(sup, blocks).build(null);
		
		for(Block block : blocks) {
			if(block instanceof TileBlock) {
				((TileBlock)block).setTile(type);
			}
		}
		
		return type;
		
	}
}
