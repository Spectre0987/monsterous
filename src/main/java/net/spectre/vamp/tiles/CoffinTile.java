package net.spectre.vamp.tiles;

import net.minecraft.block.BedBlock;
import net.minecraft.state.properties.BedPart;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.spectre.vamp.blocks.CoffinBlock;

public class CoffinTile extends TileEntity{

	public static final AxisAlignedBB RENDER = new AxisAlignedBB(-1, -1, -1, 2, 2, 2);
	
	public CoffinTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public CoffinTile() {
		this(VTiles.COFFIN.get());
	}
	
	public boolean isOpen() {
		if(this.getBlockState().hasProperty(CoffinBlock.OPEN))
			return this.getBlockState().get(CoffinBlock.OPEN);
		return false;
	}

	public boolean isHead(){
		if(this.getBlockState().hasProperty(BedBlock.PART))
			return this.getBlockState().get(BedBlock.PART) == BedPart.HEAD;
		return false;
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER.offset(getPos());
	}
	
	

}
