package net.spectre.vamp.events;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.event.InputUpdateEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.Mod;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.blocks.VBlocks;
import net.spectre.vamp.cap.Capabilities;
import net.spectre.vamp.client.ModelRegistry;
import net.spectre.vamp.client.helpers.GuiHelper;
import net.spectre.vamp.client.layers.IRemoveParts;
import net.spectre.vamp.client.models.NetModel;
import net.spectre.vamp.client.models.VampireBatModel;
import net.spectre.vamp.client.overlay.BaseOverlay;
import net.spectre.vamp.helpers.ClientHelper;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.items.VItems;
import net.spectre.vamp.misc.PowerCooldown;
import net.spectre.vamp.misc.TransformationType;
import net.spectre.vamp.monsters.FlyingHandler;
import net.spectre.vamp.monsters.IFlyingMonster;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.network.Network;
import net.spectre.vamp.network.packets.RequestMonsterUpdateMessage;
import net.spectre.vamp.network.packets.FlightMessage;
import net.spectre.vamp.network.packets.FlightStateMessage;
import net.spectre.vamp.network.packets.PowerMessage;
import net.spectre.vamp.network.packets.PowerMessage.PowerAction;
import net.spectre.vamp.powers.Power;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

@Mod.EventBusSubscriber(modid = Monsterous.MODID, value = Dist.CLIENT)
public class ClientEvents {

	private static final VampireBatModel BAT_MODEL = new VampireBatModel();
	private static final ResourceLocation BAT_TEXTURE = new ResourceLocation("textures/entity/bat.png");

	@SubscribeEvent
	public static void renderOverlay(RenderGameOverlayEvent.Pre event) {
		Minecraft.getInstance().player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {

			if(ModelRegistry.MONSTER_OVERLAY.containsKey(cap.getMonsterType())){
				BaseOverlay overlay = ModelRegistry.MONSTER_OVERLAY.get(cap.getMonsterType());

				if(overlay.shouldCancel()){
					if(cap.getMonsterType() == overlay.getMonster()) {
						//If correct element
						if(overlay.getElementType() == event.getType()) {
							overlay.render(cap.getTypeObject());
							event.setCanceled(true);

						}
					}
				}

			}

			//draw powers
			if(event.getType() == ElementType.ALL && cap.getTypeObject() != null && cap.getTypeObject().getSelectedPower() != null) {
				Power power = cap.getTypeObject().getSelectedPower();
				
				Minecraft.getInstance().getTextureManager().bindTexture(power.getIconPath());

				GlStateManager.enableBlend();

				BufferBuilder bb = Tessellator.getInstance().getBuffer();
				MainWindow window = event.getWindow();
				int x = window.getScaledWidth() / 2, y = window.getScaledHeight() - 50;
				int size = 16;
				
				bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
				
				bb.pos(x - (size / 2), y, 0).tex(0, 0).endVertex();
				bb.pos(x - (size / 2), y + (size), 0).tex(0, 1).endVertex();
				bb.pos(x + (size / 2), y + size, 0).tex(1, 1).endVertex();
				bb.pos(x + (size / 2), y, 0).tex(1, 0).endVertex();
				
				Tessellator.getInstance().draw();

				GlStateManager.disableBlend();

				//Cooldown
				PowerCooldown cooldown = cap.getTypeObject().getOrCreateCooldown(power);

				GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
				bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
				GuiHelper.renderCooldown(bb, x, y, cooldown.getCooldownPercent());
				Tessellator.getInstance().draw();

			}
			
		});
	}

	@SubscribeEvent
	public static void renderOverlayPost(RenderGameOverlayEvent.Post event){
		Minecraft.getInstance().player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {

			if (ModelRegistry.MONSTER_OVERLAY.containsKey(cap.getMonsterType())) {
				BaseOverlay overlay = ModelRegistry.MONSTER_OVERLAY.get(cap.getMonsterType());

				if (!overlay.shouldCancel()) {
					if (cap.getMonsterType() == overlay.getMonster()) {
						//If correct element
						if (overlay.getElementType() == event.getType()) {
							overlay.render(cap.getTypeObject());
						}
					}
				}


			}
		});
	}
	
	@SubscribeEvent
	public static void tryToMove(InputUpdateEvent event){
		Minecraft.getInstance().player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
			
			PlayerEntity player = Minecraft.getInstance().player;
			
			//Vampires
			if(cap.getMonsterType() == MonsterRegistry.VAMPIRE.get()) {
				//Crosses
				for(PlayerEntity p : Minecraft.getInstance().world.getEntitiesWithinAABB(PlayerEntity.class,Minecraft.getInstance().player.getBoundingBox().grow(16))) {
					if(Helper.inEitherHand(stack -> stack.getItem() == VItems.CROSS.get(), p)) {
						ClientHelper.stopAllMovement(event.getMovementInput());
						break;
					}
				}
			}
			//Demons
			else if(cap.getMonsterType() == MonsterRegistry.DEMON.get()) {
				if(player.world.getBlockState(player.getPosition().down()).getBlock() == VBlocks.DEMON_TRAP.get() || player.world.getBlockState(player.getPosition()).getBlock() == VBlocks.DEMON_TRAP.get()) {
					ClientHelper.stopAllMovement(event.getMovementInput());
				}
			}
		});
	}
	
	@SubscribeEvent
	public static void handleKeyBinds(InputEvent.KeyInputEvent event) {

		if(Minecraft.getInstance().player == null || Minecraft.getInstance().world == null)
			return;

		PlayerEntity player = Minecraft.getInstance().player;

		if(ModelRegistry.SCROLL_ACT.isPressed()) {

			player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
				if(cap.getTypeObject() != null){
					//Get current power
					Power power = cap.getTypeObject().getSelectedPower();
					//If there power is a power, and it can be used
					if(power != null && cap.getTypeObject().canUsePower(power)){
						//Run on server
						Network.sendToServer(new PowerMessage(PowerAction.ACT));
						//Run on client
						if(power.run(player, cap.getTypeObject()))
							cap.getTypeObject().getOrCreateCooldown(power).startCooldown();
					}
				}
			});

		}
		
		if(ModelRegistry.SCROLL_NEXT.isPressed()) {
			Network.sendToServer(new PowerMessage(PowerAction.NEXT));
		}
		
		if(ModelRegistry.SCROLL_PREV.isPressed()) {
			Network.sendToServer(new PowerMessage(PowerAction.PREV));
		}

		//If capable of flight
		player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
			if(cap.getTypeObject() instanceof IFlyingMonster){
				IFlyingMonster fly = (IFlyingMonster) cap.getTypeObject();

				//If hit jump
				if(Minecraft.getInstance().gameSettings.keyBindJump.isPressed()){
					//If it's flying, boost it
					if(fly.getFlightHandler().isFlying()){
						Network.sendToServer(new FlightMessage());
						fly.getFlightHandler().flap();
					}
					//If it's not flying, and in air when jump was pressed
					else if(player.isAirBorne && event.getAction() == GLFW.GLFW_PRESS){
						if(!fly.getFlightHandler().isFlying() && fly.getFlightHandler().jumped()){
							fly.getFlightHandler().setInFlight(true);
							Network.sendToServer(new FlightStateMessage(player.getEntityId(), true));
						}
					}
				}

				//If you hit shift
				if(Minecraft.getInstance().gameSettings.keyBindSneak.isPressed() && fly.getFlightHandler().isFlying()){
					fly.getFlightHandler().setInFlight(false);
					Network.sendToServer(new FlightStateMessage(player.getEntityId(), false));
				}
			}
		});
	}

	@SubscribeEvent
	public static void renderPlayerPre(RenderPlayerEvent.Pre event){
		for(LayerRenderer<?, ?> layer : event.getRenderer().layerRenderers){
			if(layer instanceof IRemoveParts<?>){
				IRemoveParts<PlayerModel<AbstractClientPlayerEntity>> parts = (IRemoveParts<PlayerModel<AbstractClientPlayerEntity>>)layer;
				if(parts.shouldRender(event.getPlayer()))
					parts.toggle(event.getRenderer().getEntityModel(), false);
			}
		}

		event.getPlayer().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
			if(cap.getTypeObject() != null){
				//Bat transformation
				if(cap.getTypeObject().getTransformationType() == TransformationType.BAT){
					event.setCanceled(true);

					PlayerEntity player = Minecraft.getInstance().player;
					float scale = 0.4F;

					event.getMatrixStack().rotate(Vector3f.XP.rotationDegrees(180));
					event.getMatrixStack().translate(0, -0.6, 0);
					event.getMatrixStack().scale(scale, scale, scale);

					//Living animations
					event.getMatrixStack().rotate(Vector3f.YP.rotationDegrees(player.rotationYaw));
					BAT_MODEL.animate(player);
					BAT_MODEL.render(event.getMatrixStack(), event.getBuffers().getBuffer(RenderType.getEntityCutoutNoCull(BAT_TEXTURE)), event.getLight(), OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);

				}
				//Net
				if(cap.getTypeObject() instanceof IFlyingMonster){
					FlyingHandler fly = ((IFlyingMonster)cap.getTypeObject()).getFlightHandler();

					if(fly.isCaptured()){

						event.getMatrixStack().push();
						event.getMatrixStack().translate(0, 1.5, 0);
						event.getMatrixStack().rotate(Vector3f.ZP.rotationDegrees(180));

						NetModel.INSTANCE.render(event.getMatrixStack(),
								event.getBuffers().getBuffer(RenderType.getEntityCutoutNoCull(NetModel.TEXTURE)),
								event.getLight(),
								OverlayTexture.NO_OVERLAY,
								1.0F, 1.0F, 1.0F, 1.0F);
						event.getMatrixStack().pop();
					}

				}
			}
		});

	}

	@SubscribeEvent
	public static void worldTick(TickEvent.WorldTickEvent event){
		if(event.side == LogicalSide.CLIENT){
			for(PlayerEntity player : event.world.getPlayers()){
				if(player != null && player.isAlive()){
					player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
						Network.sendToServer(new RequestMonsterUpdateMessage(player));
					});
				}
			}
		}
	}

}
