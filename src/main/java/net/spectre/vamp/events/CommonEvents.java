package net.spectre.vamp.events;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.ModifiableAttributeInstance;
import net.minecraft.entity.passive.DolphinEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.LootPool;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.EntityTravelToDimensionEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingSetAttackTargetEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.SleepingTimeCheckEvent;
import net.minecraftforge.event.world.SleepFinishedTimeEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.cap.*;
import net.spectre.vamp.cap.items.ColtCap;
import net.spectre.vamp.cap.items.IColt;
import net.spectre.vamp.constants.AttributeConstants;
import net.spectre.vamp.entities.ai.VFollowMasterGoal;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.items.VItems;
import net.spectre.vamp.misc.LootTableEdit;
import net.spectre.vamp.misc.TransformationType;
import net.spectre.vamp.monsters.*;
import net.spectre.vamp.tags.VItemTags;

@Mod.EventBusSubscriber(modid = Monsterous.MODID)
public class CommonEvents {

	public static final ResourceLocation MINION_CAP = Helper.createRL("minion");
	public static final ResourceLocation SMITE_CAP = Helper.createRL("smite");
	public static final ResourceLocation MAIN_CAP = Helper.createRL("main");

	@SubscribeEvent
	public static void attachEntityCap(AttachCapabilitiesEvent<Entity> event) {
		if(event.getObject() instanceof PlayerEntity)
			event.addCapability(MAIN_CAP, new IMonster.Provider(new MonsterCapability((PlayerEntity)event.getObject())));

		if(event.getObject() instanceof LivingEntity)
			event.addCapability(SMITE_CAP, new ISmiteable.Provider(new SmiteCap(event.getObject(), SmiteCap.getColorFromEntity(event.getObject()))));

		if(event.getObject() instanceof DolphinEntity)
			event.addCapability(MINION_CAP, new BasicProvider<IMinion, CompoundNBT>(Capabilities.MINION, new MinionCapability((DolphinEntity)event.getObject())));
	}

	@SubscribeEvent
	public static void attachItemCap(AttachCapabilitiesEvent<ItemStack> event) {
		if(event.getObject().getItem() == VItems.COLT.get())
			event.addCapability(new ResourceLocation(Monsterous.MODID, "colt"), new IColt.Provider(new ColtCap(event.getObject())));
	}
	
	@SubscribeEvent
	public static void hurtFirstPass(LivingAttackEvent event) {
		if (event.getEntityLiving() instanceof PlayerEntity)
			//If a player got hurt
			((PlayerEntity) event.getEntityLiving()).getCapability(Capabilities.MONSTER).ifPresent(cap -> {

				if(cap.getTypeObject() instanceof IDontTakeNormalDamage){
					IDontTakeNormalDamage damage = (IDontTakeNormalDamage) cap.getTypeObject();
					if(damage.blockCompletely(event.getSource()))
						event.setCanceled(true);
				}

			});
		//If a player hurts
		if(event.getSource().getImmediateSource() instanceof PlayerEntity) {
			//If an empty hand
			PlayerEntity player = (PlayerEntity) event.getSource().getImmediateSource();
			event.getSource().getImmediateSource().getCapability(Capabilities.MONSTER).ifPresent(monst -> {
				if(monst.getTypeObject() != null)
					monst.getTypeObject().onAttackLiving(event.getEntityLiving());
			});
		}
	}

	@SubscribeEvent
	public static void hurtSecondPass(LivingDamageEvent event){
		//If the entity that got hurt is a monster
		event.getEntityLiving().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
			if(cap.getTypeObject() instanceof IDontTakeNormalDamage){
				IDontTakeNormalDamage damage = (IDontTakeNormalDamage)cap.getTypeObject();
				event.setAmount(damage.changeDamage(event.getSource(), event.getAmount()));
			}
		});
	}
	
	@SubscribeEvent
	public static void onSleepCheck(SleepingTimeCheckEvent event) {
		event.getPlayer().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
			if(cap.getMonsterType() == MonsterRegistry.VAMPIRE.get()) {
				if(Helper.isDay(event.getPlayer().world))
					event.setResult(Result.ALLOW);
			}
		});
	}
	
	@SubscribeEvent
	public static void tick(LivingUpdateEvent event) {
		if(event.getEntityLiving() instanceof PlayerEntity)
			((PlayerEntity)event.getEntityLiving()).getCapability(Capabilities.MONSTER).ifPresent(cap -> cap.tick());
		event.getEntityLiving().getCapability(Capabilities.SMITE).ifPresent(cap -> cap.tick());
	}
	
	@SubscribeEvent
	public static void clone(PlayerEvent.Clone event) {
		event.getOriginal().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
			event.getPlayer().getCapability(Capabilities.MONSTER).ifPresent(newCap -> {
				newCap.deserializeNBT(cap.serializeNBT());
				if(event.isWasDeath())
					newCap.respawn();
				MonsterCapability.update(event.getPlayer());
			});
		});

		//Remove demonic boons
		for(AttributeModifier mod : AttributeConstants.DEMON_BOONS){
			for(ModifiableAttributeInstance instance : event.getPlayer().getAttributeManager().getInstances()){
				if(instance.hasModifier(mod))
					instance.removeModifier(mod);
			}
		}

		//Remove this contract
		if(!event.getPlayer().world.isRemote && event.isWasDeath()){
			for(PlayerEntity player : event.getPlayer().world.getPlayers()){
				player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
					if(cap.getMonsterType() == MonsterRegistry.DEMON.get()){
						cap.getMonsterObjectOptional(DemonMonster.class).ifPresent(demon -> {
							demon.getContractPlayers().remove(event.getPlayer().getUniqueID());
						});
					}
				});
			}
		}

	}
	
	@SubscribeEvent
	public static void onDimChanged(EntityTravelToDimensionEvent event) {
		if(!event.getEntity().world.isRemote) {
			if(event.getEntity() instanceof PlayerEntity) {
				event.getEntity().world.getServer().enqueue(new TickDelayedTask(1, () -> {
					event.getEntity().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
						MonsterCapability.update((PlayerEntity)event.getEntity());
					});
				}));
			}
		}
	}
	
	@SubscribeEvent
	public static void changeLootTable(LootTableLoadEvent event) {

		LootPool pool = LootTableEdit.getLootPool(event.getName());
		if(pool != null)
			event.getTable().addPool(pool);
	}
	
	@SubscribeEvent
	public static void onSetTarget(LivingSetAttackTargetEvent event) {
		
		if(event.getTarget() instanceof PlayerEntity) {
			event.getTarget().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
				if(cap.getTypeObject() instanceof ICantBeAttackedByAll) {

					ICantBeAttackedByAll attack = (ICantBeAttackedByAll)cap.getTypeObject();

					if(event.getEntity() instanceof MobEntity){
						if(!attack.canAttackMe((MobEntity)event.getEntity()))
							((MobEntity)event.getEntity()).setAttackTarget(null);

					}
						
				}
			});
		}
		
	}

	@SubscribeEvent
	public static void getDigSpeed(PlayerEvent.BreakSpeed event){
		event.getPlayer().getCapability(Capabilities.MONSTER).ifPresent(cap -> {

			if(cap.getMonsterType() == MonsterRegistry.MERMAID.get()){
				cap.getMonsterObjectOptional(MermaidMonster.class).ifPresent(data -> {
					if(data.isWet()){
						event.setNewSpeed(event.getOriginalSpeed() * (event.getPlayer().isOnGround() ? 5.0F : 25.0F));
					}
				});
			}

		});
	}

	@SubscribeEvent
	public static void onFoodEaten(LivingEntityUseItemEvent.Start event){

		if(event.getItem().isFood()){
			event.getEntityLiving().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
				if(cap.getTypeObject() instanceof ImFoodRestricted){
					if(!((ImFoodRestricted)cap.getTypeObject()).onFoodEaten(event.getItem())) {
						event.setCanceled(true);
					}
				}
			});
		}

	}

	@SubscribeEvent
	public static void setSize(EntityEvent.Size size){
		size.getEntity().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
			if(cap.getTypeObject() != null){
				if(cap.getTypeObject().getTransformationType() != TransformationType.NONE){
					size.setNewSize(cap.getTypeObject().getTransformationType().getSize(), true);
					size.setNewEyeHeight(cap.getTypeObject().getTransformationType().getSize().height * 0.85F);
				}
			}
		});
	}

	@SubscribeEvent
	public static void onRightClickEntity(PlayerInteractEvent.EntityInteractSpecific event){

		event.getTarget().getCapability(Capabilities.MINION).ifPresent(minion -> {

			minion.tryTame(event.getPlayer(), event.getItemStack());
		});
	}

	@SubscribeEvent
	public static void onEaten(LivingEntityUseItemEvent.Finish event){
		if(event.getItem().isFood() && VItemTags.HOT_DRINKS.contains(event.getItem().getItem())){
			event.getEntityLiving().getCapability(Capabilities.MONSTER).ifPresent(cap -> {
				if(cap.getTypeObject() instanceof DemonMonster){
					cap.getMonsterObjectOptional(DemonMonster.class).ifPresent(demon -> {
						demon.addCorruption(20.0F);
					});
				}
			});
		}
	}

	@SubscribeEvent
	public static void addCustomAI(EntityJoinWorldEvent event){
		if(event.getEntity() instanceof CreatureEntity){
			CreatureEntity mob = (CreatureEntity) event.getEntity();

			event.getEntity().getCapability(Capabilities.MINION).ifPresent(minion -> {
				mob.goalSelector.addGoal(1, new VFollowMasterGoal(mob));
				System.out.println("Added to " + mob.getDisplayName().getString());
			});
		}
	}

	@SubscribeEvent
	public static void tickWorld(TickEvent.WorldTickEvent event){

	}

	@SubscribeEvent
	public static void onSleepFinished(SleepFinishedTimeEvent event){
		long oldTime = event.getWorld().getWorldInfo().getDayTime();

		System.out.println("Time is " + oldTime);

		//If day, set to night
		if(Helper.isDay(event.getWorld().getWorldInfo().getDayTime())){
			event.setTimeAddition(12575L);
		}

	}
}
