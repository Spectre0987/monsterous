package net.spectre.vamp.sounds;


import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.spectre.vamp.Monsterous;
import net.spectre.vamp.helpers.Helper;

import java.util.function.Supplier;

public class Sounds {

    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, Monsterous.MODID);

    public static final RegistryObject<SoundEvent> REVOLVER_COCK = SOUNDS.register("cock", register("cock"));
    public static final RegistryObject<SoundEvent> REVOLVER_LOAD = SOUNDS.register("load", register("load"));
    public static final RegistryObject<SoundEvent> REVOLVER_SHOOT = SOUNDS.register("shoot", register("shoot"));


    public static Supplier<SoundEvent> register(String name){
        return () -> new SoundEvent(Helper.createRL(name));
    }

}
