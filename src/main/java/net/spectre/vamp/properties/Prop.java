package net.spectre.vamp.properties;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.spectre.vamp.itemgroups.VItemGroup;

import java.util.function.Supplier;

public class Prop {

	public static class Blocks{
		public static final Block.Properties CHALK = Block.Properties.create(Material.CLAY).hardnessAndResistance(1).sound(SoundType.STONE).harvestLevel(0);
	}
	
	public static class Items{
		public static final Supplier<Item.Properties> BASE = () -> new Item.Properties().maxStackSize(64).group(VItemGroup.MAIN);
		public static final Supplier<Item.Properties> ONE = () -> new Item.Properties().maxStackSize(1).group(VItemGroup.MAIN);
	}
}
