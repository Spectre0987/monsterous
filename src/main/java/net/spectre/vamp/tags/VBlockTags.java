package net.spectre.vamp.tags;

import net.minecraft.block.Block;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ITag;
import net.spectre.vamp.helpers.Helper;

public class VBlockTags {

    public static final ITag.INamedTag<Block> HEAT_SOURCE = BlockTags.makeWrapperTag(Helper.createRL("heat").toString());

}
