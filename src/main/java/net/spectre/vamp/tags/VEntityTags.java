package net.spectre.vamp.tags;

import net.minecraft.entity.EntityType;
import net.minecraft.tags.EntityTypeTags;
import net.minecraft.tags.ITag;
import net.minecraftforge.registries.ForgeRegistries;
import net.spectre.vamp.Monsterous;

public class VEntityTags {

    public static final ITag.INamedTag<EntityType<?>> MERMAID_EAT = EntityTypeTags.getTagById(Monsterous.MODID + ":mermaid_eat");
    public static final ITag.INamedTag<EntityType<?>> HELL_FRIEND = EntityTypeTags.getTagById(Monsterous.MODID + ":hell_friend");

}
