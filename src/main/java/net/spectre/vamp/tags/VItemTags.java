package net.spectre.vamp.tags;

import net.minecraft.item.Item;
import net.minecraft.tags.ItemTags;
import net.minecraftforge.common.Tags;
import net.spectre.vamp.helpers.Helper;

public class VItemTags {

    public static final Tags.IOptionalNamedTag<Item> HOLY_ITEMS = ItemTags.createOptional(Helper.createRL("holy"));
    public static final Tags.IOptionalNamedTag<Item> COLT_AMMO = ItemTags.createOptional(Helper.createRL("colt_ammo"));
    public static final Tags.IOptionalNamedTag<Item> HOT_DRINKS = ItemTags.createOptional(Helper.createRL("hot_drinks"));
}
