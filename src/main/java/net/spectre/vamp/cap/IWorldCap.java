package net.spectre.vamp.cap;

import net.spectre.vamp.misc.SoulChest;

import java.util.List;
import java.util.UUID;

public interface IWorldCap {

    void addSoulChest(SoulChest chest);
    List<SoulChest> getSoulChestByOwner(UUID owner);
    List<SoulChest> getSoulChestByDemon(UUID demon);

}
