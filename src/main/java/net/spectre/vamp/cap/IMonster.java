package net.spectre.vamp.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.spectre.vamp.monsters.IMonsterType;
import net.spectre.vamp.monsters.MonsterType;

import java.util.Optional;

public interface IMonster extends INBTSerializable<CompoundNBT>{

	void tick();
	void respawn();
	MonsterType getMonsterType();
	void setMonsterType(MonsterType type);
	IMonsterType getTypeObject();
	<F extends IMonsterType> Optional<F> getMonsterObjectOptional(Class<F> objectClass);
	<F extends IMonsterType> F getMonsterObject(Class<F> monsterClass);

	public static class Storage implements Capability.IStorage<IMonster>{

		@Override
		public INBT writeNBT(Capability<IMonster> capability, IMonster instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IMonster> capability, IMonster instance, Direction side, INBT nbt) {
			instance.deserializeNBT((CompoundNBT)nbt);
		}
	
	}
	
	public static class Provider implements ICapabilitySerializable<CompoundNBT>{

		private IMonster monster;
		
		public Provider(IMonster mon) {
			this.monster = mon;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == Capabilities.MONSTER ? LazyOptional.of(() -> (T)this.monster) : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return monster.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			this.monster.deserializeNBT(nbt);
		}
		
	}
}
