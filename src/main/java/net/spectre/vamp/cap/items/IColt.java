package net.spectre.vamp.cap.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;
import net.spectre.vamp.cap.Capabilities;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface IColt extends INBTSerializable<CompoundNBT> {

    ItemStackHandler getBullets();
    boolean addBullets(ItemStack bullet);
    int getBulletCount();
    ItemStack getCurrentBullet();

    void fire(PlayerEntity player);
    void reload(PlayerEntity player);

    class Storage implements Capability.IStorage<IColt> {

        @Nullable
        @Override
        public INBT writeNBT(Capability<IColt> capability, IColt instance, Direction side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<IColt> capability, IColt instance, Direction side, INBT nbt) {
            if(nbt instanceof CompoundNBT)
                instance.deserializeNBT((CompoundNBT) nbt);
        }
    }

    class Provider implements ICapabilitySerializable<CompoundNBT> {

        IColt colt;
        LazyOptional<IColt> coltProvider;

        public Provider(IColt colt){
            this.colt = colt;
            this.coltProvider = LazyOptional.of(() -> this.colt);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return cap == Capabilities.COLT ? this.coltProvider.cast() : LazyOptional.empty();
        }

        @Override
        public CompoundNBT serializeNBT() {
            return colt.serializeNBT();
        }

        @Override
        public void deserializeNBT(CompoundNBT nbt) {
            colt.deserializeNBT(nbt);
        }
    }

}
