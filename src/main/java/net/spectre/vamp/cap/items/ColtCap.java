package net.spectre.vamp.cap.items;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.items.ItemStackHandler;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.helpers.Helper;
import net.spectre.vamp.items.ColtItem;
import net.spectre.vamp.sounds.Sounds;
import net.spectre.vamp.tags.VItemTags;

public class ColtCap implements IColt{

    private ItemStack stack;
    private ItemStackHandler inv = new ItemStackHandler(6);

    public ColtCap(ItemStack stack){
        this.stack = stack;
    }

    @Override
    public ItemStackHandler getBullets() {
        return this.inv;
    }

    @Override
    public boolean addBullets(ItemStack bullet) {

        if(!VItemTags.COLT_AMMO.contains(bullet.getItem()))
            return false;

        for(int i = 0; i < inv.getSlots(); ++i){
            if(inv.getStackInSlot(i).isEmpty()){
                inv.insertItem(i, bullet, false);
                return true;
            }
        }

        return false;
    }

    @Override
    public int getBulletCount() {
        int amount = 0;

        for(int i = 0; i < this.inv.getSlots(); ++i){
            if(!this.inv.getStackInSlot(i).isEmpty())
                ++amount;
        }

        return amount;
    }

    @Override
    public ItemStack getCurrentBullet() {
        for(int i = 0; i < this.inv.getSlots(); ++i){
            ItemStack stack = inv.getStackInSlot(i);
            if(!stack.isEmpty()) {
                ItemStack ammo = stack.split(1);
                inv.setStackInSlot(i, stack);
                return ammo;
            }
        }
        return ItemStack.EMPTY;
    }

    @Override
    public void fire(PlayerEntity player) {

        ItemStack bullet = this.getCurrentBullet();

        if(!bullet.isEmpty()){
            Entity e = Helper.rayTraceEntity(player, 128);

            if(e instanceof LivingEntity){

                LivingEntity live = (LivingEntity) e;

                live.attackEntityFrom(DamageSources.KILL_ALL_COLT, 20.0F);

            }
            Helper.playSoundAtPlayer(player, Sounds.REVOLVER_SHOOT.get(), SoundCategory.PLAYERS, 10.0F);
            bullet.shrink(1);
        }
    }

    @Override
    public void reload(PlayerEntity player) {
       if(this.getBulletCount() < 6){
           ItemStack bullet = ColtItem.findAmmo(player, true);

           if(!bullet.isEmpty()){
               if(this.addBullets(bullet.split(1))){
                   Helper.playSoundAtPlayer(player, Sounds.REVOLVER_LOAD.get(), SoundCategory.PLAYERS, 1.0F);
                   return;
               }
           }
       }
    }

    @Override
    public CompoundNBT serializeNBT() {
        return this.inv.serializeNBT();
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        inv.deserializeNBT(nbt);
    }
}
