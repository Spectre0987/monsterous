package net.spectre.vamp.cap;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.spectre.vamp.monsters.*;
import net.spectre.vamp.network.Network;
import net.spectre.vamp.network.packets.CapUpdateMessage;
import net.spectre.vamp.powers.Power;

import java.util.Optional;

public class MonsterCapability implements IMonster{

	private PlayerEntity player;
	private MonsterType type = MonsterRegistry.NONE.get();
	private IMonsterType object;
	
	private boolean firstTick = true;
	
	public MonsterCapability(PlayerEntity player) {
		this.player = player;
	}

	@Override
	public void tick() {
		
		//Handle sending after a read
		if(this.firstTick) {
			this.firstTick = false;
			this.updateClient();
		}
		
		if(this.object != null)
			this.object.update();
	}
	
	@Override
	public void respawn() {
		if(this.object != null)
			this.object.respawn();
		updateClient();
	}
	
	@Override
	public MonsterType getMonsterType() {
		return this.type;
	}
	
	@Override
	public void setMonsterType(MonsterType type) {

		//If already a monster
		if(this.type != null && this.type != MonsterRegistry.NONE.get()){
			this.getTypeObject().onRemove();
		}

		this.type = type;
		this.object = type.create(player);
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putString("type", this.type.getRegistryName().toString());
		
		if(this.object != null)
			tag.put("object", this.object.serializeNBT());
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.setMonsterType(MonsterRegistry.getFromString(nbt.getString("type")));
		if(this.object != null && nbt.contains("object"))
			this.object.deserializeNBT(nbt.getCompound("object"));
	}
	
	private void updateClient() {
		if(player instanceof ServerPlayerEntity)
			Network.sendToAll(new CapUpdateMessage(player.getUniqueID(), this.serializeNBT()));
	}
	
	@Override
	public IMonsterType getTypeObject() {
		return this.object;
	}

	@Override
	public <F extends IMonsterType> Optional<F> getMonsterObjectOptional(Class<F> objectClass) {

		if(this.object != null && objectClass.isInstance(this.object))
			return Optional.of((F)this.object);

		return Optional.empty();
	}

	@Override
	public <F extends IMonsterType> F getMonsterObject(Class<F> monsterClass) {
		return (F)this.object;
	}

	public static void update(PlayerEntity player) {
		if(player instanceof ServerPlayerEntity) {
			player.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
				Network.sendToAll(new CapUpdateMessage(player.getUniqueID(), cap.serializeNBT()));
			});
		}
	}
}
