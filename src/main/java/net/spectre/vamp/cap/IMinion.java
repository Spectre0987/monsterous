package net.spectre.vamp.cap;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.INBTSerializable;
import net.spectre.vamp.monsters.MonsterType;

import javax.annotation.Nullable;
import java.util.UUID;

public interface IMinion extends INBTSerializable<CompoundNBT> {

    UUID getOwner();
    void setOwner(UUID id);
    PlayerEntity getOwnerEntity();

    void setSitting(boolean sit);
    boolean isSitting();

    boolean canBeTamedBy(MonsterType type);

    boolean tryTame(PlayerEntity owner, ItemStack stack);

    class Storage implements Capability.IStorage<IMinion> {

        @Nullable
        @Override
        public INBT writeNBT(Capability<IMinion> capability, IMinion instance, Direction side) {
            return instance.serializeNBT();
        }

        @Override
        public void readNBT(Capability<IMinion> capability, IMinion instance, Direction side, INBT nbt) {
            if(nbt instanceof CompoundNBT)
                instance.deserializeNBT((CompoundNBT) nbt);
        }
    }

    class Provider{



    }


}
