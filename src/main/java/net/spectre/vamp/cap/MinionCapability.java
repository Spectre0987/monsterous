package net.spectre.vamp.cap;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.spectre.vamp.helpers.NBTHelper;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.monsters.MonsterType;

import javax.annotation.Nullable;
import java.util.UUID;

public class MinionCapability implements IMinion{

    protected LivingEntity minion;
    private UUID owner;
    private PlayerEntity ownerEntity;
    private boolean isSitting = false;

    public MinionCapability(LivingEntity minion){
        this.minion = minion;
    }

    @Override
    public UUID getOwner() {
        return owner;
    }

    @Override
    public void setOwner(UUID id) {
        this.owner = id;
    }

    @Nullable
    @Override
    public PlayerEntity getOwnerEntity() {

        //If this dosn't have an owner at all
        if(this.owner == null)
            return null;

        //If it is tracking an owner and that owner is alive, return it
        if(this.ownerEntity != null && this.ownerEntity.isAlive())
            return this.ownerEntity;

        //if the owner is null or not alive, find a new one
        if(minion.getEntityWorld() != null) {
            this.ownerEntity = minion.getEntityWorld().getPlayerByUuid(this.owner);
        }

        //Return whatever we've got
        return this.ownerEntity;
    }

    @Override
    public void setSitting(boolean sit) {
        this.isSitting = sit;
    }

    @Override
    public boolean isSitting() {
        return this.isSitting;
    }

    @Override
    public boolean canBeTamedBy(MonsterType type) {
        return type == MonsterRegistry.MERMAID.get();
    }

    @Override
    public boolean tryTame(PlayerEntity owner, ItemStack stack) {

        if(this.owner == null){
            boolean tame = owner.getRNG().nextDouble() <= 0.33;
            if(tame){
                this.owner = owner.getUniqueID();
                this.ownerEntity = owner;
                System.out.println("Owns");
                return true;
            }
        }

        return false;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        NBTHelper.writeUUID("owner", this.owner, tag);
        tag.putBoolean("sit", this.isSitting);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        this.owner = NBTHelper.readUUID("owner", tag);
        this.isSitting = tag.getBoolean("sit");
    }
}
