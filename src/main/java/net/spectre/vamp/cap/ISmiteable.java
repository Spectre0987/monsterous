package net.spectre.vamp.cap;

import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.spectre.vamp.misc.IPacketData;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.*;

public interface ISmiteable {

    Color getSmiteColor();
    void smite();
    void tick();
    boolean isBeingSmote();

    void setSmiteTicks(int ticks);
    void setSmiteColor(int color);
    void setSmiteColor(Color color);

    class Storage implements Capability.IStorage<ISmiteable> {

        @Nullable
        @Override
        public INBT writeNBT(Capability<ISmiteable> capability, ISmiteable instance, Direction side) {
            return null;
        }

        @Override
        public void readNBT(Capability<ISmiteable> capability, ISmiteable instance, Direction side, INBT nbt) {

        }
    }

    class Provider implements ICapabilityProvider {

        private LazyOptional<ISmiteable> smite;

        public Provider(ISmiteable smite){
            this.smite = LazyOptional.of(() -> smite);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return cap == Capabilities.SMITE ? smite.cast() : LazyOptional.empty();
        }
    }
}
