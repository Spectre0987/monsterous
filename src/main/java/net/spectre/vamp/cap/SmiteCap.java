package net.spectre.vamp.cap;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.spectre.vamp.damagesources.DamageSources;
import net.spectre.vamp.misc.ObjectWrapper;
import net.spectre.vamp.monsters.MonsterRegistry;
import net.spectre.vamp.network.Network;
import net.spectre.vamp.network.packets.SmiteMessage;

import java.awt.*;

public class SmiteCap implements ISmiteable{

    private Entity entity;
    private Color smiteColor;
    private int smiteTicks;

    public SmiteCap(Entity e, int smiteColor){
        this.entity = e;
        this.setSmiteColor(smiteColor);
    }

    @Override
    public Color getSmiteColor() {
        return this.smiteColor;
    }

    @Override
    public void smite() {

        if(this.isBeingSmote())
            return;

        //2 second animation
        this.smiteTicks = 40;

        //if on server, send packet to all tracking this
        if(!this.entity.world.isRemote){
            //Packet
            Network.sendToAllTrackingEntity(this.entity, new SmiteMessage(this.entity.getEntityId(), this.smiteTicks, this.smiteColor.getRGB()));
        }
    }

    @Override
    public void tick() {
        if(this.smiteTicks > 0) {
            --this.smiteTicks;
            if(smiteTicks == 0){
                //If smite animation is done
                this.entity.attackEntityFrom(DamageSources.SMITE, Integer.MAX_VALUE - 1);
            }

            //Make this look up
            //if(this.entity.rotationPitch < 90)
            this.entity.rotationPitch = (1.0F - (this.smiteTicks / 40.0F)) * -90.0F;

        }
    }

    @Override
    public boolean isBeingSmote() {
        return this.smiteTicks > 0;
    }

    @Override
    public void setSmiteTicks(int ticks) {
        this.smiteTicks = ticks;
    }

    @Override
    public void setSmiteColor(int color) {
        this.setSmiteColor(new Color(color));
    }

    @Override
    public void setSmiteColor(Color color) {
        this.smiteColor = color;
    }



    public static int getColorFromEntity(Entity object) {

        ObjectWrapper<Integer> color = new ObjectWrapper<>(0x62e3ea);

        object.getCapability(Capabilities.MONSTER).ifPresent(cap -> {
            if(cap.getMonsterType() == MonsterRegistry.DEMON.get() || cap.getMonsterType() == MonsterRegistry.VAMPIRE.get())
                color.set(0x9e1100);
        });

        return color.get();
    }
}
