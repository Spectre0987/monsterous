package net.spectre.vamp.cap;

import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class BasicProvider<T extends INBTSerializable<C>, C extends INBT> implements ICapabilitySerializable<C> {


    private Capability<T> capability;
    private LazyOptional<T> provider;
    private T instance;

    public BasicProvider(Capability<T> capability, T instance){
        this.capability = capability;
        this.instance = instance;
        this.provider = LazyOptional.of(() -> instance);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return this.capability == cap ? this.provider.cast() : LazyOptional.empty();
    }

    @Override
    public C serializeNBT() {
        return instance.serializeNBT();
    }

    @Override
    public void deserializeNBT(C nbt) {
        this.instance.deserializeNBT(nbt);
    }
}
