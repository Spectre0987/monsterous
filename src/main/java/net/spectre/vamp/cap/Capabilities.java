package net.spectre.vamp.cap;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.spectre.vamp.cap.items.IColt;

public class Capabilities {

	@CapabilityInject(IMonster.class)
	public static final Capability<IMonster> MONSTER = null;

	@CapabilityInject(IMinion.class)
	public static final Capability<IMinion> MINION = null;

	@CapabilityInject(ISmiteable.class)
	public static final Capability<ISmiteable> SMITE = null;

	@CapabilityInject(IColt.class)
	public static final Capability<IColt> COLT = null;
} 
