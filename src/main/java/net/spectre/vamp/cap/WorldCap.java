package net.spectre.vamp.cap;

import com.google.common.collect.Lists;
import net.minecraft.world.World;
import net.spectre.vamp.misc.SoulChest;

import java.util.List;
import java.util.UUID;

public class WorldCap implements IWorldCap{

    private World world;
    private List<SoulChest> soulChests = Lists.newArrayList();

    public WorldCap(World world){
        this.world = world;
    }

    @Override
    public void addSoulChest(SoulChest chest) {
        this.soulChests.add(chest);
    }

    @Override
    public List<SoulChest> getSoulChestByOwner(UUID owner) {
        List<SoulChest> matchingChests = Lists.newArrayList();
        for(SoulChest chest : this.soulChests){
            if(chest.getOwner().equals(owner))
                matchingChests.add(chest);
        }
        return matchingChests;
    }

    @Override
    public List<SoulChest> getSoulChestByDemon(UUID demon) {
        List<SoulChest> matchingChests = Lists.newArrayList();
        for(SoulChest chest : this.soulChests){
            if(chest.getDemon().equals(demon))
                matchingChests.add(chest);
        }
        return matchingChests;
    }
}
